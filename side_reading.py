import cv2
import numpy as np
from pymba import Vimba, Frame, VimbaException
from typing import Optional
import kociemba


class SideReading:
    PIXEL_FORMATS_CONVERSIONS = {
        'BayerRG8': cv2.COLOR_BAYER_RG2RGB,
    }
    COLOR_NAMES = ["BIALY", "CZERWONY", "NIEBIESKI", "POMARANCZOWY", "ZIELONY", "ZOLTY"]
    COLORS = [(244.44, 255, 255),
              (12.33, 62.44, 255.0),
              (151.55, 86.77, 43.33),
              (29.77, 224, 255.0),
              (73.33, 255, 121),
              (85.11, 255, 255)]

    MITSUBISHI_COLORS = [(255.0, 255.0, 255.0),
                         (14.555555555555555, 69.0, 255.0),
                         (171.66666666666666, 93.11111111111111, 44.888888888888886),
                         (33.333333333333336, 237.11111111111111, 255.0),
                         (87.33333333333333, 255.0, 139.88888888888889),
                         (99.77777777777777, 255.0, 255.0)]

    SIDE_NAMES = ['U', 'B', 'D', 'R', 'F', 'L']
    SIDES = []

    def __init__(self):
        self.image = None
        self.kociemba_string = None
        self.wall_number = 0  # 0-gora, 1-tyl, 2-dol, 3-lewo, 4-front, 5-prawo

    def get_cubes(self, image):

        percent = 0.3
        side_length = 83
        left_up = image[int(117+percent*side_length):int(201-percent*side_length), int(198+percent*side_length):int(281-percent*side_length)]
        center_up = image[int(117+percent*side_length):int(201-percent*side_length), int(281+percent*side_length):int(364-percent*side_length)]
        right_up = image[int(117+percent*side_length):int(201-percent*side_length), int(364+percent*side_length):int(448-percent*side_length)]

        left_mid = image[int(201+percent*side_length):int(286-percent*side_length), int(198+percent*side_length):int(281-percent*side_length)]
        center_mid = image[int(201+percent*side_length):int(286-percent*side_length), int(281+percent*side_length):int(364-percent*side_length)]
        right_mid = image[int(201+percent*side_length):int(286-percent*side_length), int(364+percent*side_length):int(448-percent*side_length)]

        left_down = image[int(286+percent*side_length):int(370-percent*side_length), int(198+percent*side_length):int(281-percent*side_length)]
        center_down = image[int(286+percent*side_length):int(370-percent*side_length), int(281+percent*side_length):int(364-percent*side_length)]
        right_down = image[int(286+percent*side_length):int(370-percent*side_length), int(364+percent*side_length):int(448-percent*side_length)]

        if self.wall_number == 0:
            return [right_up, right_mid, right_down, center_up, center_mid, center_down, left_up, left_mid, left_down]
        elif self.wall_number == 1 or self.wall_number == 2:
            return [left_down, left_mid, left_up, center_down, center_mid, center_up, right_down, right_mid, right_up]
        elif self.wall_number == 3 or self.wall_number == 4:
            return [left_up, center_up, right_up, left_mid, center_mid, right_mid, left_down, center_down, right_down]
        elif self.wall_number == 5:
            return [right_down, center_down, left_down, right_mid, center_mid, left_mid, right_up, center_up, left_up]

    def histogram(self, image):
        color = []
        for channel in cv2.split(image):
            array = np.array(channel).flatten()
            unique, counts = np.unique(array, return_counts=True)
            array_h = np.zeros(256)
            array_h[unique] = counts
            color.append(np.argmax(array_h))
        return tuple(color)

    def read_cube_colors(self, cubes, robot):
        read_cube = []
        if robot == 'k':
            colors = self.COLORS
        elif robot == 'm':
            colors = self.MITSUBISHI_COLORS
        for cube in cubes:
            color = self.histogram(cube)
            differences = [[self.color_difference(color, target_value), i] for i, target_value in
                           enumerate(colors)]
            differences.sort()
            color_name = self.COLOR_NAMES[differences[0][1]]
            read_cube.append(color_name)
        return read_cube

    def color_difference(self, color1, color2):
        return sum([abs(component1 - component2) for component1, component2 in zip(color1, color2)])

    def show_cube(self, cube_colors):
        image = np.zeros((360, 360, 3), np.uint8)
        color = (0, 0, 0)
        thick = 2
        colors = {"BIALY": (255, 255, 255), "CZERWONY": (0, 0, 255), "NIEBIESKI": (255, 0, 0),
                  "POMARANCZOWY": (0, 128, 255), "ZIELONY": (0, 255, 0), "ZOLTY": (0, 255, 255)}

        image[0:120, 0:120] = colors[cube_colors[0]]
        image[0:120, 120:240] = colors[cube_colors[1]]
        image[0:120, 240:360] = colors[cube_colors[2]]
        image[120:240, 0:120] = colors[cube_colors[3]]
        image[120:240, 120:240] = colors[cube_colors[4]]
        image[120:240, 240:360] = colors[cube_colors[5]]
        image[240:360, 0:120] = colors[cube_colors[6]]
        image[240:360, 120:240] = colors[cube_colors[7]]
        image[240:360, 240:360] = colors[cube_colors[8]]

        image = cv2.line(image, (120, 0), (120, 360), color, thick)
        image = cv2.line(image, (240, 0), (240, 360), color, thick)
        image = cv2.line(image, (0, 120), (360, 120), color, thick)
        image = cv2.line(image, (0, 240), (360, 240), color, thick)

        cv2.imshow('cube', image)

    def display_frame(self, frame: Frame) -> None:
        image = frame.buffer_data_numpy()
        image = self.draw_lines(image)
        try:
            image = cv2.cvtColor(image, self.PIXEL_FORMATS_CONVERSIONS[frame.pixel_format])
        except KeyError:
            pass
        self.image = image

    def draw_lines(self, img):
        color = (0, 0, 0)
        thick = 2
        img = cv2.line(img, (198, 117), (198, 370), color, thick)
        img = cv2.line(img, (198, 117), (448, 117), color, thick)
        img = cv2.line(img, (198, 370), (448, 370), color, thick)
        img = cv2.line(img, (448, 117), (448, 370), color, thick)
        img = cv2.line(img, (281, 117), (281, 370), color, thick)
        img = cv2.line(img, (364, 117), (364, 370), color, thick)
        img = cv2.line(img, (198, 201), (448, 201), color, thick)
        img = cv2.line(img, (198, 285), (448, 285), color, thick)
        return img

    def get_camera_image(self):
        with Vimba() as vimba:
            camera = vimba.camera(0)
            camera.open()
            feature = camera.feature('ExposureAuto')
            feature.value = 'Off'

            feature = camera.feature('ExposureAutoAdjustTol')
            feature.value = 2

            feature = camera.feature('ExposureAutoMin')
            feature.value = 4838

            feature = camera.feature('ExposureTimeAbs')
            feature.value = 7233
            camera.arm('SingleFrame')
            try:
                frame = camera.acquire_frame()
                self.display_frame(frame)
            except VimbaException as e:
                if e.error_code == VimbaException.ERR_TIMEOUT:
                    print(e)
                    camera.disarm()
                    camera.arm('SingleFrame')
                else:
                    raise
            camera.disarm()
            camera.close()

    def get_cube_colors(self, robot):
        self.get_camera_image()
        cubes = self.get_cubes(self.image)
        read_cubes = self.read_cube_colors(cubes, robot)
        self.SIDES.append(read_cubes)
        print(read_cubes)
        self.show_cube(read_cubes)
        cv2.imshow('image', self.image)
        cv2.waitKey()
        self.wall_number += 1

    def prepare_for_kociemba(self):
        colors_dict = {}
        for i, side in enumerate(self.SIDES):
            colors_dict[side[4]] = self.SIDE_NAMES[i]
        print(colors_dict)
        for i, side in enumerate(self.SIDES):
            for j, cube in enumerate(side):
                self.SIDES[i][j] = colors_dict[self.SIDES[i][j]]

        self.kociemba_string = ''.join(self.SIDES[0]) + ''.join(self.SIDES[3]) + ''.join(self.SIDES[4]) + ''.join(
            self.SIDES[2]) + ''.join(self.SIDES[5]) + ''.join(self.SIDES[1])
        print(self.kociemba_string)

    def solve(self):
        try:
            moves = kociemba.solve(self.kociemba_string)
        except:
            moves = "Q"
        return moves


# detector = SideReading()
# # trzeba dodać żeby najpierw byl sygnal ze klocek ustawiony do czytania, do powtorzenia 6 razy!!!
# detector.get_cube_colors()
# ## po przejsciu wszystkich scian
# detector.prepare_for_kociemba()
# detector.solve()
