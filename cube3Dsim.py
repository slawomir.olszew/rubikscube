from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
import matplotlib.pyplot as plt
from matplotlib.widgets import Button
import numpy as np
import kociemba
import matplotlib as mpl
import random

mpl.rcParams['toolbar'] = 'None'


# To jest kostka do wlasnej symulacji, w trakcie dzialania robotow odpalamy cube3D.py
# odpalac przed .prepare_for_kociemba z argumentem colors_array=.SIDES i argumentem robot_solving=True -> wtedy nie mozemy uzywac przyciskow do obrotu
class Cube:
    def __init__(self, colors_array=None, robot_solving=False):
        self.num = 9
        self.robot_solving = robot_solving
        self.colors_dict = {'BIALY': 'white', 'CZERWONY': 'red', 'NIEBIESKI': 'blue', 'POMARANCZOWY': 'orange',
                            'ZIELONY': 'green', 'ZOLTY': 'yellow'}
        fig = plt.figure()
        self.ax = fig.gca(projection='3d')
        self.c_a = None
        if colors_array:
            self.colors = self.get_colors(colors_array)
        else:
            self.colors = ['orange'] * 9 + ['green'] * 9 + ['red'] * 9 + ['blue'] * 9 + ['yellow'] * 9 + ['white'] * 9
        self.color_gen = self.color_generator()

        self.up = []
        for x in np.linspace(-1.5, 0.5, 3):
            for y in np.linspace(-1.5, 0.5, 3):
                verts = [[(x, y, 1.5), (x + 1, y, 1.5), (x + 1, y + 1, 1.5), (x, y + 1, 1.5)]]
                cube = Poly3DCollection(verts, facecolors=next(self.color_gen), edgecolors='black')
                self.ax.add_collection3d(cube)
                self.up.append([cube, verts])
        self.down = []
        for x in np.linspace(-1.5, 0.5, 3):
            for y in np.linspace(-1.5, 0.5, 3):
                verts = [[(x, y, -1.5), (x + 1, y, -1.5), (x + 1, y + 1, -1.5), (x, y + 1, -1.5)]]
                cube = Poly3DCollection(verts, facecolors=next(self.color_gen), edgecolors='black')
                self.ax.add_collection3d(cube)
                self.down.append([cube, verts])
        self.front = []
        for x in np.linspace(-1.5, 0.5, 3):
            for z in np.linspace(-1.5, 0.5, 3):
                verts = [[(x, -1.5, z), (x + 1, -1.5, z), (x + 1, -1.5, z + 1), (x, -1.5, z + 1)]]
                cube = Poly3DCollection(verts, facecolors=next(self.color_gen), edgecolors='black')
                self.ax.add_collection3d(cube)
                self.front.append([cube, verts])
        self.back = []
        for x in np.linspace(-1.5, 0.5, 3):
            for z in np.linspace(-1.5, 0.5, 3):
                verts = [[(x, 1.5, z), (x + 1, 1.5, z), (x + 1, 1.5, z + 1), (x, 1.5, z + 1)]]
                cube = Poly3DCollection(verts, facecolors=next(self.color_gen), edgecolors='black')
                self.ax.add_collection3d(cube)
                self.back.append([cube, verts])
        self.right = []
        for y in np.linspace(-1.5, 0.5, 3):
            for z in np.linspace(-1.5, 0.5, 3):
                verts = [[(1.5, y, z), (1.5, y + 1, z), (1.5, y + 1, z + 1), (1.5, y, z + 1)]]
                cube = Poly3DCollection(verts, facecolors=next(self.color_gen), edgecolors='black')
                self.ax.add_collection3d(cube)
                self.right.append([cube, verts])
        self.left = []
        for y in np.linspace(-1.5, 0.5, 3):
            for z in np.linspace(-1.5, 0.5, 3):
                verts = [[(-1.5, y, z), (-1.5, y + 1, z), (-1.5, y + 1, z + 1), (-1.5, y, z + 1)]]
                cube = Poly3DCollection(verts, facecolors=next(self.color_gen), edgecolors='black')
                self.ax.add_collection3d(cube)
                self.left.append([cube, verts])
        up_color = [color for sublist in self.up[4][0]._facecolors for color in sublist]
        down_color = [color for sublist in self.down[4][0]._facecolors for color in sublist]
        left_color = [color for sublist in self.left[4][0]._facecolors for color in sublist]
        right_color = [color for sublist in self.right[4][0]._facecolors for color in sublist]
        front_color = [color for sublist in self.front[4][0]._facecolors for color in sublist]
        back_color = [color for sublist in self.back[4][0]._facecolors for color in sublist]
        move_color = 'red' if self.robot_solving else 'green'

        ax_upL = plt.axes([0.05, .70, 0.1, 0.075])
        ax_upR = plt.axes([0.85, .70, 0.1, 0.075])
        ax_downL = plt.axes([0.05, .60, 0.1, 0.075])
        ax_downR = plt.axes([0.85, .60, 0.1, 0.075])
        ax_frontL = plt.axes([0.05, .50, 0.1, 0.075])
        ax_frontR = plt.axes([0.85, .50, 0.1, 0.075])
        ax_backL = plt.axes([0.05, .40, 0.1, 0.075])
        ax_backR = plt.axes([0.85, .40, 0.1, 0.075])
        ax_rightL = plt.axes([0.05, .30, 0.1, 0.075])
        ax_rightR = plt.axes([0.85, .30, 0.1, 0.075])
        ax_leftL = plt.axes([0.05, .20, 0.1, 0.075])
        ax_leftR = plt.axes([0.85, .20, 0.1, 0.075])
        ax_viewReset = plt.axes([0.60, 0.05, 0.15, 0.075])
        ax_moveActive = plt.axes([0.30, 0.05, 0.15, 0.075])
        ax_shuffle = plt.axes([0.30, 0.9, 0.15, 0.075])
        ax_solve = plt.axes([0.60, 0.9, 0.15, 0.075])

        self.b_upL = Button(ax_upL, 'UP L', color=up_color)
        self.b_upR = Button(ax_upR, 'UP R', color=up_color)
        self.b_downL = Button(ax_downL, 'DOWN L', color=down_color)
        self.b_downR = Button(ax_downR, 'DOWN R', color=down_color)
        self.b_frontL = Button(ax_frontL, 'FRONT L', color=front_color)
        self.b_frontR = Button(ax_frontR, 'FRONT R', color=front_color)
        self.b_backL = Button(ax_backL, 'BACK L', color=back_color)
        self.b_backR = Button(ax_backR, 'BACK R', color=back_color)
        self.b_rightL = Button(ax_rightL, 'RIGHT L', color=right_color)
        self.b_rightR = Button(ax_rightR, 'RIGHT R', color=right_color)
        self.b_leftL = Button(ax_leftL, 'LEFT L', color=left_color)
        self.b_leftR = Button(ax_leftR, 'LEFT R', color=left_color)
        self.b_viewReset = Button(ax_viewReset, 'Reset View')
        self.b_moveActive = Button(ax_moveActive, 'Button Moves', color=move_color)
        self.b_shuffle = Button(ax_shuffle, 'Shuffle')
        self.b_solve = Button(ax_solve, 'Solve')

        self.connect_buttons()

        if self.robot_solving:
            self.activate_move_buttons(False)

        self.ax.set_xlim(-1.5, 1.5)
        self.ax.set_ylim(-1.5, 1.5)
        self.ax.set_zlim(-1.5, 1.5)
        self.ax.set_xticklabels([])
        self.ax.set_yticklabels([])
        self.ax.set_zticklabels([])
        self.ax.grid(False)
        self.ax.axis('off')
        plt.show(block=False)

    def activate_move_buttons(self, active):
        b_move_array = [self.b_upL, self.b_upR, self.b_downL, self.b_downR, self.b_frontL, self.b_frontR,
                        self.b_rightR, self.b_rightL, self.b_backL,
                        self.b_backR, self.b_leftR, self.b_leftL]
        for button in b_move_array:
            button.set_active(active)

    def connect_buttons(self):
        self.refresh_sides()
        self.b_upL.on_clicked(lambda x: self.rotate_Z(x, 'u', 1))
        self.b_upR.on_clicked(lambda x: self.rotate_Z(x, 'u', -1))
        self.b_downL.on_clicked(lambda x: self.rotate_Z(x, 'd', -1))
        self.b_downR.on_clicked(lambda x: self.rotate_Z(x, 'd', 1))
        self.b_frontL.on_clicked(lambda x: self.rotate_Y(x, 'f', 1))
        self.b_frontR.on_clicked(lambda x: self.rotate_Y(x, 'f', -1))
        self.b_backL.on_clicked(lambda x: self.rotate_Y(x, 'b', -1))
        self.b_backR.on_clicked(lambda x: self.rotate_Y(x, 'b', 1))
        self.b_rightL.on_clicked(lambda x: self.rotate_X(x, 'r', -1))
        self.b_rightR.on_clicked(lambda x: self.rotate_X(x, 'r', 1))
        self.b_leftL.on_clicked(lambda x: self.rotate_X(x, 'l', 1))
        self.b_leftR.on_clicked(lambda x: self.rotate_X(x, 'l', -1))

        self.b_viewReset.on_clicked(self.reset_view)
        self.b_moveActive.on_clicked(self.activate_moves)
        self.b_shuffle.on_clicked(self.shuffle)
        self.b_solve.on_clicked(self.solve_by_robot)

    def refresh_sides(self):
        self.up_side = self.up + self.front[2::3] + self.left[2::3] + self.back[2::3] + self.right[2::3]
        self.down_side = self.down + self.front[::3] + self.left[::3] + self.back[::3] + self.right[::3]
        self.front_side = self.front + self.up[::3] + self.right[0:3] + self.down[::3] + self.left[0:3]
        self.back_side = self.back + self.up[2::3] + self.right[6:9] + self.down[2::3] + self.left[6:9]
        self.right_side = self.right + self.front[6:9] + self.up[6:9] + self.back[6:9] + self.down[6:9]
        self.left_side = self.left + self.front[0:3] + self.up[0:3] + self.back[0:3] + self.down[0:3]

    def get_colors(self, c_a):
        colors = []
        sides_by_my_order = [c_a[0], c_a[2], c_a[4], c_a[1], c_a[3], c_a[5]]
        for i, side in enumerate(sides_by_my_order):
            if i == 5 or i == 3:
                side = [side[8], side[5], side[2], side[7], side[4], side[1], side[6], side[3], side[0]]
            elif i == 1:
                side = [side[0], side[3], side[6], side[1], side[4], side[7], side[2], side[5], side[8]]
            else:
                side = [side[6], side[3], side[0], side[7], side[4], side[1], side[8], side[5], side[2]]
            for color in side:
                colors.append(self.colors_dict[color])
        return colors

    def color_generator(self):
        for color in self.colors:
            yield color

    def rotate_Z(self, event, side_l, mov):
        if side_l == 'u':
            side = [[i, s] for i, s in enumerate(self.up_side)]
        elif side_l == 'd':
            side = [[i, s] for i, s in enumerate(self.down_side)]
        angle_max = mov * np.pi / 2
        for _ in range(self.num):
            for i, cube_verts in side:
                cube, verts = cube_verts
                angle = angle_max / self.num
                verts = verts[0]
                verts = np.array(verts).astype(np.float)
                R = np.array([[np.cos(angle), -np.sin(angle), 0],
                              [np.sin(angle), np.cos(angle), 0],
                              [0, 0, 1]])
                new_verts = []
                for vert in verts:
                    new_vert = np.dot(vert, R.T)
                    new_vert = tuple(new_vert)
                    new_verts.append(new_vert)
                new_verts = [new_verts]
                cube.set_verts(new_verts)
                side[i][1][1] = new_verts
            plt.draw()
            plt.pause(.001)
        if side_l == 'u':
            if mov == 1:
                tmp = self.front[8::-3]
                self.front[2::3] = self.left[8::-3]
                tmp2 = self.right[8::-3]
                self.right[8::-3] = tmp
                tmp = self.back[8::-3]
                self.back[2::3] = tmp2
                self.left[8::-3] = tmp
                self.up = [self.up[2], self.up[5], self.up[8], self.up[1], self.up[4], self.up[7],
                           self.up[0], self.up[3], self.up[6]]
            elif mov == -1:
                for _ in range(3):
                    tmp = self.front[8::-3]
                    self.front[2::3] = self.left[8::-3]
                    tmp2 = self.right[8::-3]
                    self.right[8::-3] = tmp
                    tmp = self.back[8::-3]
                    self.back[2::3] = tmp2
                    self.left[8::-3] = tmp
                self.up = [self.up[6], self.up[3], self.up[0], self.up[7], self.up[4], self.up[1],
                           self.up[8], self.up[5], self.up[2]]
        elif side_l == 'd':
            if mov == 1:
                tmp = self.front[6::-3]
                self.front[0::3] = self.left[6::-3]
                tmp2 = self.right[6::-3]
                self.right[6::-3] = tmp
                tmp = self.back[6::-3]
                self.back[0::3] = tmp2
                self.left[6::-3] = tmp
                self.down = [self.down[2], self.down[5], self.down[8], self.down[1], self.down[4], self.down[7],
                             self.down[0], self.down[3], self.down[6]]
            elif mov == -1:
                for _ in range(3):
                    tmp = self.front[6::-3]
                    self.front[0::3] = self.left[6::-3]
                    tmp2 = self.right[6::-3]
                    self.right[6::-3] = tmp
                    tmp = self.back[6::-3]
                    self.back[0::3] = tmp2
                    self.left[6::-3] = tmp
                self.down = [self.down[6], self.down[3], self.down[0], self.down[7], self.down[4], self.down[1],
                             self.down[8], self.down[5], self.down[2]]
        self.refresh_sides()

    def rotate_Y(self, event, side_l, mov):
        if side_l == 'f':
            side = [[i, s] for i, s in enumerate(self.front_side)]
        elif side_l == 'b':
            side = [[i, s] for i, s in enumerate(self.back_side)]
        angle_max = -mov * np.pi / 2
        for _ in range(self.num):
            for i, cube_verts in side:
                cube, verts = cube_verts
                angle = angle_max / self.num
                verts = verts[0]
                verts = np.array(verts).astype(np.float)
                R = np.array([[np.cos(angle), 0, np.sin(angle)],
                              [0, 1, 0, ],
                              [-np.sin(angle), 0, np.cos(angle)]])
                new_verts = []
                for vert in verts:
                    new_vert = np.dot(vert, R.T)
                    new_vert = tuple(new_vert)
                    new_verts.append(new_vert)
                new_verts = [new_verts]
                cube.set_verts(new_verts)
                side[i][1][1] = new_verts
            plt.draw()
            plt.pause(.001)
        if side_l == 'f':
            if mov == -1:
                tmp = self.right[0:3]
                self.right[2::-1] = self.up[::3]
                tmp2 = self.down[::3]
                self.down[::3] = tmp
                tmp = self.left[0:3]
                self.left[2::-1] = tmp2
                self.up[::3] = tmp

                self.front = [self.front[6], self.front[3], self.front[0], self.front[7], self.front[4], self.front[1],
                              self.front[8], self.front[5], self.front[2]]

            elif mov == 1:
                for _ in range(3):
                    tmp = self.right[0:3]
                    self.right[2::-1] = self.up[::3]
                    tmp2 = self.down[::3]
                    self.down[::3] = tmp
                    tmp = self.left[0:3]
                    self.left[2::-1] = tmp2
                    self.up[::3] = tmp

                self.front = [self.front[2], self.front[5], self.front[8], self.front[1], self.front[4], self.front[7],
                              self.front[0], self.front[3], self.front[6]]

        elif side_l == 'b':
            if mov == 1:
                tmp = self.up[2::3]
                self.up[8::-3] = self.right[6:9]
                tmp2 = self.left[6:9]
                self.left[6:9] = tmp
                tmp = self.down[2::3]
                self.down[8::-3] = tmp2
                self.right[6:9] = tmp

                self.back = [self.back[2], self.back[5], self.back[8], self.back[1], self.back[4], self.back[7],
                             self.back[0], self.back[3], self.back[6]]
            elif mov == -1:
                for _ in range(3):
                    tmp = self.up[2::3]
                    self.up[8::-3] = self.right[6:9]
                    tmp2 = self.left[6:9]
                    self.left[6:9] = tmp
                    tmp = self.down[2::3]
                    self.down[8::-3] = tmp2
                    self.right[6:9] = tmp
                self.back = [self.back[6], self.back[3], self.back[0], self.back[7], self.back[4], self.back[1],
                             self.back[8], self.back[5], self.back[2]]
        self.refresh_sides()

    def rotate_X(self, event, side_l, mov):
        if side_l == 'r':
            side = [[i, s] for i, s in enumerate(self.right_side)]
        elif side_l == 'l':
            side = [[i, s] for i, s in enumerate(self.left_side)]
        angle_max = -mov * np.pi / 2
        for _ in range(self.num):
            for i, cube_verts in side:
                cube, verts = cube_verts
                angle = angle_max / self.num
                verts = verts[0]
                verts = np.array(verts).astype(np.float)
                R = np.array([[1, 0, 0],
                              [0, np.cos(angle), -np.sin(angle)],
                              [0, np.sin(angle), np.cos(angle)]])
                new_verts = []
                for vert in verts:
                    new_vert = np.dot(vert, R.T)
                    new_vert = tuple(new_vert)
                    new_verts.append(new_vert)
                new_verts = [new_verts]
                cube.set_verts(new_verts)
                side[i][1][1] = new_verts
            plt.draw()
            plt.pause(.001)
        if side_l == 'r':
            if mov == -1:
                tmp = self.front[6:9]
                self.front[6:9] = self.up[6:9]
                tmp2 = self.down[6:9]
                self.down[6:9] = tmp[::-1]
                tmp = self.back[6:9]
                self.back[6:9] = tmp2
                self.up[6:9] = tmp[::-1]
                self.right = [self.right[2], self.right[5], self.right[8], self.right[1], self.right[4], self.right[7],
                              self.right[0], self.right[3], self.right[6]]

            elif mov == 1:
                for _ in range(3):
                    tmp = self.front[6:9]
                    self.front[6:9] = self.up[6:9]
                    tmp2 = self.down[6:9]
                    self.down[6:9] = tmp[::-1]
                    tmp = self.back[6:9]
                    self.back[6:9] = tmp2
                    self.up[6:9] = tmp[::-1]
                self.right = [self.right[6], self.right[3], self.right[0], self.right[7], self.right[4], self.right[1],
                              self.right[8], self.right[5], self.right[2]]

        elif side_l == 'l':
            if mov == 1:
                tmp = self.up[0:3]
                self.up[0:3] = self.front[0:3]
                tmp2 = self.back[2::-1]
                self.back[2::-1] = tmp
                tmp = self.down[0:3]
                self.down[2::-1] = tmp2
                self.front[2::-1] = tmp

                self.left = [self.left[6], self.left[3], self.left[0], self.left[7], self.left[4], self.left[1],
                             self.left[8], self.left[5], self.left[2]]
            elif mov == -1:
                for _ in range(3):
                    tmp = self.up[0:3]
                    self.up[0:3] = self.front[0:3]
                    tmp2 = self.back[2::-1]
                    self.back[2::-1] = tmp
                    tmp = self.down[0:3]
                    self.down[2::-1] = tmp2
                    self.front[2::-1] = tmp
                self.left = [self.left[2], self.left[5], self.left[8], self.left[1], self.left[4], self.left[7],
                             self.left[0], self.left[3], self.left[6]]
        self.refresh_sides()

    def reset_view(self, event):
        curr_azim = self.ax.azim
        curr_elev = self.ax.elev
        for azim in np.linspace(curr_azim, -60, self.num):
            self.ax.view_init(elev=curr_elev, azim=azim)
            plt.draw()
            plt.pause(0.001)
        for elev in np.linspace(curr_elev, 30, self.num):
            self.ax.view_init(elev=elev, azim=-60)
            plt.draw()
            plt.pause(0.001)

    def activate_moves(self, event):
        self.robot_solving = not self.robot_solving
        self.b_moveActive.color = 'red' if self.robot_solving else 'green'
        if self.robot_solving:
            self.activate_move_buttons(False)
        else:
            self.activate_move_buttons(True)

    @staticmethod
    def show(block):
        plt.show(block=block)

    @staticmethod
    def start():
        plt.draw()
        plt.pause(1)

    def get_cube_array(self):
        def get_colors(faces):
            c_a = []
            for face in faces:
                color = face[0]._facecolors[0]
                hex_color = mpl.colors.to_hex(color, keep_alpha=True)
                color = colors_dict[hex_color]
                c_a.append(color)
            if faces == self.up or faces == self.front or faces == self.right:
                colors_array = [c_a[2], c_a[5], c_a[8], c_a[1], c_a[4], c_a[7], c_a[0], c_a[3], c_a[6]]
            elif faces == self.left or faces == self.back:
                colors_array = [c_a[8], c_a[5], c_a[2], c_a[7], c_a[4], c_a[1], c_a[6], c_a[3], c_a[0]]
            elif faces == self.down:
                colors_array = [c_a[0], c_a[3], c_a[6], c_a[1], c_a[4], c_a[7], c_a[2], c_a[5], c_a[8]]
            return colors_array

        colors_dict = {'#ffa500ff': 'orange', '#008000ff': 'green', '#ff0000ff': 'red', '#0000ffff': 'blue',
                       '#ffffffff': 'white', '#ffff00ff': 'yellow'}
        cube_array = [get_colors(self.up), get_colors(self.back), get_colors(self.down), get_colors(self.right),
                      get_colors(self.front), get_colors(self.left)]
        return cube_array

    def solve_by_robot(self, event):
        def single_move():
            if move[0] in X:
                self.rotate_X(None, move[0].lower(), direction)
            if move[0] in Y:
                self.rotate_Y(None, move[0].lower(), direction)
            if move[0] in Z:
                self.rotate_Z(None, move[0].lower(), direction)
            self.show(False)

        self.b_solve.set_active(False)
        self.b_moveActive.set_active(False)
        self.b_shuffle.set_active(False)
        self.activate_move_buttons(False)
        self.c_a = self.get_cube_array()
        moves = self.solve(self.c_a)
        moves = [list(move) for move in moves.split()]
        X = ['L', 'R']
        Y = ['F', 'B']
        Z = ['U', 'D']
        left_dir_dict = {'L': 1, 'R': -1, 'F': 1, 'B': -1, 'U': 1, 'D': -1}
        for move in moves:
            direction = left_dir_dict[move[0]]
            if len(move) == 2:
                if move[1] == "'":
                    single_move()
                if move[1] == '2':
                    for _ in range(2):
                        single_move()
            if len(move) == 1:
                direction = -1 * direction
                single_move()
        self.b_solve.set_active(True)
        self.b_shuffle.set_active(True)
        self.activate_move_buttons(True)
        self.b_moveActive.set_active(True)

    def shuffle(self, event):
        def single_move(face, direction, number):
            for _ in range(number):
                if face in X:
                    self.rotate_X(None, face.lower(), direction)
                if face in Y:
                    self.rotate_Y(None, face.lower(), direction)
                if face in Z:
                    self.rotate_Z(None, face.lower(), direction)
                self.show(False)

        self.b_shuffle.set_active(False)
        self.b_moveActive.set_active(False)
        self.b_solve.set_active(False)
        self.activate_move_buttons(False)
        self.c_a = None
        X = ['L', 'R']
        Y = ['F', 'B']
        Z = ['U', 'D']
        faces = X + Y + Z
        directions = [1, -1]
        for _ in range(20):
            face = random.choice(faces)
            direction = random.choice(directions)
            number = random.randint(1, 2)
            single_move(face, direction, number)
        self.b_shuffle.set_active(True)
        self.b_solve.set_active(True)
        self.activate_move_buttons(True)
        self.b_moveActive.set_active(True)

    def prepare_for_kociemba(self, SIDES):
        SIDE_NAMES = ['U', 'B', 'D', 'R', 'F', 'L']
        colors_dict = {}
        for i, side in enumerate(SIDES):
            colors_dict[side[4]] = SIDE_NAMES[i]
        for i, side in enumerate(SIDES):
            for j, cube in enumerate(side):
                SIDES[i][j] = colors_dict[SIDES[i][j]]

        kociemba_string = ''.join(SIDES[0]) + ''.join(SIDES[3]) + ''.join(SIDES[4]) + ''.join(
            SIDES[2]) + ''.join(SIDES[5]) + ''.join(SIDES[1])
        return kociemba_string

    def solve(self, c_a):
        kociemba_string = self.prepare_for_kociemba(c_a)
        moves = kociemba.solve(kociemba_string)
        return moves


if __name__ == '__main__':
    cube_array = [['CZERWONY', 'ZOLTY', 'ZOLTY', 'CZERWONY', 'NIEBIESKI', 'ZIELONY', 'BIALY', 'ZOLTY', 'POMARANCZOWY'],
                  ['ZIELONY', 'NIEBIESKI', 'BIALY', 'BIALY', 'ZOLTY', 'POMARANCZOWY', 'POMARANCZOWY', 'CZERWONY',
                   'NIEBIESKI'],
                  ['CZERWONY', 'POMARANCZOWY', 'ZIELONY', 'ZIELONY', 'ZIELONY', 'POMARANCZOWY', 'CZERWONY', 'NIEBIESKI',
                   'ZIELONY'],
                  ['ZOLTY', 'CZERWONY', 'CZERWONY', 'POMARANCZOWY', 'CZERWONY', 'CZERWONY', 'BIALY', 'NIEBIESKI',
                   'ZOLTY'],
                  ['POMARANCZOWY', 'ZIELONY', 'NIEBIESKI', 'NIEBIESKI', 'BIALY', 'ZIELONY', 'NIEBIESKI', 'BIALY',
                   'POMARANCZOWY'],
                  ['ZIELONY', 'ZOLTY', 'NIEBIESKI', 'ZOLTY', 'POMARANCZOWY', 'BIALY', 'ZOLTY', 'BIALY', 'BIALY']]
    C_A = cube_array
    c = Cube(cube_array, False)
    # c = Cube()
    c.start()
    c.show(True)  # zawsze na koniec, zblokuje reszte programu, dopoki nie zamknie sie okna to nie ruszy dalej
