ECHO OFF
CLS
ECHO.===================================================
ECHO =:     "C" Series Controller - AS Terminal       :=
ECHO =:          KC-monitor Install Program           :=
ECHO =:        KAWASAKI HEAVY INDUSTRIES LTD.,        :=
ECHO.===================================================

IF "%1" == "" GOTO HOW_USE
IF "%1" == "/?" GOTO HOW_USE
IF "%1" == "-?" GOTO HOW_USE

ECHO ---------------------------------------------------
ECHO Install Directory : %1%2
ECHO ---------------------------------------------------
PAUSE

IF "%2" == "" GOTO SKIP
IF NOT EXIST %1%2\*.* MD %1%2
CD %1%2
:SKIP

ECHO.

ECHO KCMON_.EXE
COPY KCMON_.EXE     %1KCMON_.EXE

ECHO KCMON.BAT
COPY KCMON.BAT      %1KCMON.BAT

ECHO EDT.BAT
COPY EDT.BAT        %1EDT.BAT

ECHO FUTL.BAT
COPY FUTL.BAT       %1FUTL.BAT

ECHO KCMONENV.DAT
COPY KCMONENV.DAT   %1KCMONENV.DAT

GOTO END

:HOW_USE
ECHO.
ECHO Usage: "A>inst c:" or "A>inst c: \kcmon"
GOTO EXIT

:END
ECHO.
ECHO End of Install Program.
ECHO Please Setting for "BAT" Files. ( [ EDT.BAT ], [ FUTL.BAT ])
ECHO.
:EXIT
ECHO.
