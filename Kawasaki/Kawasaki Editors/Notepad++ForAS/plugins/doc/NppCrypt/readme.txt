------------------------------------------------------------------------------------------------------
NppCrypt 1.009 (unicode)
------------------------------------------------------------------------------------------------------
www.cerberus-design.de/nppcrypt/nppcryptv1009.zip
------------------------------------------------------------------------------------------------------
Plugin for Notepad++ [ Copyright (C)2003 Don HO <don.h@free.fr> ]
This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
------------------------------------------------------------------------------------------------------

encrypt/decrypt/hash your notepad++ files using symmetric ciphers like aes.

-----------------------------------------------------------------------------------------------------------
###> this plugin uses openssl v1.0.1i and code from https://github.com/gvanas/KeccakCodePackage.       <###
###> the sourcecode can be downloaded from www.cerberus-design.de/nppcrypt/nppcryptv1009.src.zip       <###
###> the zip-file includes the openssl-lib and can therefore be compiled without further requirements  <###
-----------------------------------------------------------------------------------------------------------
	
###> Changelog:
v1.009:
 - reintroduced standard salting. the CSPRNG salt is stored "openssl-style" in front of the data following "Salted__" (or "53616C7465645F5F" in hexmode). "Salting" is strongly recommended especially if the same password is used for different files.
v1.008:
 - added sha3 hash (after latest draft http://csrc.nist.gov/groups/ST/hash/sha-3/sha-3_standard_fips202.html)
 - added ctr-mode for aes + a lot of des stuff
 - completely changed the use of salt in encryption
 - added preferences dialog with options for key derivation, salt-presets, etc. (thx to Eugene Kuznetsov for suggesting the option to "silently" encrypt nppcrypt-files)
 - added ability to generate (pseudo) random characters.
 - better memory-management etc.
v1.007:
 - added extension option to the plugins/config/NppCrypt.ini file, which allows to replace the default .nppcrypt with any other extension.
 - fixed a problem concerning the "use old encryption"-option for nppcrypt-files.        
v1.006:
 - added whirlpool hash
 - you now can specify the cipher-mode (cbc,ecb,cfb,ofb)
 - if no text is selected when you click "encrypt" all open documents can be encrypted.
v1.005:
 - added some hashes (md4,sha256,sha512) & fixed sha1 bug.
 - when a nppcrypt-file was saved together with another file, which had the focus, the wrong file got encrypted. (thx to David Vere)
 - added filenames to dialogs.
v1.004:
 - improved file-handling
 - problem solved, that could lead to broken salted encryption.
v1.003:
 - improved dialogs
 - header+encryption-information will be added to .nppcrypt files
 - aes-128 added
v1.002:
 - improved password dialog
 - open/save monitoring added

------------------------------------------------------------------------------------------------------
###> FAQ
------------------------------------------------------------------------------------------------------

> what exactly is a nppcrypt-file?
	It is an encrypted file with a 16 character header that tells the plugin what method of encryption was used. The header layout is as follows:
	char 1-8 = 'nppcrypt', char 9 = nppcrypt version (1.008 = 1), char 10 = cipher_id, char 11 = mode_id, char 12 = salt_id(-3 = salt ,-2 = no salt, -1 = secondary key, 0-? = preset), char 13 = key derivation
	char 14 = hexmode(1/0), char 15-16 = not used atm (0). Please remember if you use the automatic backup function of notepad++: atm it is sadly not possible to encrypt automatic backups of nppcrypt-files!
	
> how can i change the extension of the nppcrypt-files?
	via the new preferences dialog
	
> what about this secondary key-thing?
    to be honest this is just in 1.009 to secure compatibility with 1.008. Version 1.0.1, that will give up downward-compatibility, will most probably cut this non-standard feature.
	
> nppcrypt 1.008/9 fails to decrypt something i encrypted with 1.007!
	please try using the key derivation-preset: "nppcrypt-old" in the encryption dialog.
	
> How random are the generated random characters?
	the openssl-function RAND_bytes() is used. it promises "cryptographically strong pseudo-random bytes". i take them by their word... btw: the first time every notepad++ session
	you use the nppcrypt random char functionality you have to wait for half a second or so while the "openssl randomness engine" initializes itself.
	
> I don't know anything about these encryption methods. what should i use? and especially what are these modes (cbc etc)?
	https://en.wikipedia.org/wiki/Block_cipher_mode_of_operation
	
> What about these key derivation options?
	first of all you can now specify if the passwords you enter should be encoded in utf8 or window ascii. you can also choose between the two Key derivation functions detailed in RFC 2898 (http://tools.ietf.org/html/rfc2898)
	together with the used hash-function + iterations. RFC 2898 recommends PBKDF2 with a minimum of 1000 iterations. In the encryption/decryption dialog you can choose "preferences" under key derivation if you want to use the 
	options you specified under preferences. "nppcrypt default" means: utf8, pbkdf2, md5, 1000 iterations . "nppcrypt old" should ensure compatibility with nppcrypt 1.007.
	(a detail: if you choose pbkdf1 and use a cipher with iv-length+key-length >= 16 byte (md5 length) then strictly speaking not pbkdf1 is used, but a non standard key derivaton of openssl)