.***************************************************************************
.* AS Version : version  404EET2J   02/05/27 09:50
.* No = 1 �T�[�{     : A1B5D+Z107 2002.05.30
.*                   : WD13 2002.03.15 (ZX165U-A001,ZX130U-A001)
.***************************************************************************
.IPADDRESS        1=192.168.0.2,255.255.255.0,MB8696A
.ROBOTDATA1
ZROBOT.TYPE    12  11   6 666      -10426   ZX165U-A001   ( 2013-08-06 10:19 )
ZSYSTEM         1   3   1         -76
ZLINEAR         0   0   0   0   0   0  -1  -1  -1  -1  -1  -1          89
ZZERO         268427389 268432542 268428791 268433240 268429451 268429333 268435456 268435456 268435456 268435456 268435456 268435456 -1073261975
IPADDRESS     1=192.168.0.2,255.255.255.0,MB8696A
GWAY_IPADDRESS   0.0.0.0
ZSIGSPEC       64  64 256       -5761
ZSIGSPEC2      32  32        -961
ZSETAPPLI          0      0      8      8      0     63       -1186
ZAXCONFIG       1  -1  -1  -1  -1  -1  -1  -1  -1  -1         119
ZMTNIMP_01    ON  -3814
ZMTNIMP_02    ON  -3816
ZMTNIMP_03    ON  -3818
ZMTNIMP_04    ON  -3837
ZMTNIMP_05    ON  -3839
ZMTNIMP_06    OFF -4568
ZSPEC01_03            0          -1
ZSPEC01_05        3.000  1010827263
ZSPEC01_06      999.000   -52576257
ZSPEC01_09          500       -7501
ZSPEC01_10           10        -151
OPTION01_01   OFF  -502
OPTION01_02   OFF  -504
OPTION01_03   OFF  -506
OPTION01_04   OFF  -508
OPTION01_05   ON   -564
OPTION01_06   ON   -567
OPTION01_07   ON   -569
OPTION01_08   OFF  -572
OPTION01_09   OFF  -583
OPTION01_10   OFF  -576
OPTION02_03   OFF  -510
OPTION02_04   ON   -511
OPTION02_05   ON   -513
OPTION02_06   ON   -516
OPTION02_10   ON   -671
OPTION03_06     0          -1
OPTION03_09   255       -3826
OPTION03_10   255       -3826
OPTION04_01   OFF  -519
OPTION04_02   ON   -520
OPTION04_04   OFF  -604
OPTION04_05   OFF -1510
OPTION04_06   OFF  -529
OPTION04_08   OFF  -533
OPTION04_09   OFF  -535
OPTION05_01   OFF  -537
OPTION05_02   OFF  -539
OPTION05_03   OFF  -541
OPTION05_04   OFF  -563
OPTION05_05   OFF  -545
OPTION05_06   OFF  -580
OPTION05_07   OFF  -606
OPTION05_08   OFF  -601
OPTION05_09   OFF  -607
OPTION05_10   ON   -608
OPTION06_01   OFF  -547
OPTION06_02   OFF  -549
OPTION06_06   ON   -593
OPTION06_09   OFF -4548
OPTION06_10   OFF -4501
OPTION07_04   OFF  -553
OPTION07_05   OFF  -555
OPTION07_06   OFF  -557
OPTION07_07   ON   -558
OPTION07_09   ON   -581
OPTION07_10   ON   -665
OPTION08_01   OFF -2807
OPTION08_03   OFF  -668
OPTION08_06   OFF  -674
OPTION08_07   ON   -675
OPTION08_08   ON   -677
OPTION08_10   ON   -683
OPTION09_04   OFF -2256
OPTION09_05   OFF  -774
OPTION09_6    OFF -2264
OPTION09_09   OFF -2450
OPTION09_10   ON   -699
ZSPEC02_01        0.000          -1
ZSPEC02_02        0.500  1325400063
ZSPEC02_03        6.000   884998143
OPTION10_05   OFF -2455
OPTION10_06   ON  -1503
OPTION10_07   OFF -2457
OPTION010_8   ON  -2266
OPTION10_10   OFF -2462
OPTION011_1   OFF -1523
OPTION11_02   OFF -2464
OPTION11_05   ON  -1505
OPTION11_06   OFF -2466
OPTION11_08   OFF -1508
OPTION11_09   OFF -2475
OPTION11_10   OFF -1548
OPTION12_03   OFF  -781
OPTION12_06   OFF -2489
OPTOIN12_10   OFF -4578
OPTION13_02   OFF -4551
ZWCOMPK           0.000 165000.000 96400.000 19900.000 13400.000 23300.000     0.000     0.000     0.000     0.000     0.000     0.000 -1092164097
ZCPUCACHE     OFF -2572
ZULIMIT         180.000    75.000   250.000   360.000   130.000   360.000    10.000    10.000    10.000    10.000    10.000    10.000  -106766337
ZLLIMIT        -180.000   -60.000  -120.000  -360.000  -130.000  -360.000   -10.000   -10.000   -10.000   -10.000   -10.000   -10.000  2058534911
ZSETACCEL       1.000   1.000   1.000   1.000   1.000   1.000   1.000   1.000   1.000   1.000   1.000   1.000   759169023
ZSETDECEL       1.000   1.000   1.000   1.000   1.000   1.000   1.000   1.000   1.000   1.000   1.000   1.000   759169023
ZSETENVCHK      1.000   1.000   1.000   1.000   1.000   1.000   1.000   1.000   1.000   1.000   1.000   1.000   759169023
ZSETSPCHK       1.000   1.000   1.000   1.000   1.000   1.000   1.000   1.000   1.000   1.000   1.000   1.000   759169023
ZSETENVCHKTCH    1.000   1.000   1.000   1.000   1.000   1.000   1.000   1.000   1.000   1.000   1.000   1.000   759169023
ZSETKVFF        0.300   0.310   0.310   0.300   0.320   0.260   0.300   0.300   0.300   0.300   0.300   0.300   973078548
ZSETKAFF        0.600   0.620   0.620   0.600   0.640   0.520   0.600   0.600   0.600   0.600   0.600   0.600   847249428
ZSAVEMR               145        1422       16539       23573         152       53076           0           0           0           0           0           0    -1423606
ZRADIUS        3214.000  1100.000  1864.000   564.000   564.000   279.000     1.000     1.000     1.000     1.000     1.000     1.000  -417744897
ZJTSPEED        110.000   110.000   110.000   135.000   135.000   210.000  1000.000    10.000    10.000    10.000    10.000    10.000  -161447937
ZACCTIME          0.100     0.230     0.300     0.201     0.258     0.460     0.850     0.850     0.850     0.850     0.850     0.850   864582378
ZDEG_BIT       2.6050100000e-04  2.5850200000e-04 -2.6067800000e-04  3.2384200000e-04  3.1485300000e-04 -5.2922600000e-04  2.5101500000e-03  2.5101500000e-03  2.5101500000e-03  2.5101500000e-03  2.5101500000e-03  2.5101500000e-03  1184237523   771769203
LIN_COOP        0   0   0   0   0   0          -1
LIN_CO_TYPE     0   0   0   0   0   0          -1
COUPFA7        0.0000000000e+00          -1          -1
JTCHANNEL       0   1   2   3   4   5  -1  -1  -1  -1  -1  -1        -136
SRV_PARAM       0   0   0   0   0   0   0   0   0   0   0   0          -1
CD_ID_CODE     35  35  35  35  35  35  35  35  35  35  35  35       -6301
ZSVPRM_GA01        1310     16384     19660     19660     16384      2048    12.000     5.207     0.600     0.138        16         8      4096  -840667032
ZSVPRM_GA02        1310     16384     19660     19660     16384      2048    12.000     5.643     0.600     0.134        16         8      4096  -840667857
ZSVPRM_GA03        1310     16384     19660     19660     16384      2048    12.000     5.215     0.600     0.120        16         8      6144  -844599177
ZSVPRM_GA04        1310     16384     19660     19660     16384      2048    12.000     4.600     0.600     0.160        16         8      6144  -844598067
ZSVPRM_GA05        1310     16384     19660     19660     16384      2048    12.000     4.900     0.600     0.160        16         8      6144  -844598637
ZSVPRM_GA06        1310     16384     19660     19660     16384      2048    12.000     4.300     0.600     0.100        16         8      4096  -840665217
ZENDLESS               0          0          0          0          -1          -1
ZSIGWAIT        0          -1
ZCVAXIS              -1          14
ZSETMAXDAOUT  1         -16
ZSETTPNK          1.100     1.100     0.900     0.900
ZSETCYCOVER   2         -31
SGAXIS_NO     0
ZGACCEF       1.0000 1.0000 1.0000 1.0000 1.0000 0.8000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000   762314748
ZGDECEF       0.9000 1.0000 1.0000 1.0000 0.9000 0.8000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000   765460473
ZGFRIST       2496.2261 2001.3480 2001.3480 1901.6770 1942.2629 471.7320 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000  -476490672
ZGFRID0       2496.2261 2001.3480 2001.3480 1827.8370 1779.8311 471.7320 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000  -472861542
ZGFRID1       4750.0791 3636.2671 3636.2671 3.2300 1.6910 171.0240 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000  -497748372
TIMECONST       2   2   2   1   2   2   2   2   2   2   2   2        -346
TIMECSTSFT      4   4   4   4   4   4   4   4   4   4   4   4        -721
ZCOLT           130   155   250   125   130   130     0     0     0     0     0     0      -13801
ZCOLR           380   170   260   150   180   150     0     0     0     0     0     0      -19351
ZCOLTJ           15    10    15     8     8    10     0     0     0     0     0     0        -991
ZCOLRJ           30    20    20    10    15    15     0     0     0     0     0     0       -1651
ZSETIPALM       1.000   1.000   1.000   1.000   1.000   1.000   1.000   1.000   1.000   1.000   1.000   1.000   759169023
ZSETIPLMT      -1.000  -1.000  -1.000  -1.000  -1.000  -1.000  -1.000  -1.000  -1.000  -1.000  -1.000  -1.000 -1388314625
ZSETI2ALM       1.070   1.070   1.070   1.070   1.070   1.070   1.070   1.070   1.070   1.070   1.070   1.070   745956978
ZSETIPTIME           32
ZSETMAXPUMP   1         -16
TEACH_LCOOP     0   0   0   0   0   0          -1
ZCVDLYCYC             0
ZARMIDSET     ON   ON    -1498
ZLINSPEED      2500.000  -212320257
ZROTSPEED         2.356  1051344243
ZLINSPEEDPARAM      1.000     1.000     1.000     1.000     1.000     1.000     1.000     1.000     1.000     1.000     1.000     1.000   759169023
ZTQRPA0FW     0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000          -1
ZTQRPA1FW     0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000          -1
ZTQRPPRDFW     2.4000000000e+01  2.4000000000e+01  2.4000000000e+01  2.4000000000e+01  2.4000000000e+01  2.4000000000e+01  2.4000000000e+01  2.4000000000e+01  2.4000000000e+01  2.4000000000e+01  2.4000000000e+01  2.4000000000e+01   961675263          -1
ZTQRPPHFW      0.0000000000e+00  0.0000000000e+00  0.0000000000e+00  0.0000000000e+00  0.0000000000e+00  0.0000000000e+00  0.0000000000e+00  0.0000000000e+00  0.0000000000e+00  0.0000000000e+00  0.0000000000e+00  0.0000000000e+00          -1          -1
ZTQRPA0RV     0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000          -1
ZTQRPA1RV     0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000 0.0000          -1
ZTQRPPRDRV     2.4000000000e+01  2.4000000000e+01  2.4000000000e+01  2.4000000000e+01  2.4000000000e+01  2.4000000000e+01  2.4000000000e+01  2.4000000000e+01  2.4000000000e+01  2.4000000000e+01  2.4000000000e+01  2.4000000000e+01   961675263          -1
ZTQRPPHRV      0.0000000000e+00  0.0000000000e+00  0.0000000000e+00  0.0000000000e+00  0.0000000000e+00  0.0000000000e+00  0.0000000000e+00  0.0000000000e+00  0.0000000000e+00  0.0000000000e+00  0.0000000000e+00  0.0000000000e+00          -1          -1
ZTQRPVELIM    1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000 1.0000   759169023
ZTQRPOUTLM    40.0000 40.0000 40.0000 40.0000 40.0000 40.0000 40.0000 40.0000 40.0000 40.0000 40.0000 40.0000    82837503
ZSETKVILM              0          0          0          0          0          0          0          0          0          0          0          0          -1
ZSETKVI             1000       1000       1000       1000       1000       1000       1000       1000       1000       1000       1000       1000     -180001
ZSETKVI2            1000       1000       1000       1000       1000       1000       1000       1000       1000       1000       1000       1000     -180001
ZSETKVP             1000       1000       1000       1000       1000       1000       1000       1000       1000       1000       1000       1000     -180001
ZSETKPP             1000       1000       1000       1000       1000       1000       1000       1000       1000       1000       1000       1000     -180001
ZLINACCTIME       0.400     0.400  1249902588
ZRADIUS_TEACH   2152.000  1100.000  1864.000   564.000   564.000   564.000     1.000     1.000     1.000     1.000     1.000     1.000  -393875457
ZRADIUS_TEACH2   2152.000  1100.000  1864.000   564.000   564.000   564.000     1.000     1.000     1.000     1.000     1.000     1.000  -393875457
ZSETREFFLT            4         4         4        -181
ZSETREFSIG            2         -31
ZSWITCH ZDISP_GANTI     OFF  -513
ZSWITCH ZKILL_LOAD      ON   -509
ZSWITCH ZLANGUAGE       ON   -507
ZSWITCH ZSAFE_CE        ON   -505
ZSWITCH ZSAFE_STD       OFF  -509
ZSWITCH ZWELD.COMP      OFF  -511
ZSWITCH ZTPLIGHT_ON     OFF  -513
ZSWITCH ZINTFER         OFF  -505
ZSWITCH Z1HS.HAND       OFF  -509
ZSWITCH ZIRMS.OLD       OFF  -509
ZSWITCH ZSPLMT          OFF  -503
ZSWITCH ZWSPACE         OFF  -505
ZSWITCH ZFIXED          OFF  -503
ZSWITCH ZCLAMP_PULSE1   OFF  -517
ZSWITCH ZHYBINT         OFF  -505
ZSWITCH ZHYBINT.AUTO    OFF  -515
ZSWITCH ZHYBINT.TCH     OFF  -513
ZSWITCH ZIGNCONF        OFF  -507
ZSWITCH ZIGNCONF.TCH    OFF  -515
ZSWITCH ZMODEM          OFF  -503
ZSWITCH ZCINT           ON   -499
ZSWITCH ZCINT.FIXSP     OFF  -513
ZSWITCH ZCINT.AUTO      ON   -509
ZSWITCH ZCINT.C1C1      OFF  -511
ZSWITCH ZCINT.SHORT     ON   -511
ZSWITCH ZNEWPLAN        ON   -505
ZSWITCH ZNEWPLAN.OAT    ON   -513
ZSWITCH ZNEWPLAN.NXT    ON   -513
ZSWITCH ZNSP            ON   -497
ZSWITCH ZJINT           OFF  -501
ZSWITCH ZYCP            ON   -497
ZSWITCH ZSINACC         OFF  -505
ZSWITCH ZCHKCOIN        ON   -505
ZSWITCH ZLADMAXKAI      ON   -509
ZSWITCH ZDECKAI         ON   -503
ZSWITCH ZCINTVEKAI      OFF  -511
ZSWITCH ZCYCFIX         ON   -503
ZSWITCH ZCMNDJUMPDSP    OFF  -515
ZSWITCH ZSOUKOU         ON   -503
ZSWITCH ZREFFLT         ON   -503
ZSWITCH ZFIRFLT         OFF  -505
ZSWITCH ZREFKAI         ON   -503
ZSWITCH ZREFSIG         OFF  -505
ZSWITCH ZESTOPHOLD3     OFF  -513
ZSWITCH ZESTOPHOLD4     ON   -511
ZSWITCH ZRELATE         OFF  -505
ZSWITCH ZEMGRESET       OFF  -509
ZSWITCH ZCHKHOME        OFF  -507
ZSWITCH ZADFIX          OFF  -503
ZSWITCH ZADCAL          OFF  -503
ZSWITCH ZIPEAKWRN       ON   -507
ZSWITCH ZI2MONWRN       ON   -507
ZSWITCH ZCMNDJ_STOP     ON   -511
ZSWITCH ZSAFE_USA       OFF  -509
ZSWITCH ZVSW_CAMERA     OFF  -513
ZSWITCH ZPULSE256       OFF  -509
ZSWITCH ZEXIST_TP       ON   -507
ZSWITCH ZEXIST1_PNL     ON   -511
ZSWITCH ZEXIST2_PNL     OFF  -513
ZSWITCH ZEXIST_ENC      OFF  -511
ZSWITCH ZEXIST1_SH      ON   -509
ZSWITCH ZEXIST2_SH      ON   -509
ZSWITCH ZEXIST3_SH      ON   -509
ZSWITCH ZEXIST4_SH      ON   -509
ZSWITCH ZENA_REC        ON   -505
ZSWITCH ZTP_TRIAL       OFF  -509
ZSWITCH ZTP.TRG.SW      ON   -509
ZSWITCH ZMASK1_SV       OFF  -509
ZSWITCH ZMASK2_SV       OFF  -509
ZSWITCH ZMASK1_LS       OFF  -509
ZSWITCH ZMASK2_LS       OFF  -509
ZSWITCH ZMASK1_PU       ON   -507
ZSWITCH ZMASK2_PU       ON   -507
ZSWITCH ZCONSTAD        OFF  -507
ZSWITCH ZSCP            OFF  -499
ZSWITCH ZFCP            OFF  -499
ZSWITCH ZF2CP           OFF  -501
ZSWITCH ZMCP            OFF  -499
ZSWITCH ZTCH_TOPPEN     OFF  -513
ZSWITCH ZCHK_TOPPEN     OFF  -513
ZSWITCH ZREP_TOPPEN     OFF  -513
ZSWITCH ZTCH2_TOPPEN    ON   -513
ZSWITCH ZCHK2_TOPPEN    ON   -513
ZSWITCH ZREP2_TOPPEN    ON   -513
ZSWITCH ZREP3_TOPPEN    OFF  -515
ZSWITCH ZTPPORT         OFF  -505
ZSWITCH ZINSIG          OFF  -503
ZSWITCH ZEXTSW          OFF  -503
ZSWITCH ZADCCAL         OFF  -505
ZSWITCH Z_IVAR          OFF  -503
ZSWITCH ZNCHVAR         OFF  -505
ZSWITCH ZPCMDFLT        OFF  -507
ZSWITCH ZPCMD3FLT       ON   -507
ZSWITCH ZNOTCH          OFF  -503
ZSWITCH ZSRVCHK         ON   -503
ZSWITCH ZCHKASCYC       ON   -507
ZSWITCH ZRESTORE        ON   -505
ZSWITCH ZCALJL          ON   -501
ZSWITCH ZCALGRAV        ON   -505
ZSWITCH ZCALFS          ON   -501
ZSWITCH ZCALKSPVP       ON   -507
ZSWITCH ZCALKOBS        ON   -505
ZSWITCH ZPDFF           ON   -499
ZSWITCH ZVDFF           ON   -499
ZSWITCH ZCOLDET         OFF  -505
ZSWITCH ZATGEF          OFF  -503
ZSWITCH ZTQRP           OFF  -501
ZSWITCH ZNOENVCHK       OFF  -509
ZSWITCH ZTQSVINIT       OFF  -509
ZSWITCH ZIBS.WORD       OFF  -509
ZSWITCH ZIBS.RVS        OFF  -507
ZSWITCH ZCHKROT6        ON   -505
ZSWITCH ZWX.OLD         OFF  -505
ZSWITCH ZLINACC         OFF  -505
ZSWITCH ZMTN1FLT        ON   -505
ZSWITCH ZREFSP          OFF  -503
.END
.SYSDATA
REG_POINT       0
BASE              0.000     0.000     0.000     0.000     0.000     0.000
ENV_DATA              0   0   0   0   0   0   0   0   0   0
ENV2_DATA       0   0   0   0   0   0   0   0   0   0
TPNL_DSPSEL   255 255   0 255   0   0   0 255   0   0
CHECK_SPEC      1   1   1   1   1   1   1   1   0
DEFSIG_I EXT_MTRON       OFF  1032
DEFSIG_I EXT_ERR_RESET   OFF  1031
DEFSIG_I EXT_CYC_START   OFF  1030
DEFSIG_I EXT_PROGRM_RST  OFF  2009
DEFSIG_I EXT_JUMP        OFF
         JUMP_ON             0
         JUMP_OFF            0
         JUMP_ST             0
DEFSIG_I EXT_RPS         OFF
         RPS_ON           2006
         RPS_ST           2007
         RPS_CODE            0  2008     0
DEFSIG_I EXT_IT          OFF     0
DEFSIG_I EXT_SLOW_REP.   OFF     0
DEFSIG_I GUN_ACCEPT      OFF     0
DEFSIG_I SENSING         OFF     0
DEFSIG_I SENS_SPD        OFF     0
DEFSIG_I EXT_HOLD_RESET  OFF  1031
DEFSIG_I EXT_PH_PULSE.   OFF     0
DEFSIG_I I/F_PAGE1       ON   2035
DEFSIG_I I/F_PAGE2       OFF     0
DEFSIG_I OPTIMIZE_MODE   OFF     0
DEFSIG_I EXT_REPEAT      OFF     0
DEFSIG_I EXT_GUN1_OUT    OFF     0
DEFSIG_I EXT_GUN2_OUT    OFF     0
DEFSIG_I EXT_GUN3_OUT    OFF     0
DEFSIG_I EXT_GUN4_OUT    OFF     0
DEFSIG_O MOTOR_ON        OFF    32
DEFSIG_O ERROR           ON      3
DEFSIG_O AUTOMATIC       OFF    30
         CND_RUN         ON 
         CND_EHOLD       OFF
         CND_REPEAT      ON 
         CND_CCONT       ON 
         CND_SCONT       ON 
         CND_TLOCK       OFF
         CND_CYCLE       OFF
         CND_RGSO        OFF
         CND_DRYOFF      OFF
DEFSIG_O CYCLE_START     ON      2
DEFSIG_O TEACH_MODE      ON      5
DEFSIG_O HOME1           OFF    27
DEFSIG_O HOME2           OFF     0
DEFSIG_O POWER_ON        OFF     0
DEFSIG_O RGSO            OFF     0
DEFSIG_O RPS             OFF     0
DEFSIG_O WORK_SPACE_1    OFF     0
DEFSIG_O WORK_SPACE_2    OFF     0
DEFSIG_O WORK_SPACE_3    OFF     0
DEFSIG_O WORK_SPACE_4    OFF     0
DEFSIG_O WORK_SPACE_5    OFF     0
DEFSIG_O WORK_SPACE_6    OFF     0
DEFSIG_O WORK_SPACE_7    OFF     0
DEFSIG_O WORK_SPACE_8    OFF     0
DEFSIG_O WORK_SPACE_9    OFF     0
DEFSIG_O OUT_SPRAY_OK    OFF     0
DEFSIG_O OUT_RPS_OK      OFF     0
DEFSIG_O OUT_RPS_PROG    OFF
         OUT_RPS_CODE        0     0
DEFSIG_O OUT_DISIG       OFF     4    13  1013
DEFSIG_O RB2AUTOMATIC    OFF     0
DEFSIG_O RB2HOME1        OFF     0
DEFSIG_O RB2HOME2        OFF     0
DEFSIG_O RB2WORK_SPACE_1  OFF     0
DEFSIG_O RB2WORK_SPACE_2  OFF     0
DEFSIG_O RB2WORK_SPACE_3  OFF     0
DEFSIG_O RB2WORK_SPACE_4  OFF     0
DEFSIG_O RB2WORK_SPACE_5  OFF     0
DEFSIG_O RB2WORK_SPACE_6  OFF     0
DEFSIG_O RB2WORK_SPACE_7  OFF     0
DEFSIG_O RB2WORK_SPACE_8  OFF     0
DEFSIG_O RB2WORK_SPACE_9  OFF     0
DEFSIG_O CVF_LS2         OFF     0
DEFSIG_O CVF_PH2         OFF     0
DEFSIG_O ENCBRK_OFF      OFF    15
DEFSIG_O IBS_SLA_ERR     OFF     0
DEFSIG_O MECHA_WARNING   OFF     0
DEFSIG_O CVSIMU          OFF     0
DEFSIG_O STEP_CODE       OFF
         OUT_STEP_CNT        0     0
RMTOUT        0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0          -1
RMTIN         0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0          -1
FIBUS_MASTER    1
FIBUS_SLAVE     2
CONFIGVISION    0   0
ARM_OX_TOP            1
ARM_OX_NUM            0
ARM_WX_TOP         1001
ARM_WX_NUM            0
TPNL_GRPH       0   0   0   0   0
SYS_BASE          0.000     0.000     0.000     0.000     0.000     0.000
TOOL              0.000   -75.700   219.200    90.000    45.000   -90.000
FTOOL             0.000     0.000     0.000     0.000     0.000     0.000
DFF             165.000     0.000     0.000     0.000     0.000     0.000     0.000
SETSIO            19200         2
PROTOCOL          1.000     1.000     1.000         3         3         0
SET2SIO            9600         2
CVDIRECT          1.000     0.000
CVSCALE        0.500000
CVMAXSPD        500.000
CVGAIN           50.000
CVPITCH           0.000
CVFNONPITCH       0.000
CVFLS2            0.000
CVFPH2            0.000
CVFMAX            0.000
CVSMAX         1000.000
CVSPEED         100.000
SETOUTDA[1]   3000 8 1 10.0 0.0 0 0 0 0 0
SETOUTDA[2]   3000 8 1 10.0 0.0 0 0 0 0 0
SETOUTDA[3]   3000 8 1 10.0 0.0 0 0 0 0 0
SETOUTDA[4]   3000 8 1 10.0 0.0 0 0 0 0 0
SETOUTDA[5]   3000 8 1 10.0 0.0 0 0 0 0 0
SETOUTDA[6]   3000 8 1 10.0 0.0 0 0 0 0 0
SETOUTDA[7]   3000 8 1 10.0 0.0 0 0 0 0 0
SETOUTDA[8]   3000 8 1 10.0 0.0 0 0 0 0 0
SETOUTDA[9]   3000 8 1 10.0 0.0 0 0 0 0 0
SETOUTDA[10]  3000 8 1 10.0 0.0 0 0 0 0 0
FDD_SPD_BAU          96        96
FDD_SPD_STOP          2         2
FDD_PORT              2
FDD_SWITCH            0
ARMPROTOCOL        5000       100         3
SWITCH CHECK.HOLD      OFF
SWITCH CP              ON 
SWITCH CYCLE.STOP      OFF
SWITCH OX.PREOUT       ON 
SWITCH PREFETCH.SIGINS OFF
SWITCH QTOOL           ON 
SWITCH REP_ONCE        OFF
SWITCH RPS             OFF
SWITCH STP_ONCE        OFF
SWITCH AFTER.WAIT.TMR  OFF
SWITCH FLEXCOMP        OFF
SWITCH SPOT_OP         OFF
SWITCH MESSAGES        ON 
SWITCH SCREEN          ON 
SWITCH AUTOSTART.PC    ON 
SWITCH AUTOSTART2.PC   ON 
SWITCH AUTOSTART3.PC   ON 
SWITCH ERRSTART.PC     OFF
SWITCH DISPIO_01       OFF
SWITCH HOLD.STEP       OFF
SWITCH WS_COMPOFF      OFF
SWITCH WS.ZERO         OFF
SWITCH SLOW_START      OFF
SWITCH ABS.SPEED       OFF
SWITCH PLC.CHECK       OFF
SWITCH FLOWRATE        OFF
SWITCH FLOWRATE2       OFF
SWITCH FLOWRATE3       OFF
SWITCH FLOWRATE4       OFF
.END
.AUXDATA
FTOOL1            0.000     0.000     0.000     0.000     0.000     0.000
FTOOL2            0.000     0.000     0.000     0.000     0.000     0.000
FTOOL3            0.000     0.000     0.000     0.000     0.000     0.000
FTOOL4            0.000     0.000     0.000     0.000     0.000     0.000
FTOOL5            0.000     0.000     0.000     0.000     0.000     0.000
FTOOL6            0.000     0.000     0.000     0.000     0.000     0.000
FTOOL7            0.000     0.000     0.000     0.000     0.000     0.000
FTOOL8            0.000     0.000     0.000     0.000     0.000     0.000
FTOOL9            0.000     0.000     0.000     0.000     0.000     0.000
TOOL1             0.000     0.000     0.000     0.000     0.000     0.000
DFF1            165.000     0.000     0.000     0.000
TOOL2             0.000     0.000     0.000     0.000     0.000     0.000
DFF2            165.000     0.000     0.000     0.000
TOOL3             0.000     0.000     0.000     0.000     0.000     0.000
DFF3            165.000     0.000     0.000     0.000
TOOL4             0.000     0.000     0.000     0.000     0.000     0.000
DFF4            165.000     0.000     0.000     0.000
TOOL5             0.000     0.000     0.000     0.000     0.000     0.000
DFF5            165.000     0.000     0.000     0.000
TOOL6             0.000     0.000     0.000     0.000     0.000     0.000
DFF6            165.000     0.000     0.000     0.000
TOOL7             0.000     0.000     0.000     0.000     0.000     0.000
DFF7            165.000     0.000     0.000     0.000
TOOL8             0.000     0.000     0.000     0.000     0.000     0.000
DFF8            165.000     0.000     0.000     0.000
TOOL9             0.000     0.000     0.000     0.000     0.000     0.000
DFF9            165.000     0.000     0.000     0.000
WORK1             0.000     0.000     0.000     0.000     0.000     0.000
WORK2             0.000     0.000     0.000     0.000     0.000     0.000
WORK3             0.000     0.000     0.000     0.000     0.000     0.000
WORK4             0.000     0.000     0.000     0.000     0.000     0.000
WORK5             0.000     0.000     0.000     0.000     0.000     0.000
WORK6             0.000     0.000     0.000     0.000     0.000     0.000
WORK7             0.000     0.000     0.000     0.000     0.000     0.000
WORK8             0.000     0.000     0.000     0.000     0.000     0.000
WORK9             0.000     0.000     0.000     0.000     0.000     0.000
UP-LIM          180.000    75.000   250.000   360.000   130.000   360.000    10.000    10.000    10.000    10.000    10.000    10.000
LO-LIM         -180.000   -60.000  -120.000  -360.000  -130.000  -360.000   -10.000   -10.000   -10.000   -10.000   -10.000   -10.000
ACCUR             1.000    10.000    50.000   100.000
SPEED            10.000    20.000    30.000    40.000    50.000    60.000    70.000    80.000    90.000   100.000
SLOW_REPEAT      10.000
CHECK_SPEED      10.000    80.000   250.000
TEACH_SPEED      10.000    80.000   250.000     0.016     0.500     1.000
TIMER             0.000     0.100     0.200     0.300     0.400     0.500     0.600     0.700     0.800     0.900
SENSING_SPEED     10.000     0.000 255
FLOWGAIN        100.000
FLOWGAIN2       100.000
FLOWGAIN3       100.000
FLOWGAIN4       100.000
LINEAR1           0.00     0.50     1.00     1.50     2.00     2.50     3.00     3.50     4.00     4.50     5.00     5.50     6.00     6.50     7.00     7.50     8.00     8.50     9.00     9.50    10.00
LINEAR2              0     1500     3000     4500     6000     7500     9000    10500    12000    13500    15000    16500    18000    19500    21000    22500    24000    25500    27000    28500    30000
LINEAR1_2         0.00     0.50     1.00     1.50     2.00     2.50     3.00     3.50     4.00     4.50     5.00     5.50     6.00     6.50     7.00     7.50     8.00     8.50     9.00     9.50    10.00
LINEAR2_2            0     1500     3000     4500     6000     7500     9000    10500    12000    13500    15000    16500    18000    19500    21000    22500    24000    25500    27000    28500    30000
LINEAR1_3         0.00     0.50     1.00     1.50     2.00     2.50     3.00     3.50     4.00     4.50     5.00     5.50     6.00     6.50     7.00     7.50     8.00     8.50     9.00     9.50    10.00
LINEAR2_3            0     1500     3000     4500     6000     7500     9000    10500    12000    13500    15000    16500    18000    19500    21000    22500    24000    25500    27000    28500    30000
LINEAR1_4         0.00     0.50     1.00     1.50     2.00     2.50     3.00     3.50     4.00     4.50     5.00     5.50     6.00     6.50     7.00     7.50     8.00     8.50     9.00     9.50    10.00
LINEAR2_4            0     1500     3000     4500     6000     7500     9000    10500    12000    13500    15000    16500    18000    19500    21000    22500    24000    25500    27000    28500    30000
OUTSPED           0.00  2000.00    10.00     0.00      0 3000   8   1      0 0
OUTSPED2          0.00  2000.00    10.00     0.00      0 3000   8   1      0 0
OUTSPED3          0.00  2000.00    10.00     0.00      0 3000   8   1      0 0
OUTSPED4          0.00  2000.00    10.00     0.00      0 3000   8   1      0 0
1HOME             0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000    10.000
2HOME             0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000     0.000    10.000
WORK_SPACE_1      0.000     0.000     0.000     0.000     0.000     0.000         0
WORK_SPACE_2      0.000     0.000     0.000     0.000     0.000     0.000         0
WORK_SPACE_3      0.000     0.000     0.000     0.000     0.000     0.000         0
WORK_SPACE_4      0.000     0.000     0.000     0.000     0.000     0.000         0
WORK_SPACE_5      0.000     0.000     0.000     0.000     0.000     0.000         0
WORK_SPACE_6      0.000     0.000     0.000     0.000     0.000     0.000         0
WORK_SPACE_7      0.000     0.000     0.000     0.000     0.000     0.000         0
WORK_SPACE_8      0.000     0.000     0.000     0.000     0.000     0.000         0
WORK_SPACE_9      0.000     0.000     0.000     0.000     0.000     0.000         0
OUTFLOW_HSPARA[1]       0      0      0      0      0      0      0      0
OUTFLOW_HSPARA[2]       0      0      0      0      0      0      0      0
CL_APPLI        2   2   2   2   0   0   0   0
CL_CND[1]     0.000 0.300   1 PG90
CL_CND[2]     0.000 0.300   2 PG90
CL_CND[3]     0.000 0.300   3 PG90
CL_CND[4]     0.000 0.300   4 PG90
CL_CND[5]     0.000 0.300   5 PG90
CL_CND[6]     0.000 0.300   6 PG90
CL_CND[7]     0.000 0.300   7 PG90
CL_CND[8]     0.000 0.300   8 PG90
CL_CND[9]     0.000 0.300   1 PG90
BEADCC             0.0      0.0      0.0      0.0      0.0      0.0      0.0      0.0      0.0      0.0
CL_CND_GCLO     0.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00
CL_CND_GDT      0.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00   0.00
CL_CND_T3     0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00
BEADPAR       0.0 100.0 100.0 100.0 100.0 100.0 100.0 100.0 100.0 100.0
OUTSPFLG      0 0 0 0 0 0 0 0 0 0
CL_SPW[1]       1    22     0     0     0     0     0     0
CL_SPW[2]       1    22     0     0     0     0     0     0
CL_SPW[3]       1    22     0     0     0     0     0     0
CL_SPW[4]       1    22     0     0     0     0     0     0
CL_SPW[5]       1    22     0     0     0     0     0     0
CL_SPW[6]       1    22     0     0     0     0     0     0
CL_SPW[7]       1    22     0     0     0     0     0     0
CL_SPW[8]       1    22     0     0     0     0     0     0
SPW_CTRL[1]      23  0.000    17     4   0  1029   3     0     0
SPW_CTRL[2]      23  0.000    17     4   0  1029   3     0     0
SPW_CTRL[3]      23  0.000    17     4   0  1029   3     0     0
SPW_CTRL[4]      23  0.000    17     4   0  1029   3     0     0
SPW_CTRL[5]      23  0.000    17     4   0  1029   3     0     0
SPW_CTRL[6]      23  0.000    17     4   0  1029   3     0     0
SPW_CTRL[7]      23  0.000    17     4   0  1029   3     0     0
SPW_CTRL[8]      23  0.000    17     4   0  1029   3     0     0
SPW_GUN[1]      1   0  0.500  1.000   0   0  0.500  0.200
SPW_GUN[2]      1   0  0.500  1.000   0   0  0.500  0.200
SPW_GUN[3]      1   0  0.500  1.000   0   0  0.500  0.200
SPW_GUN[4]      1   0  0.500  1.000   0   0  0.500  0.200
SPW_GUN[5]      1   0  0.500  1.000   0   0  0.500  0.200
SPW_GUN[6]      1   0  0.500  1.000   0   0  0.500  0.200
SPW_GUN[7]      1   0  0.500  1.000   0   0  0.500  0.200
SPW_GUN[8]      1   0  0.500  1.000   0   0  0.500  0.200
CL_HDL[1]        10     9
CL_HDL[2]        12    11
CL_HDL[3]         0    13
CL_HDL[4]         0    15
CL_HDL[5]        24     0
CL_HDL[6]        24     0
CL_HDL[7]        24     0
CL_HDL[8]        24     0
CL_GUN[1]         9
CL_GUN[2]        10
CL_GUN[3]         0
CL_GUN[4]         0
OX_SPEC_1      0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
OX_SPEC_2      0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
INSPECTION     0 -1 -1 -1 -1
COLT                  0         0         0         0         0         0
COLTSW        255
COLTJ                 0         0         0         0         0         0
COLTJSW       255
COLR                  0         0         0         0         0         0
COLRSW        255
COLRJ                 0         0         0         0         0         0
COLRJSW       255
LOAD1           165.000     0.000     0.000     0.000     0.000     0.000     0.000
LOAD2           165.000     0.000     0.000     0.000     0.000     0.000     0.000
LOAD3           165.000     0.000     0.000     0.000     0.000     0.000     0.000
LOAD4           165.000     0.000     0.000     0.000     0.000     0.000     0.000
LOAD5           165.000     0.000     0.000     0.000     0.000     0.000     0.000
LOAD6           165.000     0.000     0.000     0.000     0.000     0.000     0.000
LOAD7           165.000     0.000     0.000     0.000     0.000     0.000     0.000
LOAD8           165.000     0.000     0.000     0.000     0.000     0.000     0.000
LOAD9           165.000     0.000     0.000     0.000     0.000     0.000     0.000
ARM_LOAD          0.000     0.000
BASE_LOAD         0.000     0.000     0.000     0.000     0.000
AUTO_SAVE     ,0,0,0,0,0,0,0,0,10,0,0,0,0,0,0,0
LAN_HOST_IPAD   192.168.0.1
LAN_HOST_USER   
LAN_HOST_PSW   
LAN_HOST_PATH   ,,
LAN_HOST_ACNT   
LAN_HOST_ASCBIN    0
LAN_HOST_EUSJ    0
LAN_HOST_FNP    1
LAN_HOST_OS    0,10,0,WIN32  1,10,1,UNIX  2,10,0,Windows_NT  3,1,0,  4,1,0,  5,1,0,  6,1,0,  7,1,0,  8,1,0,  9,1,0,
AUX_DPASSWD     1
AUX_CHOOSE    1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1
COLRTH1           0     0     0     0     0     0     0     0     0     0     0     0
COLRTH2           0     0     0     0     0     0     0     0     0     0     0     0
COLRTH3           0     0     0     0     0     0     0     0     0     0     0     0
COLRTH4           0     0     0     0     0     0     0     0     0     0     0     0
COLRTH5           0     0     0     0     0     0     0     0     0     0     0     0
COLRTH6           0     0     0     0     0     0     0     0     0     0     0     0
COLRTH7           0     0     0     0     0     0     0     0     0     0     0     0
COLRTH8           0     0     0     0     0     0     0     0     0     0     0     0
COLRTH9           0     0     0     0     0     0     0     0     0     0     0     0
COLJTH1           0     0     0     0     0     0     0     0     0     0     0     0
COLJTH2           0     0     0     0     0     0     0     0     0     0     0     0
COLJTH3           0     0     0     0     0     0     0     0     0     0     0     0
COLJTH4           0     0     0     0     0     0     0     0     0     0     0     0
COLJTH5           0     0     0     0     0     0     0     0     0     0     0     0
COLJTH6           0     0     0     0     0     0     0     0     0     0     0     0
COLJTH7           0     0     0     0     0     0     0     0     0     0     0     0
COLJTH8           0     0     0     0     0     0     0     0     0     0     0     0
COLJTH9           0     0     0     0     0     0     0     0     0     0     0     0
.END
.INTER_PANEL
3,8,3,4,7
8,3,"   LUCE","OFF     ON"," "," ",4,2,0,17,0,0
11,8,1,4,4
25,3,"RUN   HOLD"," "," "," ",4,2,0,24,0,0
27,8,2,4,5
32,3," SCARICO"," NASTRO"," PALLET"," ",4,4,0,2036,-1,0
33,5," Tornio 1","MdrAperto "," "," ",2,9,2020
34,5," Tornio 1","MdrChiuso "," "," ",2,9,2021
36,5," Tornio 2","MdrAperto "," "," ",2,9,2024
37,5," Tornio 2","MdrChiuso "," "," ",2,9,2025
40,5,"  PRESSA","  Aperta"," "," ",2,9,2028
41,5," Tornio 1"," Scarico"," "," ",2,9,2022
42,5," Tornio 1"," Carico"," "," ",2,9,2023
44,5," Tornio 2"," Scarico"," "," ",2,9,2026
45,5," Tornio 2"," Carico"," "," ",2,9,2027
48,5,"  PRESSA"," in ciclo"," "," ",2,9,2029
57,1," "," Ripresa1","  Apri"," ",2,5,2037,0
58,1," "," Ripresa1","  Chiudi"," ",2,5,2038,0
59,1," "," Ripresa2","  Apri"," ",2,5,2039,0
60,1," "," Ripresa2","  Chiudi"," ",2,5,2040,0
.END
.PROGRAM abo()
;Abortisce i prg paralleli.
  PCABORT 2: 
  PCABORT 3: 
  RETURN
.END
.PROGRAM altezza1()
;Controlla la quota Z attiva nella pallettizzazione 1.
  alt_piano1 = piano1*(int_piano1+int_falda1)
  IF alt_piano1>=altezza_max THEN
    TYPE "Superato limite altezza massima pallet carico !!!"
    TYPE "Altezza attiva : ",alt_piano1," Soglia consentita : ",altezza_max
    BREAK
    HALT
  END
  RETURN
.END
.PROGRAM apri_mdr_tor1()
;Richiesta apertura mandrino TORNIO1.
  SIGNAL out1_tornio1,-out2_tornio1 ;rich. apert. morse
5
  TIMER (1) = 0
10
  IF SIG(in1_tornio1,-in2_tornio1) GOTO 20
  IF TIMER(1)<5 GOTO 10
  allarme_m = 106 ;mandrino tornio1 non aperto
  CALL master_all
  GOTO 5
20
  SIGNAL -out1_tornio1,-out2_tornio1
  TWAIT t_apri_mdr1
  RETURN
.END
.PROGRAM apri_mdr_tor2()
;Richiesta apertura mandrino TORNIO2.
  SIGNAL out1_tornio2,-out2_tornio2 ;rich. apert. morse
5
  TIMER (1) = 0
10
  IF SIG(in1_tornio2,-in2_tornio2) GOTO 20
  IF TIMER(1)<5 GOTO 10
  allarme_m = 126 ;mandrino tornio2 non aperto
  CALL master_all
  GOTO 5
20
  SIGNAL -out1_tornio2,-out2_tornio2
  TWAIT t_apri_mdr2
  RETURN
.END
.PROGRAM apricamera()
;Apertura sportello telecamera.
  SIGNAL yv4_1
  TIMER (1) = 0
10
  TWAIT 0.5 ;TOGLIERE DOPO SOSTITUZIONE FC
  GOTO 20 ;TOGLIERE DOPO SOSTITUZIONE FC
  IF SIG(sq10,-sq11) GOTO 20
  IF TIMER(1)>5 THEN
    allarme_m = 10  ;allarme sportello non aperto
    CALL master_all
  END
  GOTO 10
20
  RETURN
.END
.PROGRAM apripinza1()
;Apre la pinza 1
  OPENI 1
  TWAIT t_apre1
  RETURN
.END
.PROGRAM apripinza2()
;Apre la pinza 2
  OPENI 2
  TWAIT t_apre2
  RETURN
.END
.PROGRAM apriripresa1()
;Salita fermo pezzo ripresa1
  SIGNAL -yv7r_1,yv7a_1
  TIMER (1) = 0
10
  IF SIG(sq19,-sq51) GOTO 20
  IF TIMER(1)>10 THEN
    allarme_m = 50 ;allarme fermo pezzo ripresa1 non salito
    CALL master_all
  END
  GOTO 10
20
  RETURN
.END
.PROGRAM apriripresa2()
;Salita fermo pezzo ripresa2
  SIGNAL -yv8r_1,yv8a_1
  TIMER (1) = 0
10
  IF SIG(sq20,-sq50) GOTO 20
  IF TIMER(1)>10 THEN
    allarme_m = 52 ;allarme fermo pezzo ripresa2 non salito
    CALL master_all
  END
  GOTO 10
20
  RETURN
.END
.PROGRAM attesa_c_tor1()
;Attesa TORNIO1 pronto al carico.
  SIGNAL out5_tornio1    ;robot in fuori ingombro
  SIGNAL -out1_tornio1,-out2_tornio1
  SIGNAL -out3_tornio1,-out4_tornio1
5
  TIMER (1) = 0
10
  CALL pausa
  IF SIG(in4_tornio1,-in3_tornio1,in5_tornio1,in6_tornio1) AND chiuso_tor1==0 GOTO 20
  IF TIMER(1)<30 GOTO 10
  IF SIG(-in5_tornio1) THEN
    allarme_m = 104 ;tornio1 non automatico
    GOTO 15
  END
  IF SIG(-in6_tornio1) THEN
    allarme_m = 102 ;porta tornio1 non aperta
    GOTO 15
  END
  allarme_m = 100 ;tornio1 non pronto al carico
15
  CALL master_all
  GOTO 5
20
  SIGNAL -out5_tornio1    ;tolgo il fuori ingombro
  CALL chiudicamera
  RETURN
.END
.PROGRAM attesa_c_tor2()
;Attesa TORNIO2 pronto al carico.
  SIGNAL out5_tornio2    ;robot in fuori ingombro
  SIGNAL -out1_tornio2,-out2_tornio2
  SIGNAL -out3_tornio2,-out4_tornio2
5
  TIMER (1) = 0
10
  CALL pausa
  IF SIG(in4_tornio2,-in3_tornio2,in5_tornio2,in6_tornio2) AND chiuso_tor2==0 GOTO 20
  IF TIMER(1)<30 GOTO 10
  IF SIG(-in5_tornio2) THEN
    allarme_m = 124 ;tornio2 non automatico
    GOTO 15
  END
  IF SIG(-in6_tornio2) THEN
    allarme_m = 122 ;porta tornio2 non aperta
    GOTO 15
  END
  allarme_m = 120 ;tornio2 non pronto al carico
15
  CALL master_all
  GOTO 5
20
  SIGNAL -out5_tornio2     ;tolgo il fuori ingombro
  CALL chiudicamera
  RETURN
.END
.PROGRAM attesa_pressa()
;Attesa PRESSA pronta al carico.
  SIGNAL out2_pressa     ;robot in fuori ingombro
  SIGNAL -out1_pressa
5
  TIMER (1) = 0
10
  CALL pausa
  IF SIG(in1_pressa,-in2_pressa) GOTO 20
  IF TIMER(1)<30 GOTO 10
  IF SIG(-in1_pressa) THEN
    allarme_m = 140 ;pressa non pronta al carico
    GOTO 15
  END
  allarme_m = 142 ;ciclo pressa non terminato
15
  CALL master_all
  GOTO 5
20
  SIGNAL -out2_pressa     ;tolgo il fuori ingombro
  CALL chiudicamera
  RETURN
.END
.PROGRAM attesa_s_tor1()
;Attesa TORNIO1 pronto allo scarico.
  SIGNAL out5_tornio1    ;robot in fuori ingombro
  SIGNAL -out1_tornio1,-out2_tornio1
  SIGNAL -out3_tornio1,-out4_tornio1
5
  TIMER (1) = 0
10
  CALL pausa
  IF SIG(-in4_tornio1,in3_tornio1,in5_tornio1,in6_tornio1) AND chiuso_tor1==0 GOTO 20
  IF TIMER(1)<300 GOTO 10
  IF SIG(-in5_tornio1) THEN
    allarme_m = 104 ;tornio1 non automatico
    GOTO 15
  END
  IF SIG(-in6_tornio1) THEN
    allarme_m = 102 ;porta tornio1 non aperta
    GOTO 15
  END
  allarme_m = 101 ;tornio1 non pronto allo scarico
15
  CALL master_all
  GOTO 5
20
  SIGNAL -out5_tornio1    ;tolgo il fuori ingombro
  CALL chiudicamera
  RETURN
.END
.PROGRAM attesa_s_tor2()
;Attesa TORNIO2 pronto allo scarico.
  SIGNAL out5_tornio2    ;robot in fuori ingombro
  SIGNAL -out1_tornio2,-out2_tornio2
  SIGNAL -out3_tornio2,-out4_tornio2
5
  TIMER (1) = 0
10
  CALL pausa
  IF SIG(-in4_tornio2,in3_tornio2,in5_tornio2,in6_tornio2) AND chiuso_tor2==0 GOTO 20
  IF TIMER(1)<300 GOTO 10
  IF SIG(-in5_tornio2) THEN
    allarme_m = 124 ;tornio2 non automatico
    GOTO 15
  END
  IF SIG(-in6_tornio2) THEN
    allarme_m = 122 ;porta tornio2 non aperta
    GOTO 15
  END
  allarme_m = 121 ;tornio2 non pronto allo scarico
15
  CALL master_all
  GOTO 5
20
  SIGNAL -out5_tornio2    ;tolgo il fuori ingombro
  CALL chiudicamera
  RETURN
.END
.PROGRAM ausiliari()
;Logiche elettromeccaniche.
;
  IF accendi==1 AND flash==1 THEN
    SIGNAL lamp_blu
  ELSE
    SIGNAL -lamp_blu
  END
;
  IF SIG(potenza,psp1_1,in_ciclo) THEN
    ciclo_staz = 1
  ELSE
    ciclo_staz = 0
  END
;
  IF on_mot_4==1 OR (SIG(sb2) AND stop_4==0) THEN
    SIGNAL km1
  ELSE
    SIGNAL -km1
  END
;
  IF SIG(in1_tornio1,-in2_tornio1) THEN
    SIGNAL lamp1
  ELSE
    SIGNAL -lamp1
  END
  IF SIG(-in1_tornio1,in2_tornio1) THEN
    SIGNAL lamp2
  ELSE
    SIGNAL -lamp2
  END
  IF SIG(-in4_tornio1,in3_tornio1,in5_tornio1,in6_tornio1) AND chiuso_tor1==0 THEN
    SIGNAL lamp3
  ELSE
    SIGNAL -lamp3
  END
  IF SIG(in4_tornio1,-in3_tornio1,in5_tornio1,in6_tornio1) AND chiuso_tor1==0 THEN
    SIGNAL lamp4
  ELSE
    SIGNAL -lamp4
  END
;
  IF SIG(in1_tornio2,-in2_tornio2) THEN
    SIGNAL lamp5
  ELSE
    SIGNAL -lamp5
  END
  IF SIG(-in1_tornio2,in2_tornio2) THEN
    SIGNAL lamp6
  ELSE
    SIGNAL -lamp6
  END
  IF SIG(-in4_tornio2,in3_tornio2,in5_tornio2,in6_tornio2) AND chiuso_tor2==0 THEN
    SIGNAL lamp7
  ELSE
    SIGNAL -lamp7
  END
  IF SIG(in4_tornio2,-in3_tornio2,in5_tornio2,in6_tornio2) AND chiuso_tor2==0 THEN
    SIGNAL lamp8
  ELSE
    SIGNAL -lamp8
  END
;
  IF SIG(in1_pressa) THEN
    SIGNAL lamp9
  ELSE
    SIGNAL -lamp9
  END
  IF SIG(in2_pressa) THEN
    SIGNAL lamp10
  ELSE
    SIGNAL -lamp10
  END
;
  IF SIG(in_teach,-in_ciclo) THEN
    IF SIG(p1_t,-p2_t,-p3_t,-p4_t) THEN
      SIGNAL -yv7r_1,yv7a_1 ;apre ripresa1
    END
    IF SIG(-p1_t,p2_t,-p3_t,-p4_t) THEN
      SIGNAL yv7r_1,-yv7a_1 ;chiude ripresa1
    END
    IF SIG(-p1_t,-p2_t,p3_t,-p4_t) THEN
      SIGNAL -yv8r_1,yv8a_1 ;apre ripresa2
    END
    IF SIG(-p1_t,-p2_t,-p3_t,p4_t) THEN
      SIGNAL yv8r_1,-yv8a_1 ;chiude ripresa2
    END
  END
;
  RETURN
.END
.PROGRAM autostart.pc()
  CALL definiz_ciclo
10
  CALL check_ciclo
  GOTO 10
.END
.PROGRAM autostart2.pc()
;Programma task parallela 2.
10
  CALL stazione4
  CALL check_ch_tor1
  CALL check_ch_tor2
  CALL stampatempi
  CALL generale
  CALL suona
  CALL win1
  GOTO 10
.END
.PROGRAM autostart3.pc()
;Programma task parallela 3.
  mem_allarme = 0
  f_meno = 0
  g_meno = 0
  flash = 0
  CALL azzera.timer
10
  CALL diagnosi
  CALL lampeggia
  CALL ausiliari
  GOTO 10
.END
.PROGRAM azzera()
;Programma per azzerare le pallettizzazioni.
  TYPE ""
  domanda = -1
  $messaggio = "VUOI AZZERARE LA PRODUZIONE ? : SI <1> - NO <0> "
  DO
    PROMPT $messaggio,domanda 
  UNTIL (domanda==0 OR domanda==1)
  IF domanda==1 THEN
    domanda = -1
    $stampa = "SEI SICURO ? : SI <1> - NO <0> "
    DO
      PROMPT $stampa,domanda 
    UNTIL (domanda==0 OR domanda==1)
  END
  IF domanda==1 THEN
    domanda = -1
    $stampa = "AZZERO PALLET CARICO ? : SI <1> - NO <0> "
    DO
      PROMPT $stampa,domanda 
    UNTIL (domanda==0 OR domanda==1)
    IF domanda==1 THEN
      CALL azzera.pallet1
      TYPE "AZZERATO !!!"
    ELSE
      TYPE "NON AZZERATO !!!"
    END
    domanda = -1
    $stampa = "AZZERO PALLET SCARICO ? : SI <1> - NO <0> "
    DO
      PROMPT $stampa,domanda 
    UNTIL (domanda==0 OR domanda==1)
    IF domanda==1 THEN
      CALL azzera.pallet2
      TYPE "AZZERATO !!!"
    ELSE
      TYPE "NON AZZERATO !!!"
    END
;    domanda = -1
;    $stampa = "AZZERO PALLET BOCCOLE ? : SI <1> - NO <0> "
;    DO
;      PROMPT $stampa,domanda 
;    UNTIL (domanda==0 OR domanda==1)
;    IF domanda==1 THEN
;      CALL azzera.magazzin
;      TYPE "AZZERATO !!!"
;    ELSE
;      TYPE "NON AZZERATO !!!"
;    END
  END
  RETURN
.END
.PROGRAM azzera.magazzin()
;Azzera pallettizzazione in corso sul magazzino.
  rigam = n_rigam
  colonnam = 0
  fine_magazzino = 0
  RETURN
.END
.PROGRAM azzera.pallet1()
;Azzera pallettizzazione in corso pallet 1.
  riga1 = 0
  colonna1 = 0
  piano1 = n_piano1-1
  togli_falda = 0
  fine_pallet1 = 0
  tentacerca = 0
  contr_piano = 0
  RETURN
.END
.PROGRAM azzera.pallet2()
;Azzera pallettizzazione in corso pallet 2.
  riga2 = 0
  colonna2 = n_colonna2-1
  piano2 = 0
  RETURN
.END
.PROGRAM azzera.timer()
;Azzera i timer di stazione all' accensione.
  t_flash = TIMER(0)
  t_suona = TIMER(0)
  tinizio = TIMER(0)
  t_staz0 = TIMER(0)
  t_staz1 = TIMER(0)
  t_staz2 = TIMER(0)
  t_staz3 = TIMER(0)
  t_staz4 = TIMER(0)
  t_staz5 = TIMER(0)
  t_staz6 = TIMER(0)
  t_staz7 = TIMER(0)
  t_staz8 = TIMER(0)
  t_staz9 = TIMER(0)
  t_staz10 = TIMER(0)
  t_staz11 = TIMER(0)
  t_staz12 = TIMER(0)
  t_staz13 = TIMER(0)
  t_staz14 = TIMER(0)
  t_staz15 = TIMER(0)
  t_staz16 = TIMER(0)
  t_staz17 = TIMER(0)
  t_staz18 = TIMER(0)
  t_staz19 = TIMER(0)
  t_staz20 = TIMER(0)
  RETURN
.END
.PROGRAM boccole()
;Prelievo boccole dal magazzino.
  TOOL pinza2
  JMOVE #passaggio
  JMOVE #passaggio1
  n_prelievi = 0
  JMOVE #fuori_magazzino
  ACCURACY 50
  JMOVE #fuori_cerca
;
5
  IF fine_magazzino==1 THEN
    ACCURACY 50
    LMOVE #fuori_cerca
    allarme_m = 5 ;magazzino boccole vuoto
    CALL master_all
    CALL azzera.magazzin
  END
;
  CALL pausa
  CALL check_magazzino
  CALL cerca_boccol
;
  IF t_boccola>0 THEN
    OPEN 2
    ACCURACY 50
    LMOVE #fuori_cerca
    ACCURACY 50
    JMOVE #fuori_magazzino
    CALL prelievo_boccol
    ACCURACY 50
    LMOVE #fuori_magazzino
  END
;
  rigam = rigam-1
  IF rigam<=0 THEN
    rigam = n_rigam
    colonnam = colonnam+1
    IF colonnam>=n_colonnam THEN
      colonnam = 0
      fine_magazzino = 1
    END
  END
;
  IF t_boccola==0 THEN
    n_prelievi = n_prelievi+1
    IF n_prelievi>10 THEN
      n_prelievi = 0
      allarme_m = 82   ;allarme mancanza boccola a tastatore
      CALL master_all
    END
    GOTO 5
  END
;
  CALL carico_boccole
  CLOSE 2
  JMOVE #passaggio1
  JMOVE #passaggio
  RETURN
.END
.PROGRAM c_fcam()
;Programma per autocalcolo vettore Camera->Pezzo.
  TOOL punta
  TYPE "APPRENDERE I PUNTI B0, BX, BY IN TEACH SULLA CARTA DI CALIBRAZIONE"
  TYPE "FATTO?...PREMI CICLO START"
  PAUSE
  POINT fvis = FRAME(b0,bx,by,b0)
  TOOL punta
  POINT fcam = -offset_camera-TRANS(riga1*int_riga1,colonna1*int_col1,(piano1*(int_piano1+int_falda1))+int_euro1,0,0,0)-pallet1+fvis
  RETURN
.END
.PROGRAM c_inter2()
;Programma per calcolare presa interfalda su pallet2.
  TYPE "METTERE IL ROBOT CON ORGANO INTEFALDE IN PINZA"
  TYPE "SUL PIANO DEL PALLET 2 ..."
  TYPE "PREMERE CICLO."
  PAUSE
  TOOL pinza1
  HERE pallet2+TRANS(0,0,(piano2*(int_piano2+int_falda2))+int_euro2,0,0,0)+grip_pal2+offset_inter2
  RETURN
.END
.PROGRAM c_offset_camera()
;Programma per autocalcolo vettore Pallet->Camera
  TOOL punta
  TYPE "POSIZIONARE IL ROBOT SULLA ZONA DI CALIBRAZIONE RISPETTO AL PALLET"
  POINT offset_camera = -TRANS(riga1*int_riga1,colonna1*int_col1,(piano1*(int_piano1+int_falda1))+int_euro1,0,0,0)-pallet1+HERE
  RETURN
.END
.PROGRAM calcola_f()
  CALL calcola_s
  CASE settore OF
   VALUE 1:
    POINT grip[flag] = grip1[flag]
    IF tdebug>0 THEN
      TYPE /C1,">>> GRIP1 ",flag," <<<"
    END
   VALUE 2:
    POINT grip[flag] = grip2[flag]
    IF tdebug>0 THEN
      TYPE /C1,">>> GRIP2 ",flag," <<<"
    END
   VALUE 3:
    POINT grip[flag] = grip3[flag]
    IF tdebug>0 THEN
      TYPE /C1,">>> GRIP3 ",flag," <<<"
    END
   VALUE 4:
    POINT grip[flag] = grip4[flag]
    IF tdebug>0 THEN
      TYPE /C1,">>> GRIP4 ",flag," <<<"
    END
  END
  POINT presa = pallet1+TRANS(offset_falda_x,offset_falda_y,(piano1*(int_piano1+int_falda1))+int_euro1,0,0,0)+offset_camera+fcam+TRANS(x,y,0,0,0,a)+grip[flag]
  RETURN
.END
.PROGRAM calcola_g()
;Calcola grip position.
  POINT offgrip = -presa+ra[flag]
  CALL calcola_s
  IF tutte==1 THEN
    POINT grip1[flag] = offgrip
    POINT grip2[flag] = offgrip
    POINT grip3[flag] = offgrip
    POINT grip4[flag] = offgrip
    TYPE /C1,">>> TUTTE LE GRIP DRITTE SONO UGUALI <<<"
  ELSE
    CASE settore OF
     VALUE 1:
      POINT grip1[flag] = offgrip
      IF tdebug>0 THEN
        TYPE /C1,">>> GRIP1 ",flag," <<<"
      END
     VALUE 2:
      POINT grip2[flag] = offgrip
      IF tdebug>0 THEN
        TYPE /C1,">>> GRIP2 ",flag," <<<"
      END
     VALUE 3:
      POINT grip3[flag] = offgrip
      IF tdebug>0 THEN
        TYPE /C1,">>> GRIP3 ",flag," <<<"
      END
     VALUE 4:
      POINT grip4[flag] = offgrip
      IF tdebug>0 THEN
        TYPE /C1,">>> GRIP4 ",flag," <<<"
      END
    END
  END
  tutte = 0
  RETURN
.END
.PROGRAM calcola_p()
  CALL calcola_s
  CASE settore OF
   VALUE 1:
    POINT grip[flag] = grip1[flag]
    IF tdebug>0 THEN
      TYPE /C1,">>> GRIP1 ",flag," <<<"
    END
   VALUE 2:
    POINT grip[flag] = grip2[flag]
    IF tdebug>0 THEN
      TYPE /C1,">>> GRIP2 ",flag," <<<"
    END
   VALUE 3:
    POINT grip[flag] = grip3[flag]
    IF tdebug>0 THEN
      TYPE /C1,">>> GRIP3 ",flag," <<<"
    END
   VALUE 4:
    POINT grip[flag] = grip4[flag]
    IF tdebug>0 THEN
      TYPE /C1,">>> GRIP4 ",flag," <<<"
    END
  END
  POINT presa = pallet1+TRANS(riga1*int_riga1,colonna1*int_col1,(piano1*(int_piano1+int_falda1))+int_euro1,0,0,0)+offset_camera+fcam+TRANS(x,y,0,0,0,a)+grip[flag]
  RETURN
.END
.PROGRAM calcola_s()
;Calcola settore attivo da angolo.
  IF (a<=45 AND a>=-45) THEN
    settore = 1
  END
  IF (a<=135 AND a>=45) THEN
    settore = 2
  END
  IF (a<=-135 OR a>=135) THEN
    settore = 3
  END
  IF (a<=-45 AND a>=-135) THEN
    settore = 4
  END
  IF tdebug>0 THEN
    TYPE /C1,">>> SETTORE : ",settore," <<<"
  END
  RETURN
.END
.PROGRAM calibra_fasa()
;Programma per rilevare dimensione angolare della razza.
  CALL set.up
  ABS.SPEED ON
  SPEED 5,10 DEG/S ALWAYS
  ACCURACY 1 ALWAYS
  XMOVE HERE+TRANS(0,0,0,0,0,-90) TILL sq21
  BREAK
  STABLE 0.2
  HERE temp
  XMOVE HERE+TRANS(0,0,0,0,0,-90) TILL -sq21
  BREAK
  STABLE 0.2
  ABS.SPEED OFF
  HERE temp+angolo_c
  SPEED 100 ALWAYS
  ACCURACY 500 ALWAYS
  offset_rot = ABS((DEXT(angolo_c,4)+DEXT(angolo_c,6))/2)
  TYPE "Angolo compensazione razza : ",offset_rot
  RETURN
.END
.PROGRAM calibra_mag()
;Questa procedura permette di calibrare il magazzino delle boccole.
;usare tool pinza2 per apprendere i punti
;- m0 = punto origine magazzino.
;- mx = punto asse x magazzino.
;- my = punto asse y magazzino.
  TYPE ""
  TYPE " mx--------------------"
  TYPE " |                    |"
  TYPE " |                    |"
  TYPE " |                    |"
  TYPE " |                    |"
  TYPE " m0------------------my"
  TYPE ""
  TYPE ""
;
  TYPE "CALIBRAZIONE MAGAZZINO BOCCOLE !"
  $stampa = "SEI SICURO ? : SI <1> - NO <0> "
  DO
    PROMPT $stampa,fare 
  UNTIL (fare==0 OR fare==1)
;
  IF fare==1 THEN
;
    $stampa = "QUANTI PEZZI PER COLONNA ? (asse X) "
    $stampa = $stampa+$ENCODE(/F8.1,n_rigam)+" : "
    DO
      PROMPT $stampa,n_rigam 
    UNTIL (n_rigam>=1)
;
    $stampa = "QUANTE COLONNE ? (asse Y) "
    $stampa = $stampa+$ENCODE(/F8.1,n_colonnam)+" : "
    DO
      PROMPT $stampa,n_colonnam 
    UNTIL (n_colonnam>=1)
;
    $stampa = "ALTEZZA PEZZO ? "
    $stampa = $stampa+$ENCODE(/F8.1,int_pianom)+" : "
    DO
      PROMPT $stampa,int_pianom 
    UNTIL (int_pianom>=0)
;
    TOOL pinza2
    POINT magazzino = FRAME(m0,mx,my,m0)
    gg = DISTANCE(mx,m0)
    gg = ABS(gg)
    int_rigam = gg/(n_rigam-1)
    gg = DISTANCE(my,m0)
    gg = ABS(gg)
    int_colm = gg/(n_colonnam-1)
    CALL azzera.magazzin
;
    TYPE ""
    TYPE "  MAGAZZINO : "
    TYPE "  PARAMETRI CALCOLATI : "
    TYPE "  INTERASSE RIGA       asse x = ",int_rigam
    TYPE "  INTERASSE COLONNA    asse y = ",int_colm
    TYPE "  ALTEZZA PEZZO        asse z = ",int_pianom
    TYPE ""
    TYPE "  NUMERO PEZZI RIGA    asse x = ",n_rigam
    TYPE "  NUMERO COLONNE       asse y = ",n_colonnam
    TYPE ""
    TYPE "--->>> CALCOLO MAGAZZINO COMPLETATO !!!."
    HALT
  ELSE
    TYPE "--->>> CALCOLO MAGAZZINO ABORTITO !!!."
  END
  RETURN
.END
.PROGRAM calibra_pallet()
;Questa procedura permette di calibrare il piano del pallet 1 o 2.
;- a0 = punto origine pallet.
;- ax = punto asse x pallet.
;- ay = punto asse y pallet.
  TYPE ""
  TYPE " -----------------ax1/2"
  TYPE " |                    |"
  TYPE " |                    |"
  TYPE " |                    |"
  TYPE " |                    |"
  TYPE " ay1/2------------a01/2"
  TYPE ""
  TYPE "PALLET 1 - CARICO "
  TYPE "PALLET 2 - SCARICO"
  TYPE ""
;
  side = -1
  $stampa = "QUALE PALLET VUOI ACQUISIRE ? <1-2> "
  DO
    PROMPT $stampa,side 
  UNTIL (side==1 OR side==2)
;
  fare = -1
  $stampa = "SEI SICURO ? : SI <1> - NO <0> "
  DO
    PROMPT $stampa,fare 
  UNTIL (fare==0 OR fare==1)
;
  IF fare==1 THEN
;
    CASE side OF
     VALUE 1:
      $stampa = "QUANTI PEZZI PER RIGA ? (asse X) "
      $stampa = $stampa+$ENCODE(/F8.1,n_riga1)+" : "
      DO
        PROMPT $stampa,n_riga1 
      UNTIL (n_riga1>=1)
;
      $stampa = "QUANTI PEZZI PER COLONNA ? (asse Y) "
      $stampa = $stampa+$ENCODE(/F8.1,n_colonna1)+" : "
      DO
        PROMPT $stampa,n_colonna1 
      UNTIL (n_colonna1>=1)
;
      $stampa = "QUANTI PIANI ? "
      $stampa = $stampa+$ENCODE(/F8.1,n_piano1)+" : "
      DO
        PROMPT $stampa,n_piano1 
      UNTIL (n_piano1>=1)
;
      $stampa = "ALTEZZA PEZZO ? "
      $stampa = $stampa+$ENCODE(/F8.1,int_piano1)+" : "
      DO
        PROMPT $stampa,int_piano1 
      UNTIL (int_piano1>=0)
;
      $stampa = "ALTEZZA INTERFALDA ? "
      $stampa = $stampa+$ENCODE(/F8.1,int_falda1)+" : "
      DO
        PROMPT $stampa,int_falda1 
      UNTIL (int_falda1>=0)
;
      $stampa = "ALTEZZA EUROPALLET ? "
      $stampa = $stampa+$ENCODE(/F8.1,int_euro1)+" : "
      DO
        PROMPT $stampa,int_euro1 
      UNTIL (int_euro1>=0)
;
      $stampa = "INTERASSE RIGA ? (asse X) "
      $stampa = $stampa+$ENCODE(/F8.1,int_riga1)+" : "
      DO
        PROMPT $stampa,int_riga1 
      UNTIL (int_riga1>=0)
;
      $stampa = "INTERASSE COLONNA ? (asse Y) "
      $stampa = $stampa+$ENCODE(/F8.1,int_col1)+" : "
      DO
        PROMPT $stampa,int_col1 
      UNTIL (int_col1>=0)
;
      TOOL punta
     VALUE 2:
      $stampa = "QUANTI PEZZI PER RIGA ? (asse X) "
      $stampa = $stampa+$ENCODE(/F8.1,n_riga2)+" : "
      DO
        PROMPT $stampa,n_riga2 
      UNTIL (n_riga2>=1)
;
      $stampa = "QUANTI PEZZI PER COLONNA ? (asse Y) "
      $stampa = $stampa+$ENCODE(/F8.1,n_colonna2)+" : "
      DO
        PROMPT $stampa,n_colonna2 
      UNTIL (n_colonna2>=1)
;
      $stampa = "ALTEZZA PEZZO ? "
      $stampa = $stampa+$ENCODE(/F8.1,int_piano2)+" : "
      DO
        PROMPT $stampa,int_piano2 
      UNTIL (int_piano2>=0)
;
      $stampa = "ALTEZZA INTERFALDA ? "
      $stampa = $stampa+$ENCODE(/F8.1,int_falda2)+" : "
      DO
        PROMPT $stampa,int_falda2 
      UNTIL (int_falda2>=0)
;
      $stampa = "ALTEZZA EUROPALLET ? "
      $stampa = $stampa+$ENCODE(/F8.1,int_euro2)+" : "
      DO
        PROMPT $stampa,int_euro2 
      UNTIL (int_euro2>=0)
;
      $stampa = "INTERASSE RIGA ? (asse X) "
      $stampa = $stampa+$ENCODE(/F8.1,int_riga2)+" : "
      DO
        PROMPT $stampa,int_riga2 
      UNTIL (int_riga2>=0)
;
      $stampa = "INTERASSE COLONNA ? (asse Y) "
      $stampa = $stampa+$ENCODE(/F8.1,int_col2)+" : "
      DO
        PROMPT $stampa,int_col2 
      UNTIL (int_col2>=0)
;
      TOOL pinza1
    END
;
    fare = -1
    $stampa = "VUOI CALCOLARE LA FRAME DI ORIGINE ? : SI <1> - NO <0> "
    DO
      TYPE " "
      TYPE " "
      PROMPT $stampa,fare 
    UNTIL (fare==0 OR fare==1)
;
    IF fare==1 THEN
      fare = -1
      $stampa = "SEI SICURO ? : SI <1> - NO <0> "
      DO
        PROMPT $stampa,fare 
      UNTIL (fare==0 OR fare==1)
      IF fare==1 THEN
        CASE side OF
         VALUE 1:
          POINT pallet1 = FRAME(a01,ax1,ay1,a01)
          POINT pallet1 = pallet1+TRANS(0,0,-1*int_euro1,0,0,0)
          POINT grip_pal1 = -pallet1+TRANS(0,0,-1*int_euro1,0,0,0)+a01
         VALUE 2:
          POINT pallet2 = FRAME(a02,ax2,ay2,a02)
          POINT pallet2 = pallet2+TRANS(0,0,-1*int_euro2,0,0,0)
          POINT grip_pal2 = -pallet2+TRANS(0,0,-1*int_euro2,0,0,0)+a02
        END
      END
    END
;
    TYPE ""
    TYPE "  PALLET : ",side
    TYPE "  PARAMETRI CALCOLATI : "
    CASE side OF
     VALUE 1:
      TYPE "  INTERASSE RIGA        asse x = ",int_riga1
      TYPE "  INTERASSE COLONNA     asse y = ",int_col1
      TYPE "  INTERASSE PIANO       asse z = ",int_piano1
      TYPE "  INTERFALDA                   = ",int_falda1
      TYPE ""
      TYPE "  NUMERO PEZZI RIGA     asse x = ",n_riga1
      TYPE "  NUMERO PEZZI COLONNA  asse y = ",n_colonna1
      TYPE "  NUMERO PIANI          asse z = ",n_piano1
     VALUE 2:
      TYPE "  INTERASSE RIGA        asse x = ",int_riga2
      TYPE "  INTERASSE COLONNA     asse y = ",int_col2
      TYPE "  INTERASSE PIANO       asse z = ",int_piano2
      TYPE "  INTERFALDA                   = ",int_falda2
      TYPE ""
      TYPE "  NUMERO PEZZI RIGA     asse x = ",n_riga2
      TYPE "  NUMERO PEZZI COLONNA  asse y = ",n_colonna2
    END
    TYPE ""
    TYPE "--->>> CALCOLO PIANO DEL PALLET COMPLETATO !!!."
    HALT
  ELSE
    TYPE "--->>> CALCOLO PIANO DEL PALLET ABORTITO !!!."
  END
  RETURN
.END
.PROGRAM cambiopallet()
;Controlla se cambiare i pallet.
  IF fine_pallet1==1 THEN
    JMOVE #attesa
    BREAK
    CALL pausa
    accendi = 1
    suona = 1
    TYPE "Pallet ultimato !!!"
    HALT
  END
  RETURN
.END
.PROGRAM carica_staffa()
;Mette la staffa nel tornio2
  CALL prel_staffa
  OPEN 2
  ACCURACY 50
  JMOVE #fs_tornio2
  CALL attesa_c_tor2;attesa pronto carico
  ACCURACY 5
  LMOVE #s_staffa
  ACCURACY 1
  SPEED 10
  DECEL 10
  LMOVE #staffa
  BREAK
  STABLE 0.1
  CALL chiudi_mdr_tor2;chiude mandrino tornio
  CALL chiudipinza2
  ACCURACY 5
  LMOVE #s_staffa
  RETURN
.END
.PROGRAM carico_boccole()
;Programma di carico boccola nella pressa
  ACCURACY 50
  JMOVE #f_pressa
  CALL pausa
  CALL attesa_pressa;attesa pronto carico
  CLOSE 2
  ACCURACY 5
  LMOVE #s_boccola
  DECEL 5
  SPEED 10
  ACCURACY 1
  LMOVE #boccola
  BREAK
  TOOL pinza1
  CALL apripinza2
  ACCURACY 5
  $boccola = "caricata"
  IF tdebug>0 THEN
    TYPE ""
    TYPE "BOCCOLA: ",$boccola
  END
  LMOVE #s_boccola
  ACCURACY 50
  LMOVE #f_pressa
  RETURN
.END
.PROGRAM carico_pressa()
;Programma di carico pezzo nella pressa
  CLOSE 1
  ACCURACY 10
  JMOVE #f_pressa
  CALL attesa_pressa;attesa pronto carico
  CALL check_sq30m
  ACCURACY 3
  LMOVE #s_pressa
  DECEL 5
  SPEED 10
  ACCURACY 1
  LMOVE #pressac
  BREAK
  CALL apripinza1
  HERE partenza
  $pressa = "piena"
  IF tdebug>0 THEN
    TYPE ""
    TYPE "PRESSA: ",$pressa
  END
  ACCURACY 3
  LMOVE #s_pressa
  ACCURACY 50
  LMOVE #f_pressa
  dist_pre = fuori_pressa
  CALL wait_out;attesa fuori pressa
  CALL check_sq30p
  CALL via_pressa
  JMOVE #passaggio1
  JMOVE #passaggio
  RETURN
.END
.PROGRAM carico_tor1()
;Programma di carico pezzo grezzo tornio 1
  CLOSE 1
  ACCURACY 50
  JMOVE #f_tornio1
  CALL attesa_c_tor1;attesa pronto carico
  ACCURACY 20
  LAPPRO #tornio1c,100
  ACCURACY 5
  LAPPRO #tornio1c,10
  DECEL 5
  SPEED 10
  ACCURACY 1
  LMOVE #tornio1c
  BREAK
  CALL apripinza1
  HERE partenza
;       ACCURACY 1
;       LMOVE #tornio1_spingi
;       BREAK
  CALL chiudi_mdr_tor1;chiusura mandrino tornio
  $tornio1 = "pieno"
  IF tdebug>0 THEN
    TYPE ""
    TYPE "MANDRINO TORNIO 1: ",$tornio1
  END
  ACCURACY 20
  LDEPART 100
  ACCURACY 50
  LMOVE #f_tornio1
  dist_pre = fuori_tornio
  CALL wait_out;attesa fuori tornio
  CALL via_ciclo_tor1;start ciclo lavoro
  RETURN
.END
.PROGRAM carico_tor2()
;Programma di carico pezzo grezzo tornio 2
  JMOVE #passaggio
  CLOSE 1
  ACCURACY 50
  JMOVE #f_tornio2
  CALL attesa_c_tor2;attesa pronto carico
  CALL check_sq22m
  ACCURACY 20
  LAPPRO #tornio2c,100
  ACCURACY 5
  LAPPRO #tornio2c,10
  DECEL 5
  SPEED 10
  ACCURACY 1
  LMOVE #tornio2c
  BREAK
  CALL apripinza1
  HERE partenza
  HERE partenza_mem
  CALL chiudi_mdr_tor2;chiusura mandrino tornio
  $tornio2 = "pieno"
  IF tdebug>0 THEN
    TYPE ""
    TYPE "MANDRINO TORNIO 2: ",$tornio2
  END
  ACCURACY 20
  LDEPART 100
  ACCURACY 50
  LMOVE #f_tornio2
  POINT partenza = partenza_mem
  dist_pre = fuori_tornio
  CALL wait_out;attesa fuori tornio
  CALL via_ciclo_tor2;start ciclo lavoro
  RETURN
.END
.PROGRAM cerca()
  CALL apricamera
  flag = -1
  CALL txrx
  RETURN
.END
.PROGRAM cerca_boccol()
;esegue la ricerca della boccole da magazzino.
  POINT presa_pallet = magazzino+TRANS(int_rigam*(rigam-1),int_colm*colonnam,0,0,0,0)
  POINT s_presa_pallet = magazzino+TRANS(400,int_colm*colonnam,0,0,0,0)
  POINT controllo = presa_pallet+tasta_boccola
  POINT s_controllo = controllo+TRANS(0,-50,0,0,0,0)
  ACCURACY 5
  LMOVE s_controllo
ritest:
  IF SIG(sq24) THEN
    allarme_m = 300 ;allarme tastatore
    CALL master_all
    GOTO ritest
  END
  ACCURACY 0.5
  SPEED 10
  DECEL 5
  LMOVE controllo
  BREAK
  STABLE 0.1
  IF SIG(sq24) THEN
    t_boccola = 1
  ELSE
    t_boccola = 0
  END
  ACCURACY 5
  LMOVE s_controllo
  RETURN
.END
.PROGRAM cerca_fase()
;Ricerca razza del pezzo con fotocellula.
  CLOSE 1
  JMOVE #ff_fasa
10
  ACCURACY 50
  LMOVE #f_fasa
  CALL pausa
  ACCURACY 1
  LMOVE fasa
  BREAK
  STABLE 0.1
  HERE temp
  ABS.SPEED ON
  SPEED 5,10 DEG/S
  ACCURACY 1
  RIGHTY
  IF SIG(sq21) THEN
    $fronte = "negativo"
    XMOVE fasa+TRANS(0,0,0,0,0,ruota) TILL -sq21
  ELSE
    $fronte = "positivo"
    XMOVE fasa+TRANS(0,0,0,0,0,ruota) TILL sq21
  END
  BREAK
  STABLE 0.1
  ABS.SPEED OFF
  IF SIG(-sq21) AND $fronte=="positivo" THEN
    allarme_m = 36
    CALL master_all
    GOTO 10
  END
  IF SIG(sq21) AND $fronte=="negativo" THEN
    allarme_m = 37
    CALL master_all
    GOTO 10
  END
  HERE temp+angolo
  corr_ang = DEXT(angolo,4)+DEXT(angolo,6)
  IF corr_ang>=180 THEN
    corr_ang = corr_ang-360
  END
  IF corr_ang<=-180 THEN
    corr_ang = corr_ang+360
  END
  IF tdebug>0 THEN
    TYPE "Angolo rilevato : ",corr_ang
    TYPE "Fronte : ",$fronte
  END
  ACCURACY 50
  LMOVE #f_fasa
  RETURN
.END
.PROGRAM cerca_fase1()
;Ricerca razza del pezzo con fotocellula: ripresa1
  CLOSE 1
10
  ACCURACY 50
  LMOVE #f_fasa1
  CALL pausa
  ACCURACY 1
  LMOVE fasa1
  BREAK
  STABLE 0.1
  HERE temp1
  ABS.SPEED ON
  SPEED 3,3 DEG/S
  ACCURACY 1
  RIGHTY
  IF SIG(sq52) THEN
    TYPE "Fronte negativo"
    $fronte1 = "negativo"
    XMOVE fasa1+TRANS(0,0,0,0,0,ruota) TILL -sq52
  ELSE
;
    TYPE "Fronte positivo"
    $fronte1 = "positivo"
    XMOVE fasa1+TRANS(0,0,0,0,0,ruota) TILL sq52
  END
  BREAK
  STABLE 0.1
  ABS.SPEED OFF
  IF SIG(-sq52) AND $fronte1=="positivo" THEN
    allarme_m = 36
    CALL master_all
    GOTO 10
  END
  IF SIG(sq52) AND $fronte1=="negativo" THEN
    allarme_m = 37
    CALL master_all
    GOTO 10
  END
  HERE temp1+angolo1
  corr_ang1 = DEXT(angolo1,4)+DEXT(angolo1,6)
  IF corr_ang1>=180 THEN
    corr_ang1 = corr_ang1-360
  END
  IF corr_ang1<=-180 THEN
    corr_ang1 = corr_ang1+360
  END
  IF tdebug>0 THEN
    TYPE "Angolo rilevato : ",corr_ang1
    TYPE "Fronte : ",$fronte1
  END
  ACCURACY 50
  LMOVE #f_fasa1
  RETURN
.END
.PROGRAM cgrip()
  CALL set.up
  TYPE /C16
  TYPE "---------------------------------------------------------------"
  TYPE ".1..DEFINIRE UNA POSIZIONE FUORI DAL CAMPO DI VISIONE (#fuori_pallet1)"
  TYPE ".2..VERIFICARE CHE IL TOOL PINZA UTILIZZATO SIA CORRETTO."
  TYPE ".3..PRELEVARE IL PEZZO DAL MORSETTO E PORTARLO IN"
  TYPE "    POSIZIONE DI VISIONE."
  TYPE ".4..PREMERE CICLO."
  TYPE "---------------------------------------------------------------"
  PAUSE
  $messaggio = "QUALE MODELLO DI "+$tipo+" VUOI ACQUISIRE ? "
  DO
    PROMPT $messaggio,rflag 
  UNTIL (rflag==0 OR rflag==1 OR rflag==2 OR rflag==3 OR rflag==4 OR rflag==5)
  IF rflag==1 OR rflag==2 THEN
    rflag = 1
  END
  HERE ra[rflag]   ;memorizza la posizione effettiva
  IF rflag==1 THEN
    rflag = 2
  END
  OPENI 1
  CLOSEI 3
  CLOSEI 4
  TWAIT 1
  ACCURACY 5
  SPEED 20
  LMOVE SHIFT(HERE BY 0,0,300)
  IF rflag==0 THEN
    JMOVE #fuori_pallet1
    CALL punto_pezzo
  ELSE
    LMOVE #inter1
    LMOVE #inter
    JMOVE #pass_inter_rit
    CALL lascia_organo
    JMOVE #fuori_pallet1
    offset_falda_x = -80
    offset_falda_y = 1000
    CALL punto_falda
  END
  DO
    CALL vedi
    IF rflag<>flag THEN
      TYPE /C1,">>> MODELLO RICONOSCIUTO DIVERSO DAL RICHIESTO <<<"
      TYPE /C1," FLAG = ",flag," RFLAG = ",rflag
      TYPE /C1,".... PREMI CICLO SE VUOI RITENTARE IL RICONOSCIMENTO"
      PAUSE
    END
  UNTIL rflag==flag
  IF rflag==0 THEN
    angolo_cal[rflag] = (0-b)
    TYPE "Angolo calibrazione : ",angolo_cal[rflag]
  ELSE
    POINT presa2 = presa
    offset_falda_x = -40
    offset_falda_y = -80
    CALL punto_falda
    DO
      rflag = 1
      CALL vedi
      IF rflag<>flag THEN
        TYPE /C1,">>> MODELLO RICONOSCIUTO DIVERSO DAL RICHIESTO <<<"
        TYPE /C1," FLAG = ",flag," RFLAG = ",rflag
        TYPE /C1,".... PREMI CICLO SE VUOI RITENTARE IL RICONOSCIMENTO"
        PAUSE
      END
    UNTIL rflag==flag
    POINT presa1 = presa
    DECOMPOSE rel1[0] = presa1
    DECOMPOSE rel2[0] = presa2
    a = -1*ATAN2((rel1[1]-rel2[1]),(rel1[0]-rel2[0]))
    POINT presa = pallet1+TRANS(offset_falda_x,offset_falda_y,(piano1*(int_piano1+int_falda1))+int_euro1,0,0,0)+offset_camera+fcam+TRANS(x,y,0,0,0,a)
  END
  DECOMPOSE loc[0] = ra[flag]
  TYPE /C1," --------------------------------------------------------------"
  TYPE "POSIZIONATO A =",/F7.2,loc[0],loc[1],loc[2],loc[3],loc[4],loc[5],/C1
  tutte = 0
  PROMPT "VUOI METTERE TUTTE LE GRIP UGUALI ? NO <0> SI <1> ",tutte 
;calcola la grip position relativa all'angolo
  CALL calcola_g
  RETURN
.END
.PROGRAM check_all()
;Controlla che il prg master_all non sia stato abortito.
  IF allarme_m>0 AND fasem==100 THEN
    CALL master_all
  END
  RETURN
.END
.PROGRAM check_ch_tor1()
;Controlla porta chiusa TORNIO1.
  CASE fase10 OF
   VALUE 1:
    IF chiuso_tor1==0 GOTO 10
    fase10 = 2
    t_staz10 = TIMER(0)
10
   VALUE 2:
    IF SIG(-in6_tornio1) GOTO 24
    IF ((TIMER(0)-t_staz10)<20) GOTO 22
    r_fase10 = 1         ;salva fase di rientro
    fase10 = 99
    allarme_10 = 103      ;allarme porta tornio 1 non chiusa 
    GOTO 22
24
    fase10 = 1
    chiuso_tor1 = 0
22
   VALUE 99:
    IF vis.occupato==1 GOTO 990     ;visualizzatore occupato?
    vis.occupato = 1                ;occupa visualizzatore
    allarme = allarme_10            ;carica codice allarme
    fase10 = 100
990
   VALUE 100:
    IF allarme>0 GOTO 1000          ;attesa tacitazione allarme
    vis.occupato = 0                ;libera visualizzatore
    fase10 = r_fase10               ;carica fase di rientro
    allarme_10 = 0                  ;pulisce coda di allarme
1000
  END
  RETURN
.END
.PROGRAM check_ch_tor2()
;Controlla porta chiusa TORNIO2.
  CASE fase11 OF
   VALUE 1:
    IF chiuso_tor2==0 GOTO 10
    fase11 = 2
    t_staz11 = TIMER(0)
10
   VALUE 2:
    IF SIG(-in6_tornio2) GOTO 24
    IF ((TIMER(0)-t_staz11)<20) GOTO 22
    r_fase11 = 1         ;salva fase di rientro
    fase11 = 99
    allarme_11 = 123      ;allarme porta tornio 2 non chiusa 
    GOTO 22
24
    fase11 = 1
    chiuso_tor2 = 0
22
   VALUE 99:
    IF vis.occupato==1 GOTO 990     ;visualizzatore occupato?
    vis.occupato = 1                ;occupa visualizzatore
    allarme = allarme_11            ;carica codice allarme
    fase11 = 100
990
   VALUE 100:
    IF allarme>0 GOTO 1000          ;attesa tacitazione allarme
    vis.occupato = 0                ;libera visualizzatore
    fase11 = r_fase11               ;carica fase di rientro
    allarme_11 = 0                  ;pulisce coda di allarme
1000
  END
  RETURN
.END
.PROGRAM check_ciclo()
;*************************************************************
;* Gestione simulata tasto CYCLE START per anticollisione
;* Data     : 21 02 2003
;*************************************************************
;
;Settaggio uscita watch dog
;
  PULSE watch_dog,0.5
;
;Pulisce programma in corso al primo avvio
;
  IF mk_primo==1 THEN
    mk_primo = 0
    MC kill
  END
;
;Verifica stato selettori Robot
;
  IF NOT SWITCH(CS ) AND NOT SWITCH(ERROR ) AND allarme==0 THEN
    IF NOT SWITCH(TEACH_LOCK ) AND SWITCH(RUN ) THEN
      IF SWITCH(POWER ) AND SWITCH(REPEAT ) THEN
        mk_sel_ok = 1
      ELSE
        mk_sel_ok = 0
      END
    ELSE
      mk_sel_ok = 0
    END
  ELSE
    mk_sel_ok = 0
  END
;
;Controllo del pulsante simulato di EX CICLO
;
  IF SIG(-puls_cycle,-puls_reset) AND NOT SWITCH(CS ) THEN
    mk_off = 1
  END
;
  IF SIG(puls_cycle,puls_reset) AND mk_off==1 THEN
    mk_off = 0
    IF mk_sel_ok==1 THEN
      MC ex ciclo,,1
    ELSE
      IF NOT SWITCH(POWER ) THEN
        TYPE "ERROR (-201) Cannot execute program, MOTOR POWER is OFF."
      ELSE
        IF NOT SWITCH(REPEAT ) THEN
          TYPE "ERROR (-201) Cannot execute a program, in TEACH mode."
        ELSE
          IF SWITCH(TEACH_LOCK ) THEN
            TYPE "ERROR (-202) Cannot execute program, TEACH LOCK is ON."
          END
        END
      END
    END
  END
;
;Memoria posizione arresto robot
;
  IF SWITCH(CS ) AND TASK(1)==1 THEN
    mk_ciclo_on = 1
  END
  IF NOT SWITCH(CS ) AND NOT SWITCH(RGSO ) AND mk_ciclo_on==1 THEN
    mk_ciclo_on = 0
    HERE posizione
  END
;
;Verifica se simulare tasto CYCLE START
;
;
  IF SIG(puls_cycle,-puls_reset) AND mk_off==1 THEN
    mk_off = 0
    IF mk_sel_ok==1 THEN
      IF TASK(1)<>0 THEN
        scosta = DISTANCE(HERE,posizione)
        IF scosta<=2 THEN
          MC cont
        ELSE
          MC hold
          TYPE "ERROR (-3999) Robot is out POSITION."
        END
      ELSE
        TYPE "ERROR (-4000) Program is not READY."
      END
    ELSE
      IF NOT SWITCH(POWER ) THEN
        TYPE "ERROR (-201) Cannot execute program, MOTOR POWER is OFF."
      ELSE
        IF NOT SWITCH(REPEAT ) THEN
          TYPE "ERROR (-201) Cannot execute a program, in TEACH mode."
        ELSE
          IF SWITCH(TEACH_LOCK ) THEN
            TYPE "ERROR (-202) Cannot execute program, TEACH LOCK is ON."
          END
        END
      END
    END
  END
;
  RETURN
.END
.PROGRAM check_magazzino()
;Controllo presenza magazzino boccole.
10
  IF SIG(sq1) GOTO 20
  allarme_m = 16  ;allarme magazzino boccole non pos.
  CALL master_all
  GOTO 10
20
  RETURN
.END
.PROGRAM check_pal_car()
;Controllo presenza pallet carico.
10
  IF SIG(sq3) GOTO 20
  allarme_m = 14  ;allarme pallet carico non pos.
  CALL master_all
  GOTO 10
20
  RETURN
.END
.PROGRAM check_pal_scar()
;Controllo presenza pallet scarico.
10
  IF SIG(sq3) GOTO 20
  allarme_m = 15  ;allarme pallet scarico non pos.
  CALL master_all
  GOTO 10
20
  RETURN
.END
.PROGRAM check_sq15m()
;Controllo efficenza presenza staffa 1
10
  IF SIG(-sq15) GOTO 20
  allarme_m = 56 ;allarme efficenza presenza staffa 1
  CALL master_all
  GOTO 10
20
  RETURN
.END
.PROGRAM check_sq15p()
;Controllo presenza staffa 1
10
  IF SIG(sq15) GOTO 20
  allarme_m = 55 ;allarme mancanza staffa 1
  CALL master_all
  GOTO 10
20
  RETURN
.END
.PROGRAM check_sq16m()
;Controllo efficenza presenza staffa 2
10
  IF SIG(-sq16) GOTO 20
  allarme_m = 58 ;allarme efficenza presenza staffa 1
  CALL master_all
  GOTO 10
20
  RETURN
.END
.PROGRAM check_sq16p()
;Controllo presenza staffa 2
10
  IF SIG(sq16) GOTO 20
  allarme_m = 57 ;allarme mancanza staffa 2
  CALL master_all
  GOTO 10
20
  RETURN
.END
.PROGRAM check_sq17m()
;Controllo efficenza presenza staffa 3
10
  IF SIG(-sq17) GOTO 20
  allarme_m = 60 ;allarme efficenza presenza staffa 3
  CALL master_all
  GOTO 10
20
  RETURN
.END
.PROGRAM check_sq17p()
;Controllo presenza staffa 3
10
  IF SIG(sq17) GOTO 20
  allarme_m = 59 ;allarme mancanza staffa 3
  CALL master_all
  GOTO 10
20
  RETURN
.END
.PROGRAM check_sq18m()
;Controllo efficenza presenza staffa 4
10
  IF SIG(-sq18) GOTO 20
  allarme_m = 62 ;allarme efficenza presenza staffa 4
  CALL master_all
  GOTO 10
20
  RETURN
.END
.PROGRAM check_sq18p()
;Controllo presenza staffa 4
10
  IF SIG(sq18) GOTO 20
  allarme_m = 61 ;allarme mancanza staffa 4
  CALL master_all
  GOTO 10
20
  RETURN
.END
.PROGRAM check_sq19m()
;Controllo pistone ripresa1 basso
10
  IF SIG(-sq19) GOTO 20
  allarme_m = 51 ;allarme efficenza fermo pezzo ripresa1 alto
  CALL master_all
  GOTO 10
20
  RETURN
.END
.PROGRAM check_sq19p()
;Controllo pistone ripresa1 salito
10
  IF SIG(sq19) GOTO 20
  allarme_m = 50 ;allarme fermo pezzo ripresa1 non alto
  CALL master_all
  GOTO 10
20
  RETURN
.END
.PROGRAM check_sq20m()
;Controllo pistone ripresa2 basso
10
  IF SIG(-sq20) GOTO 20
  allarme_m = 53 ;allarme efficenza fermo pezzo ripresa2 alto
  CALL master_all
  GOTO 10
20
  RETURN
.END
.PROGRAM check_sq20p()
;Controllo pistone ripresa2 salito
10
  IF SIG(sq20) GOTO 20
  allarme_m = 52 ;allarme fermo pezzo ripresa2 non alto
  CALL master_all
  GOTO 10
20
  RETURN
.END
.PROGRAM check_sq22m()
;Controllo posaggio ripresa verticale libero
  TIMER (1) = 0
10
  IF SIG(-sq22) GOTO 20
  IF TIMER(1)<5 GOTO 10
  allarme_m = 78 ;allarme posaggio ripresa verticale non libero
  CALL master_all
  GOTO 10
20
  RETURN
.END
.PROGRAM check_sq22p()
;Controllo posaggio ripresa verticale occupato
10
  IF SIG(sq22) GOTO 20
  allarme_m = 79 ;allarme posaggio ripresa verticale non occupato
  CALL master_all
  GOTO 10
20
  RETURN
.END
.PROGRAM check_sq23m()
;Controllo efficenza FC presenza boccola in pinza2
10
  IF SIG(-sq23) GOTO 20
  allarme_m = 81 ;allarme efficenza presenza boccola in pinza2
  CALL master_all
  GOTO 10
20
  RETURN
.END
.PROGRAM check_sq23p()
;Controllo presenza boccola in pinza2
10
  IF SIG(sq23) GOTO 20
  allarme_m = 80 ;allarme mancanza boccola in pinza2
  CALL master_all
  GOTO 10
20
  RETURN
.END
.PROGRAM check_sq30m()
;Controllo posaggio pressa libero
10
  IF SIG(-sq30) GOTO 20
  allarme_m = 76 ;allarme posaggio pressa non libero
  CALL master_all
  GOTO 10
20
  RETURN
.END
.PROGRAM check_sq30p()
;Controllo posaggio pressa occupato
10
  IF SIG(sq30) GOTO 20
  allarme_m = 77 ;allarme posaggio pressa non occupato
  CALL master_all
  GOTO 10
20
  RETURN
.END
.PROGRAM check_sq50m()
;Controllo posaggio ripresa 2
10
  IF SIG(-sq50) GOTO 20
  allarme_m = 64 ;allarme fermo pezzo ripresa2 basso
  CALL master_all
  GOTO 10
20
  RETURN
.END
.PROGRAM check_sq51m()
;Controllo posaggio ripresa 1
10
  IF SIG(-sq51) GOTO 20
  allarme_m = 63 ;allarme fermo pezzo ripresa1 basso
  CALL master_all
  GOTO 10
20
  RETURN
.END
.PROGRAM check_sq6p()
;Controllo fotoc. presenza interfalde a magazzino
10
  IF SIG(sq6) GOTO 20
  allarme_m = 23 ;allarme mancanza interfalda a magazzino
  CALL master_all
  GOTO 10
20
  RETURN
.END
.PROGRAM check_sq8m()
;Controllo efficenza interfalda su organo
10
  IF SIG(-sq8) GOTO 20
  allarme_m = 13 ;allarme eff. interfalda
  CALL master_all
  GOTO 10
20
  RETURN
.END
.PROGRAM check_sq8p()
;Controllo presenza interfalda su organo
5
  TIMER (1) = 0
10
  IF SIG(sq8) GOTO 20
  IF TIMER(1)<10 GOTO 10
  allarme_m = 12 ;allarme manca interfalda
  CALL master_all
  GOTO 5
20
  RETURN
.END
.PROGRAM check_sq9m()
;Controllo efficenza presenza fotocellula robot.
10
  IF SIG(-sq9) GOTO 20
  allarme_m = 21 ;allarme efficenza presenza foto.
  CALL master_all
  GOTO 10
20
  RETURN
.END
.PROGRAM chiudi_mdr_tor1()
;Richiesta chiusura mandrino TORNIO1.
  SIGNAL -out1_tornio1,out2_tornio1 ;rich. chiusura morse
5
  TIMER (1) = 0
10
  IF SIG(-in1_tornio1,in2_tornio1) GOTO 20
  IF TIMER(1)<5 GOTO 10
  allarme_m = 107 ;mandrino tornio1 non chiuso
  CALL master_all
  GOTO 5
20
  SIGNAL -out1_tornio1,-out2_tornio1
  TWAIT t_chiudi_mdr1
  RETURN
.END
.PROGRAM chiudi_mdr_tor2()
;Richiesta chiusura mandrino TORNIO2.
  SIGNAL -out1_tornio2,out2_tornio2 ;rich. chius. morse
5
  TIMER (1) = 0
10
  IF SIG(-in1_tornio2,in2_tornio2) GOTO 20
  IF TIMER(1)<5 GOTO 10
  allarme_m = 127 ;mandrino tornio2 non chiuso
  CALL master_all
  GOTO 5
20
  SIGNAL -out1_tornio2,-out2_tornio2
  TWAIT t_chiudi_mdr2
  RETURN
.END
.PROGRAM chiudicamera()
;Chiusura sportello camera.
5
  SIGNAL -yv4_1
  TIMER (1) = 0
10
  TWAIT 0.5 ;TOGLIERE DOPO SOSTITUZIONE FC
  GOTO 20 ;TOGLIERE DOPO SOSTITUZIONE FC
  IF SIG(-sq10,sq11) GOTO 20
  IF TIMER(1)<5 GOTO 10
  allarme_m = 11  ;allarme sportello non chiuso
  CALL master_all
  GOTO 5
20
  RETURN
.END
.PROGRAM chiudipinza1()
;Chiude la pinza 1
  CLOSEI 1
  TWAIT t_chiude1
  RETURN
.END
.PROGRAM chiudipinza2()
;Chiude la pinza 2
  CLOSEI 2
  TWAIT t_chiude2
  RETURN
.END
.PROGRAM chiudiripresa1()
;Chiude il fermo pezzo della ripresa1
5
  TIMER (1) = 0
10
  SIGNAL -yv7a_1,yv7r_1
  IF SIG(-sq19) GOTO 20
  IF TIMER(1)<5 GOTO 10
  allarme_m = 51 ;allarme efficenza fermo pezzo ripresa1 alto
  CALL master_all
  GOTO 5
20
  TWAIT t_c_ripresa1
  IF SIG(-sq51) GOTO 30
  allarme_m = 63 ;allarme fermo pezzo ripresa1 basso
  CALL master_all
  GOTO 20
30
  RETURN
.END
.PROGRAM chiudiripresa2()
;Chiude il fermo pezzo della ripresa2
5
  TIMER (1) = 0
10
  SIGNAL -yv8a_1,yv8r_1
  IF SIG(-sq20) GOTO 20
  IF TIMER(1)<5 GOTO 10
  allarme_m = 53 ;allarme efficenza fermo pezzo ripresa2 alto
  CALL master_all
  GOTO 5
20
  TWAIT t_c_ripresa2
  IF SIG(-sq50) GOTO 30
  allarme_m = 64 ;allarme fermo pezzo ripresa2 basso
  CALL master_all
  GOTO 20
30
  RETURN
.END
.PROGRAM chk_pos_fibra()
;Controllo posizione staffa su pinza 2.
  ACCURACY 5
  LMOVE #fuori_fibra
10
  ACCURACY 1
  SPEED 50
  LMOVE fibra[pnt_staffa]+TRANS(0,50,0,0,0,0)
  BREAK
  STABLE 0.1
ritest:
  IF SIG(sq_fibra) THEN
    allarme_m = 310 ;allarme eff.fibra
    CALL master_all
    GOTO ritest
  END
  HERE partenza
  ABS.SPEED ON
  SPEED 10 MM/S
  ACCURACY 0.1
  XMOVE fibra[pnt_staffa] TILL sq_fibra
  BREAK
  STABLE 0.1
  ABS.SPEED OFF
  IF SIG(-sq_fibra) THEN
    ACCURACY 1
    SPEED 50
    LMOVE fibra[pnt_staffa]+TRANS(0,50,0,0,0,0)
    allarme_m = 311 ;allarme staffa
    CALL master_all
    GOTO 10
  END
  q_staffa = DISTANCE(HERE,partenza)
  IF cal_q_staffa==1 THEN
    cal_q_staffa = 0
    zero_q_staffa[pnt_staffa] = q_staffa
  END
  quota_staffa = ABS(q_staffa-zero_q_staffa[pnt_staffa])
  IF tdebug>0 THEN
    TYPE "Quota staffa su pinza 2 : ",quota_staffa
  END
  ACCURACY 1
  SPEED 50
  LMOVE fibra[pnt_staffa]+TRANS(0,50,0,0,0,0)
  IF quota_staffa>soglia_staffa THEN
    allarme_m = 311 ;allarme staffa
    CALL master_all
    GOTO 10
  END
  ACCURACY 5
  LMOVE #fuori_fibra
  RETURN
.END
.PROGRAM ciclo()
;Programma principale.
  CALL io.ini
  aggiorna = 0
  CALL exe
  CALL reset.out
  CALL set.up
  CALL monitor
  CALL check_all
  CALL start
  CALL startup
  CALL chiudicamera
  TYPE /C15,"******************************************************************"
  TYPE "                 T  H  E  S  Y  S   s.r.l."
  TYPE "                   Via Industriale 13/15"
  TYPE "                   Montichiari - BRESCIA"
  TYPE "      TEL. 030/9961811  (4 Linee r.a.)   FAX 030/9962763"
  TYPE "                  E-mail techdept@thesys.it"
  TYPE " "
  TYPE "                   E\' IN ESECUZIONE IL PROGRAMMA"
  TYPE "                  NOME FILE CARICATO :  ",$tipo
  TYPE "                  SE OK PREMERE CYCLE START."
  TYPE " "
  TYPE "                  PEZZI DA PRODURRE : ",contapezzi
  TYPE "******************************************************************"
  CALL azzera
  CALL svuota_pinze
  conta = 0
  contaquali = 0
  accendi = 0
  tinizio = TIMER(0)
  CLOSE 2
;-------------------------------------
loopciclo:
  CALL start
  CALL cambiopallet
  CALL contapezzi
;-------------------------------------
  IF SIG(selcontinuo) GOTO lcontinua
  JMOVE #attesa
  BREAK
  TYPE /C15
  TYPE "Ciclo singolo ... per continuare premere CYCLE START !!!"
  CALL monitor
  PAUSE
  CALL monitor
  CALL startup
  TYPE /C15
  tinizio = TIMER(0)
lcontinua:
;-------------------------------------
;***      loop a regime     ***
  CALL apriripresa1
  CALL apriripresa2
  CALL pallettizza1;*
  CALL compensa;*
  CALL cerca_fase1;*
  CALL rifasa1;*
  CALL dep_ripresa1;*
  CALL scarico_tor1;*
  CALL dep_ripresa2;*
  CALL prel_ripresa1;*
  CALL carico_tor1;*
  CALL scarico_tor2;*
  CALL scarico;*
  CALL prel_ripresa2;*
  CALL carico_tor2;*
  CALL esiti
  GOTO loopciclo
.END
.PROGRAM compensa()
;Azzera errore altezza prelievo.
  ACCURACY 50
  JMOVE #s_compensa
  CALL pausa
  CALL check_sq22m
  ACCURACY 5
  LAPPRO compensa,10
  ACCURACY 1
  SPEED 10
  DECEL 5
  LAPPRO compensa,4
  BREAK
  CALL apripinza1
  ACCURACY 1
  LMOVE compensa
  BREAK
  CALL check_sq22p
  CALL chiudipinza1
  ACCURACY 50
  LDEPART 150
  RETURN
.END
.PROGRAM contapezzi()
;Programma per arrestare il ciclo.
  IF contapezzi<=conta THEN
    JMOVE #attesa
    BREAK
    TYPE "Raggiunto contapezzi  CARONTE RIPOSA (e FRANCO lavora)!!!"
    HALT
  END
  RETURN
.END
.PROGRAM definiz_ciclo()
;*************************************************************
;* Gestione simulata tasto CYCLE START per anticollisione
;* Data     : 21 02 2003
;*************************************************************
;
;Definizioni di sistema
;
  mk_sel_ok = 0     ;Memoria stato selettori robot
  mk_off = 0        ;Memoria rilascio comandi simulati
  mk_ciclo_on = 0   ;Memoria fronte discesa Cycle Start
  mk_primo = 1      ;Memoria primo loop del programma
  scosta = 9999     ;Distanza posizione ripartenza
  puls_cycle = 1046 ;Ingresso Pulsante Cycle Start simulato
  puls_reset = 1020 ;Ingresso Selettore Reset Program simulato
  watch_dog = 1     ;Uscita controllo programma in running
;
  RETURN
.END
.PROGRAM dep_falda()
;Lascia l'interfalda nel pallet2.
  POINT dep_falda = pallet2+TRANS(0,0,(piano2*(int_piano2+int_falda2))+int_euro2,0,0,0)+grip_pal2+offset_inter2
  CALL pausa
  LMOVE #inter2
  ACCURACY 50
  LMOVE SHIFT(dep_falda BY 0,0,200)
  SPEED 20
  ACCURACY 5
  LMOVE SHIFT(dep_falda BY 0,0,50)
  ACCURACY 1
  DECEL 5
  SPEED 5
  LMOVE dep_falda
  BREAK
  STABLE 0.1
  CALL offvuoto
  SPEED 20
  ACCURACY 5
  LMOVE SHIFT(dep_falda BY 0,0,50)
  LMOVE SHIFT(dep_falda BY 0,0,200)
  MVWAIT 190 MM
  CALL check_sq8m
  LMOVE #inter2
  RETURN
.END
.PROGRAM dep_ripresa1()
;Deposita il pezzo nella ripresa1.
  CLOSE 1
  CALL pausa
  ACCURACY 50
  LMOVE #s_d_ripresa1
  ACCURACY 30
  LMOVE SHIFT(d_ripresa1+TRANS(0,0,-150,0,0,0) BY 0,0,20)
  CALL check_sq19p
  CALL check_sq22m
  CALL check_sq51m
  ACCURACY 5
  LMOVE SHIFT(d_ripresa1 BY 0,0,20)
  ACCURACY 1
  SPEED 10
  DECEL 5
  LMOVE d_ripresa1
  BREAK
  CALL chiudiripresa1
  CALL apripinza1
  ACCURACY 30
  LDEPART 100
  ACCURACY 50
  LMOVE #s_d_ripresa1
  LMOVE #u_d_ripresa1
  RETURN
.END
.PROGRAM dep_ripresa1_b()
;Deposita pezzo ripresa1 seconda fase.
  CLOSE 1
  CALL pausa
  JMOVE #passa_t
  ACCURACY 30
  JMOVE SHIFT(d_ripresa1_b+TRANS(0,0,-250,0,0,0) BY 0,0,7)
  CALL check_sq19p
  CALL check_sq51m
  ACCURACY 5
  LMOVE SHIFT(d_ripresa1_b BY 0,0,7)
  ACCURACY 1
  SPEED 10
  DECEL 5
  LMOVE d_ripresa1_b
  BREAK
  CALL chiudiripresa1
  CALL apripinza1
  ACCURACY 30
  LDEPART 100
  JMOVE #passa1
  RETURN
.END
.PROGRAM dep_ripresa2()
;Deposita il pezzo nella ripresa2.
  CLOSE 1
  CALL pausa
  JMOVE #passaggio
  ACCURACY 50
  JMOVE #s_d_ripresa2
  ACCURACY 30
  LMOVE SHIFT(d_ripresa2+TRANS(0,0,-250,0,0,0) BY 0,0,20)
  CALL check_sq50m
  CALL check_sq20p
  ACCURACY 5
  LMOVE SHIFT(d_ripresa2 BY 0,0,20)
  ACCURACY 1
  SPEED 10
  DECEL 5
  LMOVE d_ripresa2
  BREAK
  CALL chiudiripresa2
  CALL apripinza1
  ACCURACY 30
  LDEPART 200
  ACCURACY 50
  LMOVE #s_d_ripresa2
  RETURN
.END
.PROGRAM dep_staffa()
;Deposita la staffa dal magazzino.
  OPEN 2
  JMOVE #f_mag_staffe
  CALL pausa
  CALL chk_pos_fibra
  ACCURACY 5
  LMOVE #ustaffa[pnt_staffa]
  CASE pnt_staffa OF
   VALUE 1:
    CALL check_sq18m
   VALUE 2:
    CALL check_sq17m
   VALUE 3:
    CALL check_sq16m
   VALUE 4:
    CALL check_sq15m
  END
  ACCURACY 3
  SPEED 20
  DECEL 10
  LMOVE #estaffa[pnt_staffa]
  ACCURACY 1
  SPEED 10
  DECEL 5
  LMOVE #pstaffa[pnt_staffa]
  BREAK
  CALL chiudipinza2
  HERE partenza
  ACCURACY 5
  SPEED 20
  LMOVE #estaffa[pnt_staffa]
  dist_pre = 40
  CALL wait_out
  LMOVE #f_mag_staffe
  dist_pre = 200
  CALL wait_out
  CASE pnt_staffa OF
   VALUE 1:
    CALL check_sq18p
   VALUE 2:
    CALL check_sq17p
   VALUE 3:
    CALL check_sq16p
   VALUE 4:
    CALL check_sq15p
  END
  RETURN
.END
.PROGRAM deposito()
;Deposita il pezzo nel pallet 2.
  CLOSE 1
  CALL pausa
  ACCURACY 50
  JAPPRO deposito,250
  ACCURACY 5
  LAPPRO deposito,15
  ACCURACY 1
  SPEED 10
  LMOVE deposito
  BREAK
  CALL apripinza1
  ACCURACY 50
  LDEPART 200
  RETURN
.END
.PROGRAM diagnosi()
;Controlla stato di allarme.
  IF allarme<>0 THEN
    IF mem_allarme==1 GOTO 20
    mem_allarme = 1
    SIGNAL errore
    suona = 1
    CALL messaggi
20
    IF SIG(-tacita) THEN
      f_meno = 1
    END
    IF SIG(tacita) AND f_meno==1 THEN
      mem_allarme = 0
      f_meno = 0
      CALL tacitato
      SIGNAL -errore
      IF all_mas==1 THEN
        all_mas = 0
        vis.occupato = 0
      END
      suona = 0
      allarme = 0
    END
  END
  RETURN
.END
.PROGRAM esiti()
;Calcola tempi ciclo.
  IF conta==0 THEN
    ttot = 0
  END
  conta = conta+1
  globali = globali+1
  tcarico = TIMER(0)-tinizio
  tinizio = TIMER(0)
  ttot = ttot+tcarico
  aggiorna = 1
  RETURN
.END
.PROGRAM exe()
;Mette in execute i prg paralleli.
  CALL abo
  PCEXECUTE 2: autostart2.pc
  PCEXECUTE 3: autostart3.pc
  RETURN
.END
.PROGRAM generale()
;Gestione allarmi generali di macchina.
  CASE fase0 OF
   VALUE 1:
    IF ((TIMER(0)-t_staz0)<=1) GOTO 10
    fase0 = 2
10
   VALUE 2:
    IF SIG(psp1_1) GOTO 21 ;pressostato
    r_fase0 = fase0
    fase0 = 99
    allarme_0 = 6
    GOTO 200
21
    IF SIG(termiche) GOTO 22 ;termiche ok
    r_fase0 = fase0
    fase0 = 99
    allarme_0 = 7
    GOTO 200
22
    IF SIG(potenza) GOTO 23 ;potenza on
    r_fase0 = fase0
    fase0 = 99
    allarme_0 = 8
    GOTO 200
23
    fase0 = 1
    t_staz0 = TIMER(0)
200
   VALUE 99:
    IF vis.occupato==1 GOTO 990     ;visualizzatore occupato?
    vis.occupato = 1                ;occupa visualizzatore
    allarme = allarme_0             ;carica codice allarme
    fase0 = 100
990
   VALUE 100:
    IF allarme>0 GOTO 1000          ;attesa tacitazione allarme
    vis.occupato = 0                ;libera visualizzatore
    fase0 = r_fase0                 ;carica fase di rientro
    allarme_0 = 0                   ;pulisce coda di allarme
1000
  END
  RETURN
.END
.PROGRAM inizializza()
;Programma per l' inizializzazione totale dell' impianto
;da usare solo in caso di arresto non condizionato.
  CALL abo
  RESET
  accensione = 0
  fsuona = 1
  fase0 = 1                       ;azzera le fasi delle task
  fase1 = 1
  fase2 = 1
  fase3 = 1
  fase4 = 5
  fase5 = 1
  fase6 = 1
  fase7 = 1
  fase8 = 1
  fase9 = 1
  fase10 = 1
  fase11 = 1
  fase12 = 1
  fase13 = 1
  fase14 = 1
  fase15 = 1
  fase16 = 1
  fase17 = 1
  fase18 = 1
  fase19 = 1
  fase20 = 1
  fasem = 99
  via_robot0 = 0
  via_staz0 = 0
  via_robot1 = 0
  via_staz1 = 0
  via_robot2 = 0
  via_staz2 = 0
  via_robot3 = 0
  via_staz3 = 0
  via_robot4 = 0
  via_staz4 = 0
  via_robot5 = 0
  via_staz5 = 0
  via_robot6 = 0
  via_staz6 = 0
  via_robot7 = 0
  via_staz7 = 0
  via_robot8 = 0
  via_staz8 = 0
  via_robot9 = 0
  via_staz9 = 0
  via_robot10 = 0
  via_staz10 = 0
  via_robot11 = 0
  via_staz11 = 0
  via_robot12 = 0
  via_staz12 = 0
  via_robot13 = 0
  via_staz13 = 0
  via_robot14 = 0
  via_staz14 = 0
  via_robot15 = 0
  via_staz15 = 0
  via_robot16 = 0
  via_staz16 = 0
  via_robot17 = 0
  via_staz17 = 0
  via_robot18 = 0
  via_staz18 = 0
  via_robot19 = 0
  via_staz19 = 0
  via_robot20 = 0
  via_staz20 = 0
  vis.occupato = 0                ;libera visualizzatore
  allarme = 0                     ;pulisce la coda di allarme
  allarme_0 = 0
  allarme_1 = 0
  allarme_2 = 0
  allarme_3 = 0
  allarme_4 = 0
  allarme_5 = 0
  allarme_6 = 0
  allarme_7 = 0
  allarme_8 = 0
  allarme_9 = 0
  allarme_10 = 0
  allarme_11 = 0
  allarme_12 = 0
  allarme_13 = 0
  allarme_14 = 0
  allarme_15 = 0
  allarme_16 = 0
  allarme_17 = 0
  allarme_18 = 0
  allarme_19 = 0
  allarme_20 = 0
  allarme_m = 0
  accendi = 0
  togli_falda = 0
  CALL azzera.timer
  CALL tacitato
  CALL exe
  TYPE "Programma inizializzato."
  TYPE "Svuotare la macchina dai pezzi."
  HALT
.END
.PROGRAM inquadra_falda()
;Muove il robot alla posizione di ricerca interfalda a pallet e riconosce.
  IF riga1==0 AND colonna1==0 THEN
    offset_falda_x = -40
    offset_falda_y = 1000
    CALL punto_falda
    IF setta_visione==1 THEN
      CALL apricamera
      TYPE "SETTARE LA VISIONE PER INTERFALDA !!!"
      TYPE "... PREMERE CICLO PER CONTINUARE ..."
      PAUSE
    END
    trovato = 0
    tenta = 0
    DO
      $tx = "M2"
      CALL cerca
      IF flag==2 THEN
        POINT presa2 = pallet1+TRANS(offset_falda_x,offset_falda_y,(piano1*(int_piano1+int_falda1))+int_euro1,0,0,0)+offset_camera+fcam+TRANS(x,y,0,0,0,a)
        trovato = 1      ;informa robot del pezzo pronto
      END
      tenta = tenta+1
    UNTIL trovato==1 OR tenta>=3
    IF trovato==1 THEN
      offset_falda_x = -40
      offset_falda_y = -80
      CALL punto_falda
      IF setta_visione==1 THEN
        CALL apricamera
        TYPE "SETTARE LA VISIONE PER INTERFALDA !!!"
        TYPE "... PREMERE CICLO PER CONTINUARE ..."
        PAUSE
      END
      trovato = 0
      tenta = 0
      DO
        $tx = "M1"
        CALL cerca
        IF flag==1 THEN
          POINT presa1 = pallet1+TRANS(offset_falda_x,offset_falda_y,(piano1*(int_piano1+int_falda1))+int_euro1,0,0,0)+offset_camera+fcam+TRANS(x,y,0,0,0,a)
          DECOMPOSE rel1[0] = presa1
          DECOMPOSE rel2[0] = presa2
          a = -1*ATAN2((rel1[1]-rel2[1]),(rel1[0]-rel2[0]))
          CALL calcola_f
          trovato = 1      ;informa robot del pezzo pronto
        END
        tenta = tenta+1
      UNTIL trovato==1 OR tenta>=3
    END
    CALL chiudicamera
  ELSE
    TYPE "Gestione interfalda1 non possibile !!!"
    TYPE "puntatori riga1 e colonna1 non corretti..."
    BREAK
    HALT
  END
  RETURN
.END
.PROGRAM inquadra_pezzo()
;Muove il robot alla posizione di ricerca pezzo su pallet e riconosce.
  CALL punto_pezzo
  IF setta_visione==1 THEN
    CALL apricamera
    TYPE "SETTARE LA VISIONE PEZZO !!!"
    TYPE "... PREMERE CICLO PER CONTINUARE ..."
    PAUSE
  END
10
  trovato = 0
  tenta = 0
  DO
    $tx = "M0"
    CALL cerca
    IF flag==0 THEN
      a = 0
      CALL calcola_p
      trovato = 1               ;informa robot del pezzo pronto
    END
    tenta = tenta+1
  UNTIL trovato==1 OR tenta>=3
  IF trovato==0 AND stop_visione==1 THEN
    TYPE ""
    TYPE "Pezzo non riconosciuto!!!!"
    TYPE "Verifica la visione e premi Cycle Start..."
    PAUSE
    GOTO 10
  END
  SIGNAL -yv4_1 ;chiusura sportello camera
  RETURN
.END
.PROGRAM interfalda1()
;Controlla se togliere interfalda.
  IF togli_falda==1 THEN
    JMOVE #fuori_pallet1
    IF contr_piano==1 THEN
      contr_piano = 0
      allarme_m = 25  ;allarme pezzo non riconosciuto
      CALL master_all
      allarme_m = 25
      CALL master_all
    END
10
    piano1 = piano1+1
    CALL inquadra_falda
    piano1 = piano1-1
    IF trovato==0 THEN
      CALL apricamera
      allarme_m = 2  ;allarme falda non riconosciuta
      CALL master_all
      GOTO 10
    END
    JMOVE #fuori_pallet1
    IF SIG(sq6) THEN
      CALL misura
    ELSE
      misura = 0
    END
    CALL prendi_organo
    CALL prelievo_falda
    CALL lascia_falda
    CALL lascia_organo
    togli_falda = 0
    JMOVE #fuori_pallet1
  END
  RETURN
.END
.PROGRAM interfalda2()
;Controlla se mettere interfalda su pallet2.
  JMOVE #f_pallet2
10
  IF SIG(sq6) THEN
    CALL misura
  ELSE
    allarme_m = 23        ;mancanza interfalda a magazzino
    CALL master_all
    GOTO 10
  END
  CALL prendi_organo
  CALL prendi_falda
  CALL dep_falda
  CALL lascia_organo
  RETURN
.END
.PROGRAM io.ini()
;Definizione ingressi,uscite e segnali interni.
;
;******************************** INGRESSI *******************************
;
  sq1 = 1001                 ;presenza pallet boccole
  sq2 = 1002                 ;pallet di carico in posizione
  sq3 = 1003                 ;pallet di scarico in posizione
  sq21 = 1004                ;fotocellula ricerca fase pezzo 2
  sq5 = 1005                 ;presenza organo interfalde
  sq6 = 1006                 ;fotocellula presenza interfalde
  selcontinuo = 1007         ;selettore ciclo singolo continuo
  psp1_1 = 1008              ;pressostato linea aria
;
  sq8 = 1009                 ;presenza interfalda
  sq9 = 1010                 ;fotocellula tasteggio robot
  sq10 = 1011                ;sportello camera aperto
  sq11 = 1012                ;sportello camera chiuso
  sq12 = 1013                ;fotocellula nastro scarico libero
  sq13 = 1014                ;fotocellula nastro scarico pieno
  sb1 = 1015                 ;selettore ciclo nastro continuo
  sb2 = 1016                 ;pulsante avanza nastro scarico
;
  termiche = 1017            ;termiche ok
  potenza = 1018             ;macchina in emergenza
  tacita = 1019              ;pulsante tacitazione allarmi
;1020                      ;reset programma (SYS)
  sq15 = 1021                ;presenza staffa 1
  sq16 = 1022                ;presenza staffa 2
  sq17 = 1023                ;presenza staffa 3
  sq18 = 1024                ;presenza staffa 4
;
  in1_tornio1 = 1025         ;mandrino aperto
  in2_tornio1 = 1026         ;mandrino chiuso
  in3_tornio1 = 1027         ;richiesta scarico pezzo
  in4_tornio1 = 1028         ;richiesta carico pezzo
  in5_tornio1 = 1029         ;tornio automatico
  in6_tornio1 = 1030         ;porta aperta
  in1_tornio2 = 1031         ;mandrino aperto
  in2_tornio2 = 1032         ;mandrino chiuso
;
  in3_tornio2 = 1033         ;richiesta scarico pezzo
  in4_tornio2 = 1034         ;richiesta carico pezzo
  in5_tornio2 = 1035         ;tornio automatico
  in6_tornio2 = 1036         ;porta aperta
  in1_pressa = 1037          ;pressa alta (pronta al carico)
  in2_pressa = 1038          ;pressatura in corso
  in3_pressa = 1039          ;riserva
  in4_pressa = 1040          ;riserva
;
  sq19 = 1041                ;fermo pezzo ripresa 1 alto
  sq20 = 1042                ;fermo pezzo ripresa 2 alto
  sq30 = 1043                ;posaggio pressa libero
  sq22 = 1044                ;posaggio ripresa verticale libero
  sq23 = 1045                ;controllo presenza boccola in pinza
;1046                        ;pulsante cycle start simulato (SYS)
  sq24 = 1047                ;tastatore polso robot
;1048                        ;disponibile
;
  sq50 = 1049                ;ripresa 2 chiusa
  sq51 = 1050                ;ripresa 1 chiusa
  sq_fibra = 1051            ;controllo pos. staffa magazzino
  sq52 = 1052                ;fotocellula ricerca fase pezzo 1
;1053                      ;disponibile
;1054                      ;disponibile
;1055                      ;disponibile
;1056                      ;disponibile
;
;1057                      ;disponibile
;1058                      ;disponibile
;1059                      ;disponibile
;1060                      ;disponibile
;1061                      ;disponibile
;1062                      ;disponibile
;1063                      ;disponibile
;1064                      ;disponibile
;
;******************************** USCITE *******************************
;
;1                         ;watch dog (SYS)
  in_ciclo = 2               ;cycle start (SYS)
;3                         ;error robot (SYS)
  errore = 4                 ;allarme da programma
  in_teach = 5               ;teach mode robot (SYS)
  km1 = 6                    ;start nastro scarico
  lamp_blu = 7               ;lampada mancanza pezzi
  yv6_1 = 8                  ;soffiatura pezzo lavorato
;
  yv2a_1 = 9                 ;pinza 1 (CLAMP 1)
  yv2r_1 = 10                ;pinza 1
  yv3a_1 = 11                ;pinza 2 (CLAMP 2)
  yv3r_1 = 12                ;pinza 2
  yv4_1 = 13                 ;apertura sportello (CLAMP 3)
;14                        ;riserva
  yv1_1 = 15                 ;vuoto (CLAMP 4)
  yv5_1 = 16                 ;pressurizzazione camera
;
  rl3 = 17                   ;illuminazione pallet prelievo
  rl4 = 18                   ;illuminazione predisposta
  yv7a_1 = 19                ;apertura ripresa 1
  yv7r_1 = 20                ;chiusura ripresa 1
  yv8a_1 = 21                ;apertura ripresa 2
  yv8r_1 = 22                ;chiusura ripresa 2
  sirena = 23                ;sirena
;24                        ;run-hold da inter. panel
;
  out1_tornio1 = 25          ;richiesta apertura mandrino
  out2_tornio1 = 26          ;richiesta chiusura mandrino
  out3_tornio1 = 27          ;fine scarico
  out4_tornio1 = 28          ;fine carico
  out5_tornio1 = 29          ;fuori ingombro
  out1_tornio2 = 30          ;richiesta apertura mandrino
  out2_tornio2 = 31          ;richiesta chiusura mandrino
  out3_tornio2 = 32          ;fine scarico
;
  out4_tornio2 = 33          ;fine carico
  out5_tornio2 = 34          ;fuori ingombro
  out1_pressa = 35           ;start pressatura
  out2_pressa = 36           ;robot fuori ingombro
  out3_pressa = 37           ;riserva 
  out4_pressa = 38           ;riserva
;39                        ;disponibile
;40                        ;disponibile
;
;41                        ;disponibile
;42                        ;disponibile
;43                        ;disponibile
;44                        ;disponibile
;45                        ;disponibile
;46                        ;disponibile
;47                        ;disponibile
;48                        ;disponibile
;
;49                        ;disponibile
;50                        ;disponibile
;51                        ;disponibile
;52                        ;disponibile
;53                        ;disponibile
;54                        ;disponibile
;55                        ;disponibile
;56                        ;disponibile
;
;57                        ;disponibile
;58                        ;disponibile
;59                        ;disponibile
;60                        ;disponibile
;61                        ;disponibile
;62                        ;disponibile
;63                        ;disponibile
;64                        ;disponibile
;
;*********************** SEGNALI INTERNI ********************************
  sel_svuota = 2001
  sel_camp = 2002            ;numero pezzi scarico nastro a 10 bit
;2003
;2004
;2005
;2006
;2007
;2008
;2009
;2010
;2011
;
  lamp1 = 2020               ;lampada 1 i/f
  lamp2 = 2021               ;lampada 1 i/f
  lamp3 = 2022               ;lampada 1 i/f
  lamp4 = 2023               ;lampada 1 i/f
  lamp5 = 2024               ;lampada 1 i/f
  lamp6 = 2025               ;lampada 1 i/f
  lamp7 = 2026               ;lampada 1 i/f
  lamp8 = 2027               ;lampada 1 i/f
  lamp9 = 2028               ;lampada 1 i/f
  lamp10 = 2029              ;lampada 1 i/f
  lamp11 = 2030              ;lampada 1 i/f
;2035                              ;segnale per abilitazione I/F
  sel_scarico = 2036         ;scelta scarico su nastro o pallet
  p1_t = 2037                ;pulsante apertura ripresa1
  p2_t = 2038                ;pulsante chiusura ripresa1
  p3_t = 2039                ;pulsante apertura ripresa2
  p4_t = 2040                ;pulsante chiusura ripresa2
;
  RETURN
.END
.PROGRAM lampeggia()
;Genera impulso programmabile.
  CASE flash OF
   VALUE 0:
    IF ((TIMER(0)-t_flash)>=t_clock) THEN
      flash = 1
      t_flash = TIMER(0)
    END
   VALUE 1:
    IF ((TIMER(0)-t_flash)>=t_clock) THEN
      flash = 0
      t_flash = TIMER(0)
    END
  END
  RETURN
.END
.PROGRAM lascia_falda()
;Lascia l'interfalda nel magazzino.
  OPEN 1
  OPEN 2
  JMOVE #f_magazzino2
  CALL pausa
  TOOL pinza2
  ACCURACY 10
  JAPPRO #dep_mag,misura+100
  SPEED 20
  ACCURACY 1
  LAPPRO #dep_mag,misura+15
  SPEED 5
  ACCURACY 1
  DECEL 5
  LAPPRO #dep_mag_spi,misura
  BREAK
  CALL offvuoto
  SPEED 30
  ACCURACY 20
  LDEPART 300
  MVWAIT 200 MM
  CALL check_sq8m
  CALL check_sq6p
  TOOL pinza1
  JMOVE #f_magazzino
  RETURN
.END
.PROGRAM lascia_organo()
;Deposita organo interfalde dal magazzino.
  CLOSE 4
  OPEN 2
  JMOVE #u_organo
  CALL pausa
10
  IF SIG(-sq5) GOTO 20
  allarme_m = 31  ;allarme eff. presenza organo
  CALL master_all
  GOTO 10
20
  ACCURACY 20
  JMOVE #s_organo
  ACCURACY 3
  SPEED 30
  LMOVE #v_organo
  ACCURACY 1
  SPEED 2
  DECEL 5
  LMOVE #organo
  BREAK
  STABLE 0.2
  HERE partenza
  CALL chiudipinza2
  ACCURACY 1
  SPEED 5
  ACCEL 5
  LMOVE #v_organo
  ACCURACY 20
  SPEED 30
  LMOVE #s_organo
  dist_pre = 100
  CALL wait_out
30
  IF SIG(sq5) GOTO 40
  allarme_m = 30   ;allarme mancanza organo
  CALL master_all
  GOTO 30
40
  JMOVE #fuori_organo
  RETURN
.END
.PROGRAM master_all()
;Gestione allarme Task principale con Stop robot.
10
  CASE fasem OF
   VALUE 99:
    WAIT vis.occupato==0          ;visualizzatore libero?
    vis.occupato = 1              ;occupa visualizzatore
    all_mas = 1
    allarme = allarme_m           ;carica codice allarme
    fasem = 100
    BREAK
    BREAK
    HERE #last
   VALUE 100:
    IF allarme>0 THEN             ;attesa tacitazione allarme
      PAUSE
      CALL restart
    ELSE
      vis.occupato = 0              ;libera visualizzatore
      allarme_m = 0                 ;pulisce la coda di allarme
      fasem = 99
    END
  END
  IF allarme_m>0 GOTO 10        ;attesa allarme tacitato
  TIMER (1) = 0
  RETURN
.END
.PROGRAM messaggi()
;Prepara le stringhe per la visualizzazione dell' allarme.
  abilita = 1
  $mess0 = " A L L A R M E                      "
  $codice = $ENCODE(/F3.0,allarme)
  $spazio = " "
  CASE allarme OF
   VALUE 1:
    $mess1 = "ROBOT NON IN POSIZIONE DI PARTENZA"
    $mess2 = "PORTARLO IN #ATTESA"
   VALUE 2:
    $mess1 = "PEZZO NON RICONOSCIUTO SU INTERFALDA"
    $mess2 = "VERIFICARE"
   VALUE 3:
    $mess1 = "TASK PARALLELE NON IN RUN"
    $mess2 = "F.M. -> OFF -> ON"
   VALUE 4:
    $mess1 = "ROBOT NON USCITO DA INGOMBRO"
    $mess2 = "CONTINUO ???"
   VALUE 5:
    $mess1 = "MAGAZZINO BOCCOLE VUOTO"
    $mess2 = "RIEMPIRLO !!!"
   VALUE 6:
    $mess1 = "MANCANZA ARIA COMPRESSA"
    $mess2 = "PSP1_1 -> +"
   VALUE 7:
    $mess1 = "SCATTO MAGNETOTERMICI"
    $mess2 = "MT1 -> +"
   VALUE 8:
    $mess1 = "EMERGENZA PREMUTA"
    $mess2 = "SBLOCCARE PULSANTI A FUNGO"
;
;
;
   VALUE 10:
    $mess1 = "SPORTELLO CAMERA NON APERTO"
    $mess2 = "SQ10 -> + E SQ11 -> -"
   VALUE 11:
    $mess1 = "SPORTELLO CAMERA NON CHIUSO"
    $mess2 = "SQ10 -> - E SQ11 -> +"
   VALUE 12:
    $mess1 = "MANCANZA PRESA INTERFALDA"
    $mess2 = "SQ8 -> +"
   VALUE 13:
    $mess1 = "EFFICENZA PRESENZA INTERFALDA"
    $mess2 = "SQ8 -> -"
   VALUE 14:
    $mess1 = "PALLET CARICO NON IN POSIZIONE"
    $mess2 = "SQ3 -> +"
   VALUE 15:
    $mess1 = "PALLET SCARICO NON IN POSIZIONE"
    $mess2 = "SQ4 -> +"
   VALUE 16:
    $mess1 = "MAGAZZINO BOCCOLE NON IN POSIZIONE"
    $mess2 = "SQ1 -> +"
;
   VALUE 20:
    $mess1 = "INTERFALDA NON RICONOSCIUTA"
    $mess2 = "VERIFICARE"
   VALUE 21:
    $mess1 = "EFFICENZA FOTOCELLULA INTERFALDE"
    $mess2 = "SQ9 -> -"
   VALUE 22:
    $mess1 = "MANCANZA FOTOCELLULA INTERFALDE"
    $mess2 = "SQ9 -> +"
   VALUE 23:
    $mess1 = "MANCANZA INTERFALDA A MAGAZZINO"
    $mess2 = "SQ6 -> +"
   VALUE 24:
    $mess1 = "MACCHINA IN SVUOTAMENTO"
    $mess2 = "RUOTARE SELETTORE SVUOTAMENTO"
   VALUE 25:
    $mess1 = "PEZZO NON RICONOSCIUTO"
    $mess2 = "TOGLIERLO E LIBERARE L\'INTERFALDA "
;
;
;
   VALUE 30:
    $mess1 = "MANCANZA ORGANO INTERFALDE"
    $mess2 = "SQ5 -> +"
   VALUE 31:
    $mess1 = "EFFICENZA PRESENZA ORGANO INTERFALDE"
    $mess2 = "SQ5 -> -"
;
;
;
   VALUE 35:
    $mess1 = "EFFICENZA FOTOCELLULA FASATURA"
    $mess2 = "SQ21 -> -"
   VALUE 36:
    $mess1 = "FOTOCELLULA FASATURA NON OSCURATA"
    $mess2 = "SQ21 -> +"
   VALUE 37:
    $mess1 = "FOTOCELLULA FASATURA NON LIBERA"
    $mess2 = "SQ21 -> -"
;
;
;
   VALUE 40:
    $mess1 = "ZONA SCARICO NASTRO NON LIBERA"
    $mess2 = "SQ12 -> -"
   VALUE 41:
    $mess1 = "ZONA SCARICO NASTRO NON OCCUPATA"
    $mess2 = "SQ12 -> +"
   VALUE 42:
    $mess1 = "NASTRO SCARICO TROPPO PIENO"
    $mess2 = "SQ13 -> -"
   VALUE 43:
    $mess1 = "NASTRO SCARICO NON PIENO"
    $mess2 = "SQ13 -> +"
   VALUE 44:
    $mess1 = "PEZZO NON TOLTO DA NASTRO SCARICO"
    $mess2 = "SQ13 -> -"
;
;
;
   VALUE 50:
    $mess1 = "FERMO PEZZO RIPRESA1 NON ALTO"
    $mess2 = "SQ19 -> + E SQ51 -> -"
   VALUE 51:
    $mess1 = "EFFICENZA FERMO PEZZO RIPRESA1 ALTO"
    $mess2 = "SQ19 -> -"
   VALUE 52:
    $mess1 = "FERMO PEZZO RIPRESA2 NON ALTO"
    $mess2 = "SQ20 -> + E SQ50 -> -"
   VALUE 53:
    $mess1 = "EFFICENZA FERMO PEZZO RIPRESA2 ALTO"
    $mess2 = "SQ20 -> -"
   VALUE 55:
    $mess1 = "MANCANZA STAFFA 1 A MAGAZZINO"
    $mess2 = "SQ15 -> +"
   VALUE 56:
    $mess1 = "EFFICENZA PRESENZA STAFFA 1 A MAGAZZINO"
    $mess2 = "SQ15 -> -"
   VALUE 57:
    $mess1 = "MANCANZA STAFFA 2 A MAGAZZINO"
    $mess2 = "SQ16 -> +"
   VALUE 58:
    $mess1 = "EFFICENZA PRESENZA STAFFA 2 A MAGAZZINO"
    $mess2 = "SQ16 -> -"
   VALUE 59:
    $mess1 = "MANCANZA STAFFA 3 A MAGAZZINO"
    $mess2 = "SQ17 -> +"
   VALUE 60:
    $mess1 = "EFFICENZA PRESENZA STAFFA 3 A MAGAZZINO"
    $mess2 = "SQ17 -> -"
   VALUE 61:
    $mess1 = "MANCANZA STAFFA 4 A MAGAZZINO"
    $mess2 = "SQ18 -> +"
   VALUE 62:
    $mess1 = "EFFICENZA PRESENZA STAFFA 4 A MAGAZZINO"
    $mess2 = "SQ18 -> -"
   VALUE 63:
    $mess1 = "FERMO PEZZO RIPRESA1 BASSO"
    $mess2 = "SQ51 -> -"
   VALUE 64:
    $mess1 = "FERMO PEZZO RIPRESA2 BASSO"
    $mess2 = "SQ50 -> -"
;
;
;
   VALUE 70:
    $mess1 = "MAGAZZINO INTERFALDE NON OCCUPATO"
    $mess2 = "SQ6 -> +"
   VALUE 71:
    $mess1 = "MAGAZZINO INTERFALDE NON LIBERO"
    $mess2 = "SQ6 -> -"
;
;
;
   VALUE 76:
    $mess1 = "POSAGGIO PRESSA NON LIBERO"
    $mess2 = "SQ30 -> -"
   VALUE 77:
    $mess1 = "POSAGGIO PRESSA NON OCCUPATO"
    $mess2 = "SQ30 -> +"
   VALUE 78:
    $mess1 = "POSAGGIO RIPRESA VERTICALE NON LIBERO"
    $mess2 = "SQ22 -> -"
   VALUE 79:
    $mess1 = "POSAGGIO RIPRESA VERTICALE NON OCCUPATO"
    $mess2 = "SQ22 -> +"
   VALUE 80:
    $mess1 = "MANCANZA BOCCOLA IN PINZA"
    $mess2 = "SQ23 -> +"
   VALUE 81:
    $mess1 = "EFFICENZA PRESENZA BOCCOLA IN PINZA"
    $mess2 = "SQ23 -> -"
   VALUE 82:
    $mess1 = "MANCANZA BOCCOLA NEL MAGAZZINO"
    $mess2 = "SQ24 -> +"
;
;
;
   VALUE 100:
    $mess1 = "TORNIO1 NON PRONTO AL CARICO"
    $mess2 = "CONTINUO ???"
   VALUE 101:
    $mess1 = "TORNIO1 NON PRONTO ALLO SCARICO"
    $mess2 = "CONTINUO ???"
   VALUE 102:
    $mess1 = "PORTA TORNIO1 NON APERTA"
    $mess2 = "CONTINUO ???"
   VALUE 103:
    $mess1 = "PORTA TORNIO1 NON CHIUSA"
    $mess2 = "CONTINUO ???"
   VALUE 104:
    $mess1 = "CICLO TORNIO1 NON AVVIATO"
    $mess2 = "CONTINUO ???"
   VALUE 105:
    $mess1 = "CICLO TORNIO1 NON TERMINATO"
    $mess2 = "CONTINUO ???"
   VALUE 106:
    $mess1 = "MANDRINO TORNIO1 NON APERTO"
    $mess2 = "CONTINUO ???"
   VALUE 107:
    $mess1 = "MANDRINO TORNIO1 NON CHIUSO"
    $mess2 = "CONTINUO ???"
   VALUE 108:
    $mess1 = "PROBLEMA INTERFACCIA TORNIO1"
    $mess2 = "CONTINUO ???"
   VALUE 109:
    $mess1 = "EFFICENZA MANDRINO TORNIO1 APERTO"
    $mess2 = "CONTINUO ???"
   VALUE 110:
    $mess1 = "EFFICENZA MANDRINO TORNIO1 CHIUSO"
    $mess2 = "CONTINUO ???"
;
;
;
   VALUE 120:
    $mess1 = "TORNIO2 NON PRONTO AL CARICO"
    $mess2 = "CONTINUO ???"
   VALUE 121:
    $mess1 = "TORNIO2 NON PRONTO ALLO SCARICO"
    $mess2 = "CONTINUO ???"
   VALUE 122:
    $mess1 = "PORTA TORNIO2 NON APERTA"
    $mess2 = "CONTINUO ???"
   VALUE 123:
    $mess1 = "PORTA TORNIO2 NON CHIUSA"
    $mess2 = "CONTINUO ???"
   VALUE 124:
    $mess1 = "CICLO TORNIO2 NON AVVIATO"
    $mess2 = "CONTINUO ???"
   VALUE 125:
    $mess1 = "CICLO TORNIO2 NON TERMINATO"
    $mess2 = "CONTINUO ???"
   VALUE 126:
    $mess1 = "MANDRINO TORNIO2 NON APERTO"
    $mess2 = "CONTINUO ???"
   VALUE 127:
    $mess1 = "MANDRINO TORNIO2 NON CHIUSO"
    $mess2 = "CONTINUO ???"
   VALUE 128:
    $mess1 = "PROBLEMA INTERFACCIA TORNIO2"
    $mess2 = "CONTINUO ???"
   VALUE 129:
    $mess1 = "EFFICENZA MANDRINO TORNIO2 APERTO"
    $mess2 = "CONTINUO ???"
   VALUE 130:
    $mess1 = "EFFICENZA MANDRINO TORNIO2 CHIUSO"
    $mess2 = "CONTINUO ???"
;
;
;
   VALUE 140:
    $mess1 = "PRESSA NON PRONTA AL CARICO"
    $mess2 = "CONTINUO ???"
   VALUE 141:
    $mess1 = "CICLO PRESSA NON AVVIATO"
    $mess2 = "CONTINUO ???"
   VALUE 142:
    $mess1 = "CICLO PRESSA NON TERMINATO"
    $mess2 = "CONTINUO ???"
;
;
   VALUE 300:
    $mess1 = "EFFICENZA TASTATORE"
    $mess2 = "SQ24 -> -"
   VALUE 301:
    $mess1 = "PEZZO RILEVATO STORTO"
    $mess2 = "RIPROVO ?"
;
;
   VALUE 310:
    $mess1 = "PRESENZA STAFFA IN CONTROLLO"
    $mess2 = "ATTESA SPENTA ... RIPROVO ?"
   VALUE 311:
    $mess1 = "STAFFA IN POSIZIONE ERRATA"
    $mess2 = "SISTEMARE ... RIPROVO ?"
;
;
   VALUE 700:
    $mess1 = "MAGAZZINO INTERFALDE TROPPO"
    $mess2 = "PIENO ... TOGLIERE !!!"
;
   ANY :
    $mess1 = "CODICE ALLARME SCONOSCIUTO"
    $mess2 = "CONTROLLARE"
  END
  $tronca = $mess1
  CALL tronca
  $mess1 = $tronca
  $tronca = $mess2
  CALL tronca
  $mess2 = $tronca
  $mess3 = " PREMERE TACITAZIONE ALLARME ....       "
  TYPE ""
  $messaggio = $mess0+$codice+$spazio+$mess1+$mess2+$mess3
  IFPWPRINT 1,1,1,2=$messaggio
  TYPE $mess0+$codice+$spazio
  TYPE $mess1
  TYPE $mess2
  TYPE $mess3
  RETURN
.END
.PROGRAM misura()
;Calcola il fattore di correzione altezza interfalde.
  JMOVE #fuori_organo
  JMOVE #out_tasta
  ACCURACY 10
  JMOVE tasta_mag+TRANS(0,200,0,0,0,0)
10
  BREAK
  TWAIT 0.2
  IF SIG(sq9) THEN
    allarme_m = 21 ;allarme eff. fotocellula tast.
    CALL master_all
    GOTO 10
  END
  ABS.SPEED ON
  ACCURACY 1
  SPEED 20 MM/S
  XMOVE tasta_mag TILL sq9 ;ricerca livello interfalda
  BREAK
  STABLE 0.2
  ABS.SPEED OFF
  IF calibra_mag==1 AND SIG(-sq6) THEN
    calibra_mag = 0
    HERE zero_magazzino
  END
  misura = DISTANCE(HERE,zero_magazzino)
  IF (misura<-5) THEN
    allarme_m = 22 ;allarme fotocellula tast.
    CALL master_all
    LMOVE tasta_mag+TRANS(0,200,0,0,0,0)
    GOTO 10
  END
  IF (misura>75) THEN
    allarme_m = 700 ;allarme magazzino troppo pieno
    CALL master_all
    LMOVE tasta_mag+TRANS(0,200,0,0,0,0)
    GOTO 10
  END
  IF tdebug>0 THEN
    TYPE "ALTEZZA INTERFALDA RILEVATA: ",misura," mm."
  END
  ACCURACY 10
  LMOVE tasta_mag+TRANS(0,200,0,0,0,0)
  JMOVE #out_tasta
  RETURN
.END
.PROGRAM monitor()
;Stampa a video i dati del programma in ciclo.
  abilita = 1
  $video0 = "        T  H  E  S  Y  S   s.r.l.       "
  $video1 = "          Montichiari - BRESCIA         "
  $video2 = "            Tel.  030-9961811           "
  $video3 = "    www.thesys.it  techdept@thesys.it   "
  $video = $video0+$video1+$video2+$video3
  IFPWPRINT 3,1,1,7=$video
  RETURN
.END
.PROGRAM nastro()
;Deposita il pezzo da controllare sul nastro.
  JMOVE #passaggio
  JMOVE #f_nastro
10
  CALL pausa
  IF via_robot4==0 GOTO 10     ;attesa nastro pronto
  stop_4 = 1
20
  IF SIG(sq12) THEN       ;Controllo zona libera
    allarme_m = 40          ;allarme zona scarico non libera
    CALL master_all
    GOTO 20
  END
  ACCURACY 30
  LMOVE #s_nastro
  SPEED 50
  ACCURACY 1
  LMOVE #nastro
  BREAK
  HERE partenza
  TIMER (1) = 0
30
  IF SIG(sq12) GOTO 40
  IF TIMER(1)>5 THEN
    allarme_m = 41          ;allarme zona scarico non occupata
    CALL master_all
  END
  GOTO 30
40
  CALL apripinza1
  ACCURACY 100
  LMOVE SHIFT(HERE BY 0,0,200)
  dist_pre = 100
  CALL wait_out
  via_robot4 = 0
  via_staz4 = 1
  LMOVE #f_nastro
  JMOVE #passaggio
  RETURN
.END
.PROGRAM offvuoto()
;Spegne il vuoto.
  CLOSEI 4
  TWAIT t_vuoto/2
  RETURN
.END
.PROGRAM onvuoto()
;Accende il vuoto.
  OPENI 4
  TWAIT t_vuoto
  RETURN
.END
.PROGRAM pallettizza1()
;Prelievo pezzo nel pallet 1.
  JMOVE #fuori_pallet1
5
  IF fine_pallet1==1 THEN
    SPEED 30 ALWAYS
    JMOVE #fuori_pallet1
    JMOVE #attesa
    BREAK
    accendi = 1
    suona = 1
    TYPE "Pallet ultimato !!!"
    HALT
  END
  CALL pausa
;  CALL check_pal_car
  CALL interfalda1
10
  CALL inquadra_pezzo
  IF trovato==1 THEN
    tentacerca = 0
    CALL prelievo
  ELSE
    contr_piano = 1
    tentacerca = tentacerca+1
    IF tentacerca>n_cerca_max THEN
      tentacerca = 1
      CALL apricamera
      allarme_m = 2  ;allarme pezzo non trovato
      CALL master_all
      GOTO 10
    END
  END
;
  riga1 = riga1+1
  IF riga1>=n_riga1 THEN
    riga1 = 0
    colonna1 = colonna1+1
    IF colonna1>=n_colonna1 THEN
      colonna1 = 0
      piano1 = piano1-1
      IF piano1<0 THEN
        piano1 = n_piano1-1
        togli_falda = 0
        fine_pallet1 = 1
      ELSE
        togli_falda = 1
      END
    END
  END
  IF tentacerca<>0 GOTO 5
  RETURN
.END
.PROGRAM pallettizza2()
;Deposito pezzo nel pallet 2.
  JMOVE #f_pallet2
; CALL check_pal_scar
  CALL chiudicamera
  POINT deposito = pallet2+TRANS(riga2*int_riga2,colonna2*int_col2,(piano2*(int_piano2+int_falda2))+int_euro2,0,0,0)+grip_pal2
  CALL deposito
;
  riga2 = riga2+1
  IF riga2>=n_riga2 THEN
    riga2 = 0
    colonna2 = colonna2-1
    IF colonna2<0 THEN
      colonna2 = n_colonna2-1
      CALL interfalda2
      piano2 = piano2+1
    END
  END
  JMOVE #f_pallet2
  RETURN
.END
.PROGRAM pausa()
;Arresta il robot se task parallele in allarme.
10
  IF allarme==0 GOTO 20
  PAUSE
  GOTO 10
20
  RETURN
.END
.PROGRAM prel_ripresa1()
;Preleva il pezzo dalla ripresa1.
  OPEN 1
  CALL pausa
  JMOVE #passa1
  JMOVE #passa2
  ACCURACY 5
  JAPPRO p_ripresa1,100
  CALL check_sq19m
  CALL check_sq51m
  ACCURACY 1
  LMOVE p_ripresa1
  BREAK
  CALL chiudipinza1
  CALL apriripresa1
  ACCURACY 5
  LMOVE SHIFT(p_ripresa1 BY 0,0,20)
  ACCURACY 30
  LDEPART 250
  LMOVE #s_p_ripresa1
  RETURN
.END
.PROGRAM prel_ripresa1_f()
;Preleva il pezzo finito dalla ripresa1.
  OPEN 1
  CALL pausa
  JMOVE #passa_t
  ACCURACY 5
  JAPPRO d_ripresa1_f,250
  CALL check_sq51m
  CALL check_sq19m
  ACCURACY 1
  LMOVE d_ripresa1_f
  BREAK
  CALL chiudipinza1
  CALL apriripresa1
  ACCURACY 5
  LMOVE SHIFT(d_ripresa1_f BY 0,0,7)
  ACCURACY 30
  LDEPART 250
  ACCURACY 50
  JMOVE #passa_t
  RETURN
.END
.PROGRAM prel_ripresa2()
;Preleva il pezzo dalla ripresa2.
  OPEN 1
  CALL pausa
  JMOVE #passaggio
  ACCURACY 50
  JMOVE #s_p_ripresa2
  ACCURACY 5
  LAPPRO p_ripresa2,100
  CALL check_sq50m
  CALL check_sq20m
  ACCURACY 1
  LMOVE p_ripresa2
  BREAK
  CALL chiudipinza1
  CALL apriripresa2
  ACCURACY 5
  LMOVE SHIFT(p_ripresa2 BY 0,0,20)
  ACCURACY 30
  LDEPART 150
  LMOVE #s_p_ripresa2
  RETURN
.END
.PROGRAM prel_staffa()
;Preleva la staffa dal magazzino.
  CLOSE 2
  JMOVE #f_mag_staffe
  CALL pausa
  ACCURACY 5
  LMOVE #estaffa[pnt_staffa]
  CASE pnt_staffa OF
   VALUE 1:
    CALL check_sq18p
   VALUE 2:
    CALL check_sq17p
   VALUE 3:
    CALL check_sq16p
   VALUE 4:
    CALL check_sq15p
  END
  ACCURACY 1
  SPEED 10
  DECEL 5
  LMOVE #pstaffa[pnt_staffa]
  BREAK
  CALL apripinza2
  HERE partenza
  ACCURACY 3
  SPEED 20
  LMOVE #ustaffa[pnt_staffa]
  dist_pre = 100
  CALL wait_out
  CASE pnt_staffa OF
   VALUE 1:
    CALL check_sq18m
   VALUE 2:
    CALL check_sq17m
   VALUE 3:
    CALL check_sq16m
   VALUE 4:
    CALL check_sq15m
  END
  LMOVE #f_mag_staffe
  RETURN
.END
.PROGRAM prelievo()
  OPEN 1                 ;esegue il prelievo da visione
  WAIT trovato==1      ;attende la risposta dal prog. concorrente
  trovato = 0
  mox = 0
  moy = 0
  IF colonna1==0 THEN
    mox = -200
  END
  IF colonna1>3 THEN
    mox = 400
  END
  IF riga1==0 THEN
    moy = 300
  END
  IF riga1>1 THEN
    moy = -250
  END
  JMOVE SHIFT(HERE BY mox,moy,0)
  ACCURACY 10
  JAPPRO presa,200
  CALL chiudicamera
  ACCURACY 5
  LAPPRO presa,50
  ACCURACY 1
  SPEED 50
  LMOVE presa
  BREAK
  CALL chiudipinza1
  POINT soprap = SHIFT(presa BY 0,0,sopra)
  ACCURACY 50
  LMOVE soprap
  JMOVE #uscita_pallet1
  RETURN
.END
.PROGRAM prelievo_boccol()
;esegue il prelievo della boccole da magazzino.
  OPEN 2
  ACCURACY 5
  LMOVE s_presa_pallet
  ACCURACY 1
  SPEED 30
  LMOVE presa_pallet
  BREAK
  CALL chiudipinza2
  ACCURACY 3
  SPEED 30
  LMOVE s_presa_pallet
  RETURN
.END
.PROGRAM prelievo_falda()
;Prende l'interfalda dal pallet1.
  OPEN 1
  OPEN 2
  JMOVE #pass_inter_rit
  JMOVE #inter
  LMOVE #inter1
  WAIT trovato==1      ;attende la risposta dal prog. concorrente
  trovato = 0
  ACCURACY 50
  LMOVE SHIFT(presa BY 0,0,200)
  ACCURACY 5
  SPEED 20
  LMOVE SHIFT(presa BY 0,0,50)
  ACCURACY 1
  SPEED 5
  DECEL 5
  LMOVE presa
  BREAK
  STABLE 0.1
  CALL onvuoto
  SPEED 20
  ACCURACY 5
  LMOVE SHIFT(presa BY 0,0,50)
  LMOVE SHIFT(presa BY 0,0,200)
  MVWAIT 190 MM
  CALL check_sq8p
  LMOVE #inter1
  LMOVE #inter
  LMOVE #pass_inter
  RETURN
.END
.PROGRAM prendi_falda()
;Prende l'interfalda dal magazzino.
  OPEN 1
  OPEN 2
  JMOVE #f_magazzino
  CALL pausa
  TOOL pinza2
  ACCURACY 10
  JAPPRO #prel_mag,misura+100
  SPEED 20
  ACCURACY 1
  LAPPRO #prel_mag,misura+15
  SPEED 5
  ACCURACY 1
  DECEL 5
  LAPPRO #prel_mag,misura
  BREAK
  CALL onvuoto
  SPEED 20
  ACCURACY 1
  LAPPRO #prel_mag_sop,misura
  ACCURACY 50
  LDEPART 300
  MVWAIT 200 MM
  CALL check_sq8p
  TOOL pinza1
  JMOVE #f_magazzino
  JMOVE #pass_inter
  RETURN
.END
.PROGRAM prendi_organo()
;Preleva organo interfalde dal magazzino.
  CLOSE 2
  CLOSE 4
  JMOVE #fuori_organo
  CALL pausa
10
  IF SIG(sq5) GOTO 20
  allarme_m = 30   ;allarme mancanza organo
  CALL master_all
  GOTO 10
20
  ACCURACY 20
  JMOVE #s_organo
  ACCURACY 1
  SPEED 30
  LMOVE #v_organo
  ACCURACY 1
  DECEL 3
  SPEED 2
  LMOVE #organo
  BREAK
  STABLE 0.2
  HERE partenza
  CALL apripinza2
  TIMER (1) = 0
30
  IF SIG(-sq8) GOTO 40
  IF TIMER(1)>5 THEN
    allarme_m = 13 ;allarme eff. presenza falda
    CALL master_all
  END
  GOTO 30
40
  ACCURACY 1
  SPEED 5
  LMOVE #v_organo
  SPEED 20
  ACCURACY 50
  LMOVE #s_organo
  dist_pre = 100
  CALL wait_out
50
  IF SIG(-sq5) GOTO 60
  allarme_m = 31  ;allarme eff. presenza organo
  CALL master_all
  GOTO 50
60
  JMOVE #u_organo
  RETURN
.END
.PROGRAM prova1()
;Prova sincronismi tornio1
  SIGNAL -25,-26,-27,-28,29
10
  TYPE "attesa tornio pronto allo scarico"
  SWAIT -1025,1026,1027,-1028,1029,1030
  TYPE "tornio pronto allo scarico"
  IF passo==1 THEN
    PAUSE
  END
  SIGNAL -29
  TWAIT 0.1
  SIGNAL 25
  TYPE "attesa mdr aperto"
  SWAIT 1025,-1026
  SIGNAL -25
  TYPE "mdr aperto"
  IF passo==1 THEN
    PAUSE
  END
  TWAIT 0.1
  SIGNAL 29,27
  TYPE "attesa consenso fine scarico"
  SWAIT -1027,-1028,1029,-1030
  TYPE "consenso fine scarico"
  SIGNAL -27
  IF passo==1 THEN
    PAUSE
  END
  TYPE "attesa tornio pronto al carico"
  SWAIT 1025,-1026,-1027,1028,1029,1030
  TYPE "tornio pronto al carico"
  IF passo==1 THEN
    PAUSE
  END
  SIGNAL -29
  TWAIT 0.1
  SIGNAL 26
  TYPE "attesa mdr chiuso"
  SWAIT -1025,1026
  SIGNAL -26
  TYPE "mdr chiuso"
  IF passo==1 THEN
    PAUSE
  END
  TWAIT 0.1
  SIGNAL 29,28
  TYPE "attesa consenso fine carico"
  SWAIT -1027,-1028,1029,-1030
  SIGNAL -28
  TYPE "consenso fine carico"
  IF SIG(selcontinuo) GOTO 10
  RETURN
.END
.PROGRAM prova2()
;Prova sincronismi tornio2
  SIGNAL -30,-31,-32,-33,34
10
  TYPE "attesa tornio pronto allo scarico"
  SWAIT -1031,1032,1033,-1034,1035,1036
  TYPE "tornio pronto allo scarico"
  IF passo==1 THEN
    PAUSE
  END
  SIGNAL -34
  TWAIT 0.1
  SIGNAL 30
  TYPE "attesa mdr aperto"
  SWAIT 1031,-1032
  SIGNAL -30
  TYPE "mdr aperto"
  IF passo==1 THEN
    PAUSE
  END
  TWAIT 0.1
  SIGNAL 34,32
  TYPE "attesa consenso fine scarico"
  SWAIT -1033,-1034,1035,-1036
  TYPE "consenso fine scarico"
  SIGNAL -32
  IF passo==1 THEN
    PAUSE
  END
  TYPE "attesa tornio pronto al carico"
  SWAIT 1031,-1032,-1033,1034,1035,1036
  TYPE "tornio pronto al carico"
  IF passo==1 THEN
    PAUSE
  END
  SIGNAL -34
  TWAIT 0.1
  SIGNAL 31
  TYPE "attesa mdr chiuso"
  SWAIT -1031,1032
  SIGNAL -31
  TYPE "mdr chiuso"
  IF passo==1 THEN
    PAUSE
  END
  TWAIT 0.1
  SIGNAL 34,33
  TYPE "attesa consenso fine carico"
  SWAIT -1033,-1034,1035,-1036
  SIGNAL -33
  TYPE "consenso fine carico"
  IF SIG(selcontinuo) GOTO 10
  RETURN
.END
.PROGRAM prova_fasa()
;Programma per provare la fasatura pezzo
  CALL set.up
  JMOVE #passa1
  CALL scarico_pressa
  CALL cerca_fase
  CALL rifasa
  PAUSE
  ACCURACY 1
  SPEED 10
  LMOVE compensa
  BREAK
  CALL apripinza1
  LDEPART 200
  JMOVE #attesa
  RETURN
.END
.PROGRAM prova_int_tor1()
10
  TYPE "ATTESA TORNIO 1 PRONTO ALLO SCARICO"
  CALL attesa_s_tor1
  TYPE "TORNIO 1 PRONTO ALLO SCARICO"
  TYPE "PREMERE START PER APRIRE IL MANDRINO..."
  IF passo==1 THEN
    PAUSE
  END
  CALL apri_mdr_tor1
  TYPE "MANDRINO APERTO"
  TYPE "PREMERE START PER AVVIARE IL CICLO DI PULIZIA..."
  IF passo==1 THEN
    PAUSE
  END
  CALL via_puliz_tor1
  TYPE "PREMERE START PER ATTENDERE TORNIO 1 PRONTO AL CARICO..."
  IF passo==1 THEN
    PAUSE
  END
  TYPE "ATTESA TORNIO 1 PRONTO AL CARICO"
  CALL attesa_c_tor1
  TYPE "TORNIO 1 PRONTO AL CARICO"
  TYPE "PREMERE START PER CHIUDERE IL MANDRINO..."
  IF passo==1 THEN
    PAUSE
  END
  CALL chiudi_mdr_tor1
  TYPE "MANDRINO CHIUSO"
  TYPE "PREMERE START AVVIARE CICLO DI LAVORAZIONE..."
  IF passo==1 THEN
    PAUSE
  END
  CALL via_ciclo_tor1
  TYPE "CICLO TORNIO 1 AVVIATO"
  TYPE "PREMERE START PER RIPETERE UN CICLO..."
  IF SIG(selcontinuo) GOTO 10
  PAUSE
  GOTO 10
.END
.PROGRAM prova_int_tor2()
10
  TYPE "ATTESA TORNIO 2 PRONTO ALLO SCARICO"
  CALL attesa_s_tor2
  TYPE "TORNIO 2 PRONTO ALLO SCARICO"
  TYPE "PREMERE START PER APRIRE IL MANDRINO..."
  IF passo==1 THEN
    PAUSE
  END
  CALL apri_mdr_tor2
  TYPE "MANDRINO APERTO"
  TYPE "PREMERE START PER AVVIARE IL CICLO DI PULIZIA..."
  IF passo==1 THEN
    PAUSE
  END
  CALL via_puliz_tor2
  TYPE "PREMERE START PER ATTENDERE TORNIO 2 PRONTO AL CARICO..."
  IF passo==1 THEN
    PAUSE
  END
  TYPE "ATTESA TORNIO 2 PRONTO AL CARICO"
  CALL attesa_c_tor2
  TYPE "TORNIO 2 PRONTO AL CARICO"
  TYPE "PREMERE START PER CHIUDERE IL MANDRINO..."
  IF passo==1 THEN
    PAUSE
  END
  CALL chiudi_mdr_tor2
  TYPE "MANDRINO CHIUSO"
  TYPE "PREMERE START AVVIARE CICLO DI LAVORAZIONE..."
  IF passo==1 THEN
    PAUSE
  END
  CALL via_ciclo_tor2
  TYPE "CICLO TORNIO 2 AVVIATO"
  TYPE "PREMERE START PER RIPETERE UN CICLO..."
  IF SIG(selcontinuo) GOTO 10
  PAUSE
  GOTO 10
.END
.PROGRAM prova_presa()
;************* PROVA LA PRESA CON LE GRIP ATTUALI ***********************
  CALL set.up
  JMOVE #fuori_pallet1
  trovato = 0
loop:
  IF (trovato==0) GOTO lvedi
  CALL prelievo
  TYPE /C2,"... PREMI CICLO PER FAR DEPOSITARE IL PEZZO SUL PALLET ",/C2
  PAUSE
  JMOVE soprap
  SPEED vel_presa
  ACCURACY 1
  LMOVE presa
  BREAK
  STABLE 0.2
  CALL apripinza1
  ACCURACY 5
  SPEED 30
  LMOVE soprap
  JMOVE #fuori_pallet1
  TYPE ".... METTERE UN PEZZO SOTTO LA VISIONE PER VERIFICARE LA PRESA."
  TYPE ".... oppure EX CGRIP per ricalcolare offgrip.",/C2
  PAUSE
lvedi:
  CALL inquadra_pezzo
  IF trovato==1 GOTO loop
  TYPE "PEZZO NON TROVATO RITENTARE"
  CALL apricamera
  PAUSE
  GOTO lvedi
.END
.PROGRAM prova_staffa()
  CALL prel_staffa
  CALL dep_staffa
  RETURN
.END
.PROGRAM punto_falda()
;Muove il robot alla posizione di inquadratura interfalda
  TOOL punta
  CALL altezza1
  SIGNAL yv4_1 ;anticipa apertura camera
  JMOVE pallet1+TRANS(offset_falda_x,offset_falda_y,(piano1*(int_piano1+int_falda1))+int_euro1,0,0,0)+offset_camera
  BREAK
  TWAIT t_stab
  TOOL pinza1
  RETURN
.END
.PROGRAM punto_pezzo()
;Muove il robot alla posizione di inquadratura pezzo
  TOOL punta
  CALL altezza1
  SIGNAL yv4_1 ;anticipa apertura camera
  ACCURACY 0.1
  JMOVE pallet1+TRANS(riga1*int_riga1,colonna1*int_col1,(piano1*(int_piano1+int_falda1))+int_euro1,0,0,0)+offset_camera
  BREAK
  STABLE 0.1
  TWAIT t_stab
  TOOL pinza1
  RETURN
.END
.PROGRAM reset.out()
;Tornio1
  SIGNAL out5_tornio1    ;robot in fuori ingombro
  SIGNAL -out1_tornio1,-out2_tornio1
  SIGNAL -out3_tornio1,-out4_tornio1
;Tornio2
  SIGNAL out5_tornio2    ;robot in fuori ingombro
  SIGNAL -out1_tornio2,-out2_tornio2
  SIGNAL -out3_tornio2,-out4_tornio2
;Pressa
;  SIGNAL -out1_pressa,-out2_pressa
;  SIGNAL -out3_pressa,-out4_pressa
  RETURN
.END
.PROGRAM restart()
;Verifica della posizione di ripartenza con CYCLE START
  SPEED 10 ALWAYS
  BREAK
10
  HERE #attuale
  DECOMPOSE loc[1] = #last
  DECOMPOSE loc1[1] = #attuale
  giunto = 1
20
  angolo = loc[giunto]-loc1[giunto]
  absangolo = ABS(angolo)
  IF (absangolo>5) THEN
    TYPE "********************************************************************************"
    TYPE "***********           POSIZIONE DI RIPARTENZA NON CORRETTA           ***********"
    TYPE "********************************************************************************"
    TYPE ""
    TYPE "Mettere l\'impianto nella posizione di arresto da allarme #LAST"
    TYPE "....Premere Cycle start!!!"
    PAUSE
    GOTO 10
  END
  giunto = giunto+1
  IF giunto<=6 GOTO 20
  SPEED 100 ALWAYS
  RETURN
.END
.PROGRAM rifasa()
;Recupera angolo rotazione calcolato da cerca_fase.
  CLOSE 1
  JMOVE #passaggio
  CALL check_sq22m
  JMOVE #s_compensa
  CALL pausa
  ACCURACY 5
  LAPPRO compensa,10
  ACCURACY 1
  SPEED 10
  DECEL 5
  LAPPRO compensa,2
  BREAK
  IF $fronte=="positivo" THEN
    cf = -1
  ELSE
    cf = 1
  END
  c_fin = -1*(corr_ang+(cf*offset_rot))
  c_fin = c_fin+k_corr
  IF tdebug>0 THEN
    TYPE "Correzione finale : ",c_fin
  END
  IF c_fin>=c_max OR c_fin<=c_min THEN
    TYPE ""
    TYPE "Correzione angolare fuori limite !!!"
    TYPE "Correzione calcolata : ",c_fin
    TYPE "Limite positivo : ",c_max
    TYPE "Limite negativo : ",c_min
    HALT
  END
  CALL check_sq22p
  CALL apripinza1
  ABS.SPEED ON
  ACCURACY 1
  SPEED 50,50 DEG/S
  LMOVE compensa+TRANS(0,0,4,0,0,c_fin)
  BREAK
  ABS.SPEED OFF
;       ACCURACY 1
;       LDEPART -4
;       BREAK
  CALL chiudipinza1
;       CALL apripinza1
;       CALL chiudipinza1
  TWAIT 0.2
  ACCURACY 5
  LDEPART 20
  SPEED 50,50 DEG/S
  LMOVE #s_compensa
  JMOVE #passaggio
  JMOVE #passaggio1
  RETURN
.END
.PROGRAM rifasa1()
;Recupera angolo rotazione calcolato da cerca_fase.
  CLOSE 1
  CALL check_sq22m
  CALL pausa
  ACCURACY 5
  LAPPRO compensa1,10
  ACCURACY 1
  SPEED 10
  DECEL 5
  LAPPRO compensa1,2
  BREAK
  IF $fronte1=="positivo" THEN
    cf = -1
  ELSE
    cf = 1
  END
;  c_fin1 = -1*(corr_ang1+(cf*offset_rot))
;  c_fin1 = c_fin1+k_corr
;
  c_fin1 = cf*corr_ang1
;
  IF tdebug>0 THEN
    TYPE "Correzione finale : ",c_fin1
  END
  IF c_fin1>=c_max1 OR c_fin1<=c_min1 THEN
    TYPE ""
    TYPE "Correzione angolare fuori limite !!!"
    TYPE "Correzione calcolata : ",c_fin1
    TYPE "Limite positivo : ",c_max1
    TYPE "Limite negativo : ",c_min1
    HALT
  END
  CALL check_sq22p
  CALL apripinza1
  ABS.SPEED ON
  ACCURACY 1
  SPEED 50,50 DEG/S
  LMOVE compensa1+TRANS(0,0,4,0,0,c_fin1)
  BREAK
  ABS.SPEED OFF
;  ACCURACY 1
;  LDEPART -4
;  BREAK
  CALL chiudipinza1
;       CALL apripinza1
;       CALL chiudipinza1
  TWAIT 0.2
  ACCURACY 5
  LDEPART 150
  JMOVE #s_compensa
;
  RETURN
.END
.PROGRAM scarica_staffa()
;Toglie la staffa nel tornio2
  CLOSE 2
  ACCURACY 5
  LMOVE #s_staffa
  CALL apri_mdr_tor2
  ACCURACY 1
  SPEED 20
  DECEL 10
  LMOVE #staffa
  BREAK
  CALL apripinza2
;       CALL apri_mdr_tor2;apre mandrino tornio
  ACCURACY 5
  SPEED 20
  ACCEL 10
  LMOVE #s_staffa
  LMOVE #f_tornio2
  CALL dep_staffa
  RETURN
.END
.PROGRAM scarico()
;Decide dove mettere il pezzo finito.
  CALL chiudicamera
;  IF SIG(-sel_scarico) THEN
;    CALL nastro
;  ELSE
  CALL pallettizza2
;  END
  RETURN
.END
.PROGRAM scarico_pressa()
;Programma di scarico pezzo da pressa
  OPEN 1
  ACCURACY 10
  JMOVE #f_pressa
  CALL attesa_pressa;attesa pressa pronta scarico
  CALL check_sq30p
  ACCURACY 3
  LMOVE #s_pressa
  ACCURACY 1
  LMOVE #pressas
  BREAK
  STABLE 0.1
  HERE partenza
  CALL chiudipinza1
  ACCURACY 3
  LMOVE #s_pressa
  ACCURACY 10
  LMOVE #f_pressa
  dist_pre = fuori_pressa
  CALL wait_out;attesa fuori pressa
  CALL check_sq30m
  $pressa = "vuota"
  IF tdebug>0 THEN
    TYPE ""
    TYPE "PRESSA: ",$pressa
  END
  JMOVE #passaggio1
  JMOVE #passaggio
  RETURN
.END
.PROGRAM scarico_tor1()
;Programma di scarico pezzo finito tornio 1
  OPEN 1
  ACCURACY 50
  JMOVE #f_tornio1
  CALL attesa_s_tor1;attesa tornio pronto scarico
  ACCURACY 20
  LAPPRO #tornio1s,100
  ACCURACY 1
  LMOVE #tornio1s
  BREAK
  HERE partenza
  CALL chiudipinza1
  CALL apri_mdr_tor1;apertura mandrino tornio
  ACCURACY 20
  LDEPART 100
  ACCURACY 50
  LMOVE #f_tornio1
  dist_pre = fuori_tornio
  CALL wait_out;attesa fuori tornio
  CALL via_puliz_tor1;start lavaggio tornio
  $tornio1 = "vuoto"
  IF tdebug>0 THEN
    TYPE ""
    TYPE "MANDRINO TORNIO 1: ",$tornio1
  END
  RETURN
.END
.PROGRAM scarico_tor2()
;Programma di scarico pezzo finito tornio 2
  OPEN 1
  ACCURACY 50
  JMOVE #f_tornio2
  CALL attesa_s_tor2;attesa tornio pronto scarico
  ACCURACY 20
  LAPPRO #tornio2s,100
  ACCURACY 1
  LMOVE #tornio2s
  BREAK
  HERE partenza
  CALL chiudipinza1
  CALL apri_mdr_tor2;apertura mandrino tornio
  ACCURACY 20
  LDEPART 100
  ACCURACY 50
  LMOVE #f_tornio2
  dist_pre = fuori_tornio
  CALL wait_out;attesa fuori tornio
  CALL via_puliz_tor2;start ciclo pulizia
  $tornio2 = "vuoto"
  IF tdebug>0 THEN
    TYPE ""
    TYPE "MANDRINO TORNIO 2: ",$tornio2
  END
  RETURN
.END
.PROGRAM set.up()
;Parametri movimento robot.
  TOOL pinza1
  SPEED 100 ALWAYS
  ACCURACY 500 ALWAYS
  ACCEL 100 ALWAYS
  DECEL 100 ALWAYS
  CP ON
  RETURN
.END
.PROGRAM stampatempi()
;Stampa a video il report del ciclo eseguito.
  IF aggiorna==0 GOTO 10
  $dato0 = " ***      P R O D U Z I O N E       *** "
  $dato1 = " TEMPO CICLO SEC .............."
  $dato2 = "  CICLI TOTALI ................."
  $dato3 = "  PRODUZIONE ORARIA ............"
  $tempo1 = $ENCODE(/F8.2,tcarico)
  $tempo2 = $ENCODE(/F8.0,conta)
  $tempo3 = $ENCODE(/F8.0,3600/(ttot/conta))
  $report1 = $dato0+$dato1+$tempo1+$dato2+$tempo2+$dato3+$tempo3
  IFPWPRINT 2,1,1,5=$report1
  aggiorna = 0
10
  RETURN
.END
.PROGRAM start()
;Controlla la task automatica se in esecuzione.
  TIMER (1) = 0
10
  IF TASK(1001)==1 AND TASK(1002)==1 AND TASK(1003)==1 GOTO 20
  IF TIMER(1)<5 GOTO 10
  allarme_m = 3
  CALL master_all
  GOTO 10
20
  RETURN
.END
.PROGRAM startup()
;Controlla la posizione del robot prima della partenza.
  SPEED 10 ALWAYS
  BREAK
10
  HERE #attuale
  DECOMPOSE loc[1] = #attesa
  DECOMPOSE loc1[1] = #attuale
  giunto = 1
20
  angolo = loc[giunto]-loc1[giunto]
  absangolo = ABS(angolo)
  IF (absangolo>1) THEN
    allarme_m = 1
    CALL master_all
    GOTO 10
  END
  giunto = giunto+1
  IF giunto<=6 GOTO 20
  SPEED 100 ALWAYS
  RETURN
.END
.PROGRAM stazione4()
;Gestione ciclo nastro scarico pezzi qualita'.
  CASE fase4 OF
   VALUE 1:
    on_mot_4 = 1            ;on motore nastro
    t_staz4 = TIMER(0)
    fase4 = 2
   VALUE 2:
    IF SIG(sq13) GOTO 23          ;nastro troppo pieno?
    IF SIG(-sq12,-sq13) GOTO 21   ;zona scarico libera?
    IF ((TIMER(0)-t_staz4)<5) GOTO 22
    on_mot_4 = 0            ;off motore nastro
    r_fase4 = 1             ;salva fase di rientro
    fase4 = 99
    allarme_4 = 40          ;allarme zona scarico non libera 
    GOTO 22
23
    on_mot_4 = 0            ;off motore nastro
    r_fase4 = 1             ;salva fase di rientro
    fase4 = 99
    allarme_4 = 42          ;allarme nastro scarico pieno 
    GOTO 22
21
    fase4 = 30
22
   VALUE 30:
    IF SIG(sb1) GOTO 301    ;ciclo nastro continuo ?
    fase4 = 31
    GOTO 302
301
    fase4 = 3
302
    t_staz4 = TIMER(0)
303
   VALUE 31:
    IF ((TIMER(0)-t_staz4)<t_passo) GOTO 311
    on_mot_4 = 0            ;off motore nastro
    fase4 = 5
    via_robot4 = 1
    t_staz4 = TIMER(0)
311
   VALUE 3:
    on_mot_4 = 1
    IF SIG(sq13) GOTO 31         ;nastro troppo pieno?
    IF ((TIMER(0)-t_staz4)<25) GOTO 32
    on_mot_4 = 0            ;off motore nastro
    r_fase4 = fase4         ;salva fase di rientro
    fase4 = 99
    allarme_4 = 43          ;allarme nastro scarico non pieno 
    GOTO 32
31
    on_mot_4 = 0            ;off motore nastro
    fase4 = 5
    via_robot4 = 1
    t_staz4 = TIMER(0)
32
   VALUE 4:
    IF SIG(-sq13) GOTO 41        ;nastro troppo pieno?
    IF ((TIMER(0)-t_staz4)<60) GOTO 42
    r_fase4 = fase4         ;salva fase di rientro
    fase4 = 99
    allarme_4 = 44          ;allarme pezzo non tolto 
    GOTO 42
41
    fase4 = 5
    via_robot4 = 1
42
   VALUE 5:
    IF via_staz4==1 THEN
      via_staz4 = 0
      via_robot4 = 0
      stop_4 = 0
      fase4 = 6
    END
   VALUE 6:
    IF SIG(sq12) GOTO 61
    r_fase4 = fase4         ;salva fase di rientro
    fase4 = 99
    allarme_4 = 41          ;allarme zona scarico non occupata 
    GOTO 62
61
    fase4 = 1
62
   VALUE 99:
    IF vis.occupato==1 GOTO 990     ;visualizzatore occupato?
    vis.occupato = 1                ;occupa visualizzatore
    allarme = allarme_4             ;carica codice allarme
    fase4 = 100
990
   VALUE 100:
    IF allarme>0 GOTO 1000          ;attesa tacitazione allarme
    vis.occupato = 0                ;libera visualizzatore
    fase4 = r_fase4                 ;carica fase di rientro
    allarme_4 = 0                   ;pulisce coda di allarme
    t_staz4 = TIMER(0)
1000
  END
  RETURN
.END
.PROGRAM suona()
;Gestione sirena chiamata operatore.
  CASE fsuona OF
   VALUE 1:
    IF suona==1 THEN
      SIGNAL sirena
      t_suona = TIMER(0)
      fsuona = 2
    END
   VALUE 2:
    IF ((TIMER(0)-t_suona)>t_intervallo) OR suona==0 THEN
      SIGNAL -sirena
      suona = 0
      fsuona = 1
    END
  END
  RETURN
.END
.PROGRAM svuota_pinze()
;Svuota le pinze ad inizio ciclo.
  SPEED 50 ALWAYS
  JMOVE #attesa
  JMOVE #s_vuota
  JMOVE #vuota
  BREAK
  OPENI 1
  OPENI 2
  TWAIT 1
  JMOVE #s_vuota
  JMOVE #attesa
  BREAK
  SPEED 100 ALWAYS
  RETURN
.END
.PROGRAM tacitato()
;Visualizzazione tacitazione allarme.
  abilita = 1
  $mess0 = "                                        "
  $mess1 = "     N E S S U N   A L L A R M E        "
  $mess2 = "           P R E S E N T E              "
  $mess3 = "                                        "
  $messaggio = $mess0+$mess1+$mess2+$mess3
  IFPWPRINT 1,1,1,4=$messaggio
  TYPE ""
  TYPE $mess0
  TYPE $mess1
  TYPE $mess2
  TYPE $mess3
  RETURN
.END
.PROGRAM tastatore()
;Verifica planarita' pezzo.
  JMOVE #fff_palpa
  JMOVE #ff_palpa
  CALL attesa_c_tor2;attesa pronto carico
  ACCURACY 1
  LMOVE #f_palpa
  BREAK
riprova:
  tocca = 1
;       FOR tocca = 1 TO 2
  ACCURACY 10
  LMOVE palpa[tocca]+TRANS(-entrata[tocca],0,0,0,0,0)
  SPEED 40
  ACCURACY 1
  LMOVE palpa[tocca]+TRANS(-15,0,0,0,0,0)
  BREAK
ritest:
  IF SIG(sq24) THEN
    allarme_m = 300 ;allarme tastatore
    CALL master_all
    GOTO ritest
  END
  ABS.SPEED ON
  SPEED 3 MM/S
  ACCURACY 0.1
  XMOVE palpa[tocca] TILL sq24
  BREAK
  STABLE 0.1
  ABS.SPEED OFF
  HERE pos[tocca]
  IF f_calibra_1>0 THEN
    POINT rpos[tocca] = pos[tocca]
    f_calibra_1 = f_calibra_1-1
  END
  POINT misura = pos[tocca]-rpos[tocca]
  DECOMPOSE buf[0] = misura
  IF tdebug>0 THEN
    TYPE "TEST ",tocca," = ",/F9.3,buf[2]
  END
  ACCURACY 10
  SPEED 40
  LMOVE palpa[tocca]+TRANS(-uscita[tocca],0,0,0,0,0)
  IF ABS(buf[2])>soglia THEN
    BREAK
    TYPE ""
    TYPE "***********************************"
    TYPE "SOGLIA IMPOSTATA : +/- ",soglia
    TYPE "VALORE RILEVATO  :     ",buf[2]
    TYPE "***********************************"
    allarme_m = 301 ;allarme pezzo storto
    CALL master_all
    GOTO riprova
  END
;       END
  ACCURACY 50
  LMOVE #f_palpa
  LMOVE #ff_palpa
  JMOVE #fff_palpa
  RETURN
.END
.PROGRAM tastatore1()
;Verifica inserimento staffa.
  CLOSE 2
  JMOVE #ff_tasta_camma
  ACCURACY 1
  LMOVE #f_tasta_camma
  BREAK
riprova:
  tocca = 4
ritest:
  IF SIG(sq24) THEN
    allarme_m = 300 ;allarme tastatore
    CALL master_all
    GOTO ritest
  END
  ABS.SPEED ON
  SPEED 45 MM/S
  ACCURACY 0.1
  XMOVE tasta_camma+TRANS(-15,0,0,0,0,0) TILL sq24
  BREAK
  ABS.SPEED OFF
  IF SIG(-sq24) THEN
    ABS.SPEED ON
    SPEED 3 MM/S
    ACCURACY 0.1
    XMOVE tasta_camma TILL sq24
    BREAK
    STABLE 0.1
    ABS.SPEED OFF
  END
  HERE pos[tocca]
  IF f_calibra_2>0 THEN
    POINT rpos[tocca] = pos[tocca]
    f_calibra_2 = f_calibra_2-1
  END
  POINT misura = pos[tocca]-rpos[tocca]
  DECOMPOSE buf[0] = misura
  IF tdebug>0 THEN
    TYPE "TEST ",tocca," = ",/F9.3,buf[1]
  END
  ACCURACY 10
  SPEED 40
  LMOVE #f_tasta_camma
  IF ABS(buf[1])>soglia1 THEN
    BREAK
    TYPE ""
    TYPE "***********************************"
    TYPE "SOGLIA IMPOSTATA : +/- ",soglia1
    TYPE "VALORE RILEVATO  :     ",buf[1]
    TYPE "***********************************"
    allarme_m = 301 ;allarme pezzo storto
    CALL master_all
    GOTO riprova
  END
  LMOVE #ff_tasta_camma
  JMOVE #fff_tasta_camma
  ACCURACY 50
  JMOVE #f_tornio2
  RETURN
.END
.PROGRAM test_boccola()
;Esegue il controllo della presenza della boccola in pinza2
  ACCURACY 50
  JMOVE #f_t_boccola
  ACCURACY 5
  LMOVE #s_t_boccola
  ACCURACY 3
  SPEED 10
  LMOVE #i_t_boccola
  CALL check_sq23m
  ACCURACY 1
  SPEED 10
  LMOVE #t_boccola
  BREAK
  IF SIG(sq23) THEN
    t_boccola = 1
  ELSE
    t_boccola = 0
  END
  ACCURACY 3
  LMOVE #s_t_boccola
  ACCURACY 50
  LMOVE #f_t_boccola
  RETURN
.END
.PROGRAM tronca()
;Normalizza le stringhe a 40 caratteri -> $tronca.
  lung_chr = 0
  DO
    lung_chr = LEN($tronca)
    IF lung_chr>38 THEN
      $tronca = $LEFT($tronca,38)
    ELSE
      IF lung_chr<38 THEN
        $tronca = $tronca+$spazio
      END
    END
  UNTIL lung_chr==38
  $tronca = $spazio+$tronca+$spazio
  RETURN
.END
.PROGRAM txrx()
;Protocollo seriale RS232 per versione Provis115.
;Aggiornato al 03.07.2002.
  err = -1
  ctxrx = 0
  WHILE ctxrx<3 AND err<>0 DO
    IF tdebug>0 THEN
      TYPE "TENTATIVI RICE-TRASMISSIONE : ",ctxrx
    END
    SEND $tx,err
    IF err==0 THEN
      RECEIVE $rx,err,10
      IF err<>0 THEN
        TYPE "ERRORE IN RX --> ",err," !!! "
        flag = -1
      ELSE
        IF "err"==$LEFT($rx,3) THEN
          TYPE "ERRORE COMANDO SCONOSCIUTO !!! "
          flag = -1
        ELSE
          $temp = $DECODE($rx,",",0)
          flag = VAL($temp)
          $temp = $DECODE($rx,",",1)
          $temp = $DECODE($rx,",",0)
          tipo = VAL($temp)
          $temp = $DECODE($rx,",",1)
          $temp = $DECODE($rx,",",0)
          x = VAL($temp)
          $temp = $DECODE($rx,",",1)
          $temp = $DECODE($rx,",",0)
          y = VAL($temp)
          $temp = $DECODE($rx,",",1)
          $temp = $DECODE($rx,",",0)
          a = VAL($temp)
          $temp = $DECODE($rx,",",1)
          IF (x<>0 AND y<>0) THEN
            $temp = $DECODE($rx,",",0)
            affida = VAL($temp)
            $temp = $DECODE($rx,",",1)
          END
          $temp = $DECODE($rx,",",0)
          npezzi = VAL($temp)
          a = -1*(180-a)
          IF a>180 THEN
            a = a-360
          END
          IF a<-180 THEN
            a = a+360
          END
          IF a<0 THEN
            ac = a+180
          ELSE
            ac = a-180
          END
        END
      END
    ELSE
      TYPE "ERRORE IN TX --> ",err," !!! "
      flag = -1
    END
    ctxrx = ctxrx+1
    IF ctxrx>=3 THEN
      TYPE "....SERIALE IN ERRORE...."
    END
  END
  RETURN
.END
.PROGRAM vedi()
lcerca1:
  TIMER (9) = 0
  TIMER (8) = 0
  IF rflag==0 THEN
    $tx = "M0"
  END
  IF rflag==1 THEN
    $tx = "M1"
  END
  IF rflag==2 THEN
    $tx = "M2"
  END
  CALL cerca;esegue il riconoscimento per interfalda
  TYPE /C1,"TEMPO DEL CERCA      ",/F6.3,TIMER(8)
  IF (flag>=0) GOTO lvedi1
  TYPE ">>> MODELLO NON RICONOSCIUTO !!! <<<"
  TYPE "....mettere a punto la visione e continuare...."
  PAUSE
  GOTO lcerca1
lvedi1:
  CALL chiudicamera
  IF flag==0 THEN
    a = 0
    POINT presa = pallet1+TRANS(riga1*int_riga1,colonna1*int_col1,(piano1*(int_piano1+int_falda1))+int_euro1,0,0,0)+offset_camera+fcam+TRANS(x,y,0,0,0,a)
  ELSE
    POINT presa = pallet1+TRANS(offset_falda_x,offset_falda_y,(piano1*(int_piano1+int_falda1))+int_euro1,0,0,0)+offset_camera+fcam+TRANS(x,y,0,0,0,a)
  END
  TYPE "TEMPO RICONOSCIMENTO ",/F6.3,TIMER(9)
  TYPE /F10.2,"X=",x," Y=",y," A=",a," MODELLO ",flag," TIPO ",tipo
  TYPE "NUMERO PEZZI RICONOSCIUTI=",npezzi
  RETURN
.END
.PROGRAM via_ciclo_tor1()
;Via ciclo lavorazione TORNIO1.
  IF setta_tornio==1 THEN
    TYPE "Premere Cycle Start per avviare il tornio..."
    PAUSE
  END
  SIGNAL out5_tornio1  ;robot fuori ingombro
10
  IF SIG(-in5_tornio1) THEN
    allarme_m = 104 ;tornio1 non automatico
    CALL master_all
    GOTO 10
  END
  SIGNAL out4_tornio1 ;via al tornio
  TIMER (1) = 0
20
  IF SIG(-in3_tornio1,-in4_tornio1) GOTO 30
  IF TIMER(1)<5 GOTO 20
  allarme_m = 104 ;ciclo tornio1 non avviato
  CALL master_all
  GOTO 20
30
  SIGNAL -out4_tornio1
  chiuso_tor1 = 1
  RETURN
.END
.PROGRAM via_ciclo_tor2()
;Via ciclo lavorazione TORNIO2.
  IF setta_tornio==1 THEN
    TYPE "Premere Cycle Start per avviare il tornio..."
    PAUSE
  END
  SIGNAL out5_tornio2  ;robot fuori ingombro
10
  IF SIG(-in5_tornio2) THEN
    allarme_m = 124 ;tornio2 non automatico
    CALL master_all
    GOTO 10
  END
  SIGNAL out4_tornio2 ;via al tornio
  TIMER (1) = 0
20
  IF SIG(-in3_tornio2,-in4_tornio2) GOTO 30
  IF TIMER(1)<5 GOTO 20
  allarme_m = 124 ;ciclo tornio2 non avviato
  CALL master_all
  GOTO 20
30
  SIGNAL -out4_tornio2
  chiuso_tor2 = 1
  RETURN
.END
.PROGRAM via_pressa()
;Via ciclo lavorazione PRESSA
  SIGNAL out2_pressa  ;robot fuori ingombro
10
  IF SIG(-in1_pressa) THEN
    allarme_m = 140 ;pressa non pronta al carico
    CALL master_all
    GOTO 10
  END
  SIGNAL out1_pressa  ;via alla pressa
  TIMER (1) = 0
20
  IF SIG(in2_pressa) GOTO 30
  IF TIMER(1)<5 GOTO 20
  allarme_m = 141 ;ciclo pressa non avviato
  CALL master_all
  GOTO 20
30
  SIGNAL -out1_pressa
  SIGNAL -out2_pressa
  RETURN
.END
.PROGRAM via_puliz_tor1()
;Via ciclo pulizia TORNIO1.
  SIGNAL out5_tornio1  ;robot fuori ingombro
10
  IF SIG(-in5_tornio1) THEN
    allarme_m = 104 ;tornio1 non automatico
    CALL master_all
    GOTO 10
  END
  SIGNAL out3_tornio1 ;via al tornio
  TIMER (1) = 0
20
  IF SIG(-in3_tornio1,-in4_tornio1) GOTO 30
  IF TIMER(1)<5 GOTO 20
  allarme_m = 104 ;ciclo tornio1 non avviato
  CALL master_all
  GOTO 20
30
  SIGNAL -out3_tornio1
  chiuso_tor1 = 1
  RETURN
.END
.PROGRAM via_puliz_tor2()
;Via ciclo pulizia TORNIO2.
  SIGNAL out5_tornio2  ;robot fuori ingombro
10
  IF SIG(-in5_tornio2) THEN
    allarme_m = 124 ;tornio2 non automatico
    CALL master_all
    GOTO 10
  END
  SIGNAL out3_tornio2 ;via al tornio
  TIMER (1) = 0
20
  IF SIG(-in3_tornio2,-in4_tornio2) GOTO 30
  IF TIMER(1)<5 GOTO 20
  allarme_m = 124 ;ciclo tornio2 non avviato
  CALL master_all
  GOTO 20
30
  SIGNAL -out3_tornio2
  chiuso_tor2 = 1
  RETURN
.END
.PROGRAM wait_out()
;Controlla la distanza del robot da ingombro.
10
  TIMER (1) = 0
20
  d_pre = DISTANCE(HERE,partenza)
  IF TIMER(1)>=15 THEN
    allarme_m = 4
    CALL master_all
    GOTO 10
  END
  IF d_pre<dist_pre GOTO 20
  RETURN
.END
.PROGRAM win1()
  IF abilita==0 THEN
    TIMER (10) = 0
  ELSE
    SOUT 2035
    IF TIMER(10)<1 GOTO 10
    SOUT -2035
    abilita = 0
10
  END
  RETURN
.END
.TRANS
a0 -1375.326904 541.648804 130.734497 158.524979 179.998444 -90.155319
a01 -1078.557373 223.317413 141.383423 -4.783838 179.999008 84.937225
a02 -1163.472168 -950.742432 278.857788 -21.287449 179.995773 66.811287
angolo -0.492050 -0.100098 0.019775 167.196579 0.026214 -175.299622
angolo1 -0.003632 0.014404 0.033325 158.849182 0.007016 -173.772308
angolo_c -0.121078 -0.149292 -0.014526 29.422520 0.023985 -68.536217
ax -1075.479980 543.719238 131.516266 142.966125 179.998611 -105.714584
ax1 -1082.474365 996.770935 140.968811 150.665543 179.993927 -119.614342
ax2 -1148.139893 -335.132507 276.123871 -31.852697 179.996109 56.246460
ay -1369.957764 245.043030 130.514160 165.929276 179.998688 -82.750793
ay1 -2266.708252 219.459610 140.887421 136.944885 179.995026 -133.335541
ay2 -2128.549561 -940.419128 279.174316 16.140640 179.988266 104.239204
b0 -1552.658203 736.810120 138.244995 -13.735850 179.999359 75.984131
bx -1273.878174 735.705933 139.907593 -18.133194 179.999069 71.586807
by -1552.513184 458.796783 140.337128 -18.454901 179.999069 71.265160
cambio_griffe 1.677718 1248.614014 1769.276367 89.667732 89.987228 179.998199
carico_tornio2 621.047241 1931.093750 1045.362915 -49.341171 179.521179 131.004150
centro 621.483826 1930.642944 972.419189 -48.949032 179.521317 131.396317
compensa -626.213440 1228.550049 1174.622559 37.732750 178.261917 -173.378845
compensa1 -626.213440 1228.550049 1174.622559 37.732750 178.261917 -173.378845
compensa2 -626.213440 1228.550049 1174.622559 37.732750 178.261917 -173.378845
compenza 0.000000 0.000000 0.000000 0.000000 0.000000 0.000000
controllo 2109.091553 -376.000671 840.256287 -123.155609 178.660446 -32.690018
d_ripresa1 -638.234253 1028.961426 533.300842 30.864044 90.337944 -169.600815
d_ripresa1_b -435.625854 1143.951782 581.906555 -151.195831 89.760742 29.489630
d_ripresa1_f -433.015564 1145.677979 581.785400 -152.030075 89.123398 34.429157
d_ripresa2 1049.790649 -1433.227661 523.299988 57.363419 89.307541 30.193151
dep_falda -1635.982788 -796.703491 732.643921 -89.235008 89.930756 90.408302
dep_mag -645.996338 -1576.027832 759.080566 -41.949490 90.857613 30.493185
deposito -2094.125000 -932.810852 288.798096 -17.948147 179.998566 70.151962
disco -593.262146 1238.747681 1110.740723 101.320068 179.465195 -109.997818
fabio -182.062347 2150.476074 1273.486572 -0.000921 90.000290 90.217850
fasa 1258.231445 -1453.895996 1314.467285 154.997559 179.414429 118.784698
fasa1 -629.788574 1225.728271 1316.818970 37.662743 178.263474 -173.447372
fasa_old 1244.089111 -1487.545044 1315.041992 147.996735 179.065918 89.995827
fcam -317.315979 -851.912720 -83.068481 -89.789673 90.584999 -0.215910
ffabio -266.416779 1611.035767 1411.355835 -0.000804 90.000389 90.217300
fibra[1] -1418.332642 1455.384644 1145.707886 103.226715 54.874371 88.498428
fibra[2] -1425.272217 1446.510010 1167.415161 103.240921 54.867561 88.473877
fibra[3] -1423.079224 1425.524658 1195.203491 103.226128 54.875626 88.496513
fibra[4] -1432.088745 1420.639893 1209.391357 103.227982 54.872112 88.496597
fvis -1552.658203 736.810120 138.244995 -51.731148 179.451065 128.497116
gh 285.695770 2395.732178 1357.810181 -59.268394 179.771011 120.725288
gj 285.899200 2395.452881 1243.424805 -59.194859 179.771103 120.798454
grip[0] 3.975708 3.103790 -118.904877 -60.275116 0.261149 -30.241014
grip[1] -491.519775 -185.587555 -365.926666 90.025436 89.603058 -90.293732
grip1[0] 3.975708 3.103790 -118.904877 -60.275116 0.261149 -30.241014
grip1[1] -491.519775 -185.587555 -365.926666 90.025436 89.603058 -90.293732
grip2[0] 3.975708 3.103790 -118.904877 -60.275116 0.261149 -30.241014
grip2[1] -491.519775 -185.587555 -365.926666 90.025436 89.603058 -90.293732
grip3[0] 3.975708 3.103790 -118.904877 -60.275116 0.261149 -30.241014
grip3[1] -491.519775 -185.587555 -365.926666 90.025436 89.603058 -90.293732
grip4[0] 3.975708 3.103790 -118.904877 -60.275116 0.261149 -30.241014
grip4[1] -491.519775 -185.587555 -365.926666 90.025436 89.603058 -90.293732
grip_pal1 0.090057 0.069946 0.000015 36.756287 179.961792 -143.232483
grip_pal2 0.439758 0.561157 0.001740 51.821377 179.719589 -130.301346
linea 0.001343 210.717346 -198.815918 -89.999649 45.000038 90.000244
m0 2300.747559 -177.461517 715.675659 0.392609 91.497330 -179.994644
magazzino 2300.747559 -177.461517 715.675659 -0.466220 90.624420 179.997925
misura -0.016205 -0.000732 0.287231 -19.350956 0.000216 19.350620
mx 2303.729492 -177.475876 989.298523 0.392407 91.496132 -179.994446
my 2297.498047 -577.092651 715.894897 0.393617 91.499268 -179.994400
offgrip 3.975708 3.103790 -118.904877 -60.275116 0.261149 -30.241014
offset_camera -77.592300 314.647827 850.657593 89.262756 89.734276 90.187805
offset_inter2 155.756104 -486.614258 -356.181976 -178.865097 90.068787 -89.593079
p1 1253.554443 -1462.568481 1267.653198 144.090210 179.997147 72.115891
p_ripresa1 -467.712860 1128.036621 531.790833 -150.210922 89.608994 29.904860
p_ripresa2 1141.193848 -1288.600098 524.282104 -122.923241 91.092926 150.153015
pallet1 -1058.319580 188.341721 -3.589456 128.135635 0.038894 -37.845463
pallet2 -1142.931641 -937.453918 124.479645 141.701401 0.281772 -51.924232
pallet20 -1098.210571 -931.403198 99.874405 -116.799980 0.475795 -151.303696
pallpa[1] -450.425110 2335.134766 1391.281250 159.624527 90.075562 0.200356
palpa[1] -450.700623 2329.490723 1408.898071 159.624313 90.076614 0.201186
palpa[2] -487.928467 2329.095703 1389.054810 159.624130 90.071594 0.194179
palpa[3] -449.566254 2329.092773 1388.458496 159.624222 90.071655 0.194577
partenza -293.460754 2407.213867 1226.523926 -166.435532 179.104553 13.557078
partenza_mem -293.460754 2407.213867 1226.523926 -166.435532 179.104553 13.557078
partenzap 2428.928955 -1506.459473 1138.498657 -30.413893 90.343254 -179.712906
pinza1 0.000000 -75.699997 219.199997 90.000214 45.000000 -90.000214
pinza1o 0.000000 -75.699997 219.199997 90.000206 45.000000 -90.000206
pinza2 0.000000 -348.700012 306.200012 -90.000214 45.000000 0.000000
pippo -1252.540527 -837.027954 123.750519 141.701401 0.281772 -51.924232
pluto -0.013012 0.004395 4.024780 105.605507 0.000757 -105.606438
pos[1] -450.703278 2329.507324 1413.686035 159.624268 90.076492 0.201239
pos[2] -487.921387 2329.114746 1392.592163 159.623901 90.071465 0.194724
pos[3] -449.560669 2329.100830 1392.224365 159.624451 90.070938 0.195594
pos[4] -182.043335 2149.841309 1273.471924 -0.002858 89.999939 90.218330
posizione 415.661133 1499.968750 1361.087646 -59.937614 179.769089 120.055084
preciso 943.869080 1526.367676 1120.568604 -133.139297 178.711823 47.176300
presa -1408.488159 349.497528 539.331726 -23.530563 179.486771 66.180359
presa1 -1154.415283 305.189697 248.501144 -51.728725 179.450790 36.099483
presa2 -2102.999756 305.329468 248.789398 -51.728725 179.450790 130.829498
presa_pallet 2302.103760 -377.291504 989.305969 -0.466220 90.624420 179.997925
presa_tornio2 -582.062500 1236.225830 1200.250610 165.122208 179.010071 -46.013084
punta -0.002658 -8.415390 289.583771 89.999962 45.000038 -90.000389
punta_o 0.000000 -36.700001 261.299988 90.000206 45.000000 -90.000206
punta_old 0.000000 -36.700001 261.299988 90.000221 45.000000 -90.000221
ra[0] -1163.399780 298.342712 540.130249 -23.530569 179.486771 66.180351
ra[1] -1638.323730 454.552246 651.524963 -89.572334 89.963440 90.043434
ra[2] -1610.974243 461.816986 809.470581 -88.614670 90.594597 90.008148
rpos[1] -450.705841 2329.506836 1413.394043 159.624603 90.076714 0.201235
rpos[2] -487.923920 2329.112793 1392.106689 159.624252 90.070938 0.194708
rpos[3] -449.564087 2329.107910 1392.122192 159.624725 90.072311 0.194926
rpos[4] -182.073425 2149.842529 1273.457153 -0.001658 89.999992 90.218254
s_controllo 2059.097168 -375.588196 840.887573 -123.155609 178.660446 -32.690018
s_preciso 946.175659 1528.830811 1270.018555 -133.097076 178.711258 47.217773
s_presa_pallet 2303.480713 -377.298126 1115.659180 -0.466220 90.624420 179.997925
sfabio -182.071930 2007.650757 1273.466309 -0.000688 90.000381 90.217644
soprap -1408.488159 349.497528 839.331726 -23.530563 179.486771 66.180359
staffa[3] -818.008179 1532.787964 1259.087646 119.613579 53.989605 90.386726
tasta_boccola -151.144287 0.274277 -191.380615 178.874710 90.099152 90.004539
tasta_camma -182.062347 2150.476074 1273.486572 -0.000921 90.000290 90.217850
tasta_mag -428.054016 -2045.402222 561.416199 45.616676 150.250214 88.768005
temp 1257.777100 -1454.094604 1314.481079 155.800858 179.410919 119.569977
temp1 -629.778870 1225.727295 1316.826538 37.625740 178.263840 -173.483719
zero_magazzino -404.237091 -2019.883911 581.353271 45.613491 150.250504 88.766251
.END
.JOINTS
#a -138.259872 -33.249557 -18.067593 44.109222 -69.055077 37.435730
#a01 -72.328629 45.442333 -104.714622 -194.707474 -59.132137 174.767334
#anti_col -10.402065 23.283533 -31.001656 158.717880 -55.799431 -94.094627
#attesa -0.367567 -48.537369 23.139605 -0.520414 -68.146362 0.674542
#attuale -0.367306 -48.538403 23.140387 -0.520090 -68.146675 0.674530
#ax1 -35.433350 43.389824 -81.958214 -242.243286 -40.795166 190.078506
#ay1 -83.339226 56.474148 -57.283997 -202.279846 -13.021104 196.838379
#b -58.346756 -4.310004 -4.049112 -0.426824 -36.833691 -107.804169
#boccola 122.357323 27.420340 -24.257391 4.142263 -20.570463 -5.175364
#cambio_griffe -0.374861 -55.386375 10.942481 -0.583563 -55.958561 0.810100
#centro 21.572870 17.537291 -35.313007 51.262245 -18.752594 -65.385429
#d_ripresa1 -46.111023 32.094315 -89.706078 -117.383659 -89.859756 89.733742
#d_ripresa2 155.418289 34.797729 -58.375450 85.979729 -70.897507 -33.979088
#dep_mag -152.366516 14.014686 -54.559906 127.644913 -41.939163 -38.386528
#dep_mag_spi -153.558044 14.513853 -56.239456 130.533905 -42.474194 -40.963970
#deposito 17.640087 -24.137625 -18.042828 27.153503 -31.609344 -37.863022
#disco -23.518814 -24.202765 -26.159559 16.590425 -19.340269 -21.313457
#estaffa[1] -31.403137 -6.837894 -26.516687 -120.431656 -27.211884 -53.157433
#estaffa[2] -35.954090 -6.003967 -26.030262 -116.975937 -30.663557 -54.674522
#estaffa[3] -41.627541 -2.804229 -25.515945 -110.691132 -34.881165 -59.442947
#estaffa[4] -47.538307 3.050840 -25.614742 -105.409920 -41.087654 -63.105606
#f_d_ripresa1 -55.050377 30.776215 -86.211693 -108.406754 -88.796059 86.399094
#f_d_ripresa2 162.278854 39.005367 -53.695759 79.025223 -75.309258 -29.660357
#f_fasa 146.377609 -4.330425 -20.877441 33.637791 -31.123703 -47.053482
#f_fasa1 -26.203014 -29.404600 -14.258306 4.447646 -30.637852 -8.508623
#f_mag_staffe -21.292051 -41.570480 -7.131368 -196.461975 -24.869844 18.213280
#f_magazzino -163.123901 -21.493925 -37.347340 123.860825 -25.254860 -22.870977
#f_magazzino2 -163.123901 -21.493664 -37.347080 123.861137 -25.254856 -22.872044
#f_nastro 51.778748 -38.086647 -14.506210 -39.010006 -44.687946 59.520489
#f_p_ripresa1 0.196418 18.303492 -72.836304 94.821907 -68.168343 -48.912319
#f_pallet2 -119.008850 -23.362635 -6.542236 -28.342651 -45.889267 41.837494
#f_palpa -9.953223 23.622946 -22.930281 145.878510 -26.840422 -58.806435
#f_preciso 26.667229 -4.427105 -32.418697 49.166340 -24.888201 -65.438927
#f_pressa 122.714211 14.128168 -32.726040 5.883885 -12.829442 -7.687662
#f_staffa 26.610178 -33.684620 -18.721373 35.433170 -35.931423 -50.374020
#f_t_boccola 107.347786 4.534900 -32.372036 -32.902023 -16.457129 41.697166
#f_tasta_camma -12.499621 26.863270 -12.351707 -43.703773 -89.752968 -72.342445
#f_tornio1 20.155485 -17.517128 -16.255880 25.969538 -33.475166 -36.790455
#f_tornio2 -18.244188 -29.325499 -12.165321 -21.871317 -36.554848 30.952566
#fabio -66.510338 24.820845 -35.569252 40.350063 -79.624451 48.837898
#ff_fasa 134.440918 -9.434547 -20.324543 19.098257 -26.582655 -25.716215
#ff_fasa1 -26.203014 -29.427868 -14.185837 4.438579 -30.709122 -8.498733
#ff_mag_staffe 3.799928 -55.451004 5.246666 6.582088 -50.663525 -9.289912
#ff_palpa -20.673882 -12.834365 -25.320698 170.764145 -25.422012 -81.571533
#ff_tasta_camma -18.194954 6.831432 -11.134861 -42.424271 -94.450012 -69.093544
#fff_palpa -11.315382 -19.191706 -19.269318 57.787663 -24.275093 -13.805268
#fff_tasta_camma -20.572287 -2.950025 -11.418479 -41.787277 -95.800949 -67.164444
#fs_tornio2 -17.638784 -28.412470 -12.529227 -21.434778 -35.999027 30.251572
#fuori_cerca 98.083839 13.624606 -24.173975 -49.074043 -68.150658 -77.117081
#fuori_fibra -59.093876 0.026367 -18.864746 -94.403183 -46.222908 -74.510796
#fuori_magazzino 105.505257 1.786507 -31.518837 35.488224 -18.751411 -43.975559
#fuori_organo -159.539169 -18.196989 7.241114 -8.622941 -75.460930 23.538839
#fuori_pallet1 -75.979553 -24.979565 -13.235144 -88.571754 -96.031235 13.306789
#i_t_boccola 108.935013 22.325008 -36.160469 -41.413239 -13.427268 50.468964
#inf_boccola 121.944954 26.906696 -23.380732 1.875045 -22.231632 -1.740944
#inter -101.041573 -19.417635 -10.182083 45.300312 -75.708778 89.970703
#inter1 -67.400215 13.361710 -13.505988 40.155758 -96.028214 63.675171
#inter2 -106.155205 4.129828 -4.213078 44.673031 -76.731384 97.421310
#last 20.155224 -17.517387 -16.255358 25.969538 -33.475166 -36.789925
#nastro 64.761337 -0.935777 -40.065430 -66.880173 -19.530399 84.473007
#organo -149.587234 35.170490 2.046844 2.258150 -102.036720 10.859860
#out_tasta -179.136124 15.166053 -36.845795 -13.227974 -43.059032 51.412571
#p1 146.216095 7.030221 -22.685242 44.938908 -38.237286 -67.174202
#p_ripresa1 -7.262508 16.341204 -74.022903 102.599617 -66.246544 -50.532043
#p_staffa[3] -40.488373 1.514305 -29.249376 -116.649513 -35.455002 -52.798489
#pass_inter -108.650543 -18.536142 -1.817968 43.875732 -98.815704 78.434883
#pass_inter_rit -157.859955 -24.669621 5.148390 6.203517 -104.868942 26.250387
#passa1 72.318726 -50.716022 -2.080210 69.945656 -80.389870 24.162226
#passa2 41.540794 -50.716537 -2.079950 69.945007 -80.390190 24.162764
#passa_t -2.780067 -36.497120 -8.819258 -3.425925 -36.467857 4.642072
#passaggio 43.658669 -57.721424 4.310572 -0.038213 -49.468609 -0.191803
#passaggio1 90.000000 -57.721424 4.310572 -0.038213 -49.468609 -0.191803
#pippo -67.238701 24.944925 -35.639637 40.701107 -79.185249 49.247669
#pluto -34.200138 1.084933 -21.957170 0.901900 -77.336456 4.023871
#preciso 39.171539 4.293201 -29.879175 51.266129 -34.758099 -75.294548
#prel_mag -154.824097 17.162207 -56.928165 132.881439 -42.549068 -42.387585
#prel_mag_sop -154.047287 14.981484 -55.219162 130.507355 -41.735882 -40.055069
#prelievo -45.292530 -1.744630 -22.538483 -46.716473 -41.630398 71.981064
#pressac 121.962410 42.499020 -21.790075 1.102034 -23.488743 -2.760754
#pressas 121.970749 42.505222 -21.750973 1.120170 -23.528503 -2.781474
#prova -131.288864 15.109958 -72.862892 -106.057610 -84.869133 73.786911
#pstaffa[1] -30.941269 -4.503622 -28.589861 -124.415558 -27.978920 -48.904305
#pstaffa[2] -35.155655 -2.373307 -29.175344 -122.453079 -31.609514 -48.711555
#pstaffa[3] -40.488373 1.514305 -29.249376 -116.649513 -35.455002 -52.798489
#pstaffa[4] -46.279568 6.885459 -28.911016 -109.985489 -41.024258 -57.806808
#s_boccola 122.353157 28.589027 -26.278950 4.565524 -18.554205 -5.622464
#s_compensa -24.753588 -31.249014 -8.153748 8.469764 -36.691944 -10.787655
#s_d_ripresa1 -64.345833 -26.459229 -32.460148 -94.837769 -81.999741 33.020504
#s_d_ripresa2 166.988449 3.860986 -22.140425 69.718323 -85.623680 0.934498
#s_nastro 60.398201 -13.690007 -35.476715 -58.109234 -24.716591 77.900795
#s_organo -150.291901 28.689070 6.563872 2.223823 -106.563164 11.921017
#s_p_ripresa1 11.040032 -15.438774 -26.277647 72.636787 -81.178314 -1.935110
#s_p_ripresa2 122.153877 -9.298317 -20.594343 -80.025246 -44.326294 -11.654998
#s_preciso 39.180393 0.698472 -22.675859 44.227100 -39.620831 -66.479294
#s_pressa 121.962151 41.941689 -20.828955 1.061230 -24.449238 -2.715859
#s_staffa -11.624076 4.265024 -34.406891 -36.207802 -14.044172 43.596806
#s_staffa1 -34.197010 1.124742 -21.919893 0.900605 -77.374573 4.021030
#s_t_boccola 108.971222 19.298983 -31.812101 -31.768576 -16.913351 40.461388
#s_tornio1 8.009364 26.695759 -13.386598 10.379459 -32.510124 -14.568227
#s_tornio2 17.807327 11.467407 -24.797777 30.023392 -24.810745 -40.383213
#s_vuota 13.859957 -25.294680 -36.679482 46.619968 -13.170345 -55.526028
#staffa -9.968853 15.493058 -32.456501 -28.562216 -14.953928 34.758282
#staffa1 -34.200138 1.084933 -21.957170 0.901900 -77.336456 4.023871
#staffa2 -27.802492 0.958267 -22.011389 -0.159654 -77.321701 -2.055884
#staffa3 -22.145452 1.639937 -22.008783 -1.098148 -77.267006 -7.431816
#staffa4 -17.768513 2.727454 -22.071346 -1.818697 -77.101265 -11.589454
#t_boccola 108.549728 20.009346 -37.011063 -44.508842 -12.983923 53.848858
#temp -28.044237 -43.317696 -4.833231 -13.285618 -41.622765 18.893490
#temp1 5.203768 -24.643253 -16.776196 72.896187 -78.792267 8.908207
#terra 123.033333 50.803394 -82.080467 162.615967 -37.386269 -176.660080
#tornio1_spingi 8.016658 28.534483 -18.457306 12.101651 -27.536903 -16.557703
#tornio1c 8.534534 28.625217 -17.164083 12.348418 -28.916651 -17.029999
#tornio1car 28.837461 -4.589703 -29.647171 47.361568 -26.832306 -64.712402
#tornio1s 8.498324 29.168850 -18.505531 12.817990 -27.611385 -17.533798
#tornio2c -8.080742 29.223392 -18.358770 -11.020666 -27.506041 16.157028
#tornio2car 38.764114 5.710826 -30.171654 52.856842 -33.472816 -77.066574
#tornio2s -8.253194 29.488615 -18.760996 -12.952061 -27.351347 17.307302
#u_d_ripresa1 -28.044237 -43.317955 -4.832188 -13.285294 -41.622131 18.893496
#u_d_ripresa2 159.922607 37.918106 -55.963139 81.510376 -73.587738 -31.740944
#u_organo -159.539169 -18.196989 7.241114 -8.622941 -75.460930 23.538839
#uscita_magazzin 102.726234 -4.038835 -20.955385 23.666050 -27.274908 -31.091257
#uscita_pallet1 -65.433167 -38.727219 1.467357 -32.588219 -58.792118 49.592438
#ustaffa[1] -32.880699 -13.468471 -20.104269 -106.941696 -25.866024 -67.548241
#ustaffa[2] -37.522568 -12.268504 -20.047703 -105.878197 -29.817677 -66.795853
#ustaffa[3] -42.657303 -6.329938 -22.201424 -105.319244 -34.751678 -65.476006
#ustaffa[4] -49.181808 -1.632440 -20.784639 -99.021164 -41.543041 -70.743546
#v_organo -149.657059 34.253063 2.569242 2.275314 -102.556023 10.952513
#vedi -84.929062 -19.077705 -7.838068 5.693790 -37.159561 172.351410
#vuota 13.859957 24.662899 -84.988075 165.114395 -40.139282 -178.182739
.END
.REALS
a = 0
abilita = 0
absangolo = 1.23978e-05
ac = -9.50999
accendi = 0
accensione = 0
accu_ripresa = 3
affida = 80.32
aggiorna = 0
all_mas = 0
allarme = 0
allarme_0 = 0
allarme_1 = 0
allarme_10 = 0
allarme_11 = 0
allarme_12 = 0
allarme_13 = 0
allarme_14 = 0
allarme_15 = 0
allarme_16 = 0
allarme_17 = 0
allarme_18 = 0
allarme_19 = 0
allarme_2 = 0
allarme_20 = 0
allarme_3 = 0
allarme_4 = 0
allarme_5 = 0
allarme_6 = 0
allarme_7 = 0
allarme_8 = 0
allarme_9 = 0
allarme_m = 0
alt_piano1 = 258
altezza_max = 500
ang_compensa = 0
angolo = 1.23978e-05
angolo_cal[0] = 0
ar[1] = -0.30957
ar[2] = -0.790161
ar[3] = -0.0633545
ar[4] = 154.927
ar[5] = 0.0169985
ar[6] = 174.064
b = 0
buf[0] = -0.0162048
buf[1] = -0.000732422
buf[2] = 0.287231
buf[3] = -19.351
buf[4] = 0.000215569
buf[5] = 19.3506
c_fin = -31.4539
c_fin1 = 37.1716
c_max = 90
c_max1 = 90
c_min = -90
c_min1 = -90
cal_q_staffa = 0
calibra_mag = 0
cf = -1
chiude = 1
chiuso_tor1 = 0
chiuso_tor2 = 0
ciclo_staz = 1
clonna1 = 1
cntapezzi = 54
co0ntapezzi = 65
cocontapezzi = 10
collonna1 = 0
colonna = 3
colonna0 = 0
colonna1 = 1
colonna2 = 5
colonnam = 0
conta = 1
contaapezzi = 5
contaopezzi = 15
contaopezzzi = 60
contapezi = 26
contapezszsi = 15
contapezzi = 60
contapezziconta = 30
contapezzii = 30
contapezzio = 15
contapezzzi = 40
contapizze = 35
contaqpezzi = 64
contaquali = 0
contcontapezzi = 40
contr_piano = 1
contrapezzi = 50
conttapezzi = 25
conttapizze = 30
copntapezzi = 31
corr_ang = -8.10304
corr_ang1 = -37.1716
cotapezzi = 30
ctxrx = 1
d_pre = 811.046
dist_pre = 800
dist_sup = 300
domanda = 1
entrata[1] = 50
entrata[2] = 50
entrata[3] = 50
err = 0
errore = 4
f_calibra_1 = 0
f_calibra_2 = 0
f_meno = 0
fare = 1
fase0 = 1
fase1 = 1
fase10 = 1
fase11 = 1
fase12 = 1
fase13 = 1
fase14 = 1
fase15 = 1
fase16 = 1
fase17 = 1
fase18 = 1
fase19 = 1
fase2 = 1
fase20 = 1
fase3 = 1
fase4 = 5
fase5 = 1
fase6 = 1
fase7 = 1
fase8 = 1
fase9 = 1
fasem = 99
fine_magazzino = 0
fine_pallet1 = 0
flag = 0
flash = 0
fsuona = 1
fuori_pressa = 500
fuori_tornio = 800
g_meno = 0
gain_1 = 100
gain_2 = 100
gain_3 = 100
gain_4 = 80
gain_5 = 80
gain_6 = 80
gg = 399.644
giunto = 7
globali = 13889
in1_pressa = 1037
in1_tornio1 = 1025
in1_tornio2 = 1031
in2_pressa = 1038
in2_tornio1 = 1026
in2_tornio2 = 1032
in3_pressa = 1039
in3_tornio1 = 1027
in3_tornio2 = 1033
in4_pressa = 1040
in4_tornio1 = 1028
in4_tornio2 = 1034
in5_tornio1 = 1029
in5_tornio2 = 1035
in6_tornio1 = 1030
in6_tornio2 = 1036
in_ciclo = 2
in_teach = 5
int_col1 = 190
int_col2 = 190
int_colm = 99.9111
int_colonna1 = 200
int_euro1 = 168
int_euro2 = 168
int_falda1 = 12
int_falda2 = 12
int_piano1 = 74
int_piano2 = 72
int_pianom = 24
int_riga1 = 185
int_riga2 = 185
int_rigam = 24.8763
inter_euro1 = 145
k_corr = -20
km1 = 6
km_puls_on = 0
lamp1 = 2020
lamp10 = 2029
lamp11 = 2030
lamp2 = 2021
lamp3 = 2022
lamp4 = 2023
lamp5 = 2024
lamp6 = 2025
lamp7 = 2026
lamp8 = 2027
lamp9 = 2028
lamp_blu = 7
loc[0] = -1163.4
loc[1] = -0.367567
loc[2] = -48.5374
loc[3] = 23.1396
loc[4] = -0.520414
loc[5] = -68.1464
loc[6] = 0.674542
loc1[1] = -0.367306
loc1[2] = -48.5384
loc1[3] = 23.1404
loc1[4] = -0.52009
loc1[5] = -68.1467
loc1[6] = 0.67453
lung_chr = 38
magnete = 48
mem_allarme = 0
misura = 25.9059
mk_avvio = 0
mk_ciclo_on = 1
mk_off = 1
mk_primo = 0
mk_puls_on = 1
mk_rob_resprog = 2005
mk_sel_ok = 0
mox = 0
moy = 300
n_cerca_max = 10
n_colonna1 = 6
n_colonna2 = 6
n_colonnam = 5
n_piano1 = 4
n_prelievi = 0
n_riga1 = 4
n_riga2 = 4
n_rigam = 12
npezzi = 1
offset_col = -120
offset_col2 = 50
offset_falda_x = -40
offset_falda_y = -80
offset_riga = 0
offset_riga2 = 50
offset_rot = 19.5569
offset_rot_old = 18.9389
on_mot_4 = 0
out1_pressa = 35
out1_tornio1 = 25
out1_tornio2 = 30
out2_pressa = 36
out2_tornio1 = 26
out2_tornio2 = 31
out3_pressa = 37
out3_tornio1 = 27
out3_tornio2 = 32
out4_pressa = 38
out4_tornio1 = 28
out4_tornio2 = 33
out5_tornio1 = 29
out5_tornio2 = 34
p1_t = 2037
p2_t = 2038
p3_t = 2039
p4_t = 2040
paino1 = 1
pano1 = 0
passo = 0
pian0o1 = 1
piano = 2
piano0 = 1
piano1 = 3
piano11 = 1
piano2 = 0
piaon1 = 3
pino1 = 1
pnt_staffa = 4
potenza = 1018
ppiano1 = 0
preciso = 10
psp1_1 = 1008
puiano1 = 1
puls_cycle = 1046
puls_reset = 1020
q_staffa = 26.6251
quota_staffa = 0.862793
r_fase0 = 2
r_fase10 = 1
r_fase11 = 1
r_fase4 = 1
rel1[0] = -1154.42
rel1[1] = 305.19
rel1[2] = 248.501
rel1[3] = -51.7287
rel1[4] = 179.451
rel1[5] = 36.0995
rel2[0] = -2103
rel2[1] = 305.329
rel2[2] = 248.789
rel2[3] = -51.7287
rel2[4] = 179.451
rel2[5] = 130.829
rfalg = 2
rflag = 0
riga = 0
riga0 = 0
riga1 = 1
riga2 = 1
riga3 = 1
rigam = 12
rl3 = 17
rl4 = 18
ruota = -125
sb1 = 1015
sb2 = 1016
scosta = 0.0451893
sel_camp = 2002
sel_scarico = 2036
sel_svuota = 2001
selcontinuo = 1007
setta_tornio = 0
setta_visione = 0
setta_visone = 0
setta_vsione = 1
setta_vvisione = 0
settore = 1
si_rob_exmaster = 1046
side = 1
sirena = 23
soglia = 1
soglia1 = 1
soglia_staffa = 3
sopra = 300
sp = 80
sq1 = 1001
sq10 = 1011
sq11 = 1012
sq12 = 1013
sq13 = 1014
sq15 = 1021
sq16 = 1022
sq17 = 1023
sq18 = 1024
sq19 = 1041
sq2 = 1002
sq20 = 1042
sq21 = 1004
sq22 = 1044
sq23 = 1045
sq24 = 1047
sq3 = 1003
sq30 = 1043
sq5 = 1005
sq50 = 1049
sq51 = 1050
sq52 = 1052
sq6 = 1006
sq8 = 1009
sq9 = 1010
sq_fibra = 1051
stop_4 = 0
stop_visione = 0
suona = 0
t_apre1 = 0.9
t_apre2 = 0.5
t_apri_mdr1 = 0
t_apri_mdr2 = 0
t_avvio = 0
t_boccol = 0
t_boccola = 1
t_c_ripresa1 = 1
t_c_ripresa2 = 1
t_chiude = 1.5
t_chiude1 = 1.8
t_chiude2 = 0.5
t_chiudi_mdr1 = 0
t_chiudi_mdr2 = 0
t_clock = 0.5
t_debug = 1
t_flash = 1.91402e+06
t_intervallo = 30
t_passo = 0.7
t_prova = 20
t_stab = 0.2
t_staz0 = 1.91402e+06
t_staz1 = 1.91378e+06
t_staz10 = 1.91391e+06
t_staz11 = 1.91395e+06
t_staz12 = 1.91378e+06
t_staz13 = 1.91378e+06
t_staz14 = 1.91378e+06
t_staz15 = 1.91378e+06
t_staz16 = 1.91378e+06
t_staz17 = 1.91378e+06
t_staz18 = 1.91378e+06
t_staz19 = 1.91378e+06
t_staz2 = 1.91378e+06
t_staz20 = 1.91378e+06
t_staz3 = 1.91378e+06
t_staz4 = 1.91378e+06
t_staz5 = 1.91378e+06
t_staz6 = 1.91378e+06
t_staz7 = 1.91378e+06
t_staz8 = 1.91378e+06
t_staz9 = 1.91378e+06
t_suona = 1.91378e+06
t_vuoto = 1
tacita = 1019
tcarico = 129.5
tdebug = 0
tenta = 1
tentacerca = 0
termiche = 1017
tinizio = 1.91395e+06
tipo = 0
tocca = 1
togli_falda = 0
trovato = 0
ttot = 129.5
tutte = 0
uscita[1] = 50
uscita[2] = 50
uscita[3] = 50
vel_presa = 10
vel_ripresa = 10
via_robot = 1
via_robot0 = 0
via_robot1 = 0
via_robot10 = 0
via_robot11 = 0
via_robot12 = 0
via_robot13 = 0
via_robot14 = 0
via_robot15 = 0
via_robot16 = 0
via_robot17 = 0
via_robot18 = 0
via_robot19 = 0
via_robot2 = 0
via_robot20 = 0
via_robot3 = 0
via_robot4 = 1
via_robot5 = 0
via_robot6 = 0
via_robot7 = 0
via_robot8 = 0
via_robot9 = 0
via_staz0 = 0
via_staz1 = 0
via_staz10 = 0
via_staz11 = 0
via_staz12 = 0
via_staz13 = 0
via_staz14 = 0
via_staz15 = 0
via_staz16 = 0
via_staz17 = 0
via_staz18 = 0
via_staz19 = 0
via_staz2 = 0
via_staz20 = 0
via_staz3 = 0
via_staz4 = 0
via_staz5 = 0
via_staz6 = 0
via_staz7 = 0
via_staz8 = 0
via_staz9 = 0
vis.occupato = 0
watch_dog = 1
x = 69.72
xcontapezzi = 15
xg = 247.69
y = 70.32
yg = 187.61
yv1_1 = 15
yv2a_1 = 9
yv2r_1 = 10
yv3a_1 = 11
yv3r_1 = 12
yv4_1 = 13
yv5_1 = 16
yv6_1 = 8
yv7a_1 = 19
yv7r_1 = 20
yv8a_1 = 21
yv8r_1 = 22
zero_magazzino = 0
zero_q_staffa[1] = 30.2309
zero_q_staffa[2] = 32.8572
zero_q_staffa[3] = 25.6068
zero_q_staffa[4] = 25.7623
.END
.STRINGS
$boccola = "caricata"
$codice = "101"
$dato0 = " ***      P R O D U Z I O N E       *** "
$dato1 = " TEMPO CICLO SEC .............."
$dato2 = "  CICLI TOTALI ................."
$dato3 = "  PRODUZIONE ORARIA ............"
$fronte = "negativo"
$fronte1 = "positivo"
$mess0 = "                                        "
$mess1 = "     N E S S U N   A L L A R M E        "
$mess2 = "           P R E S E N T E              "
$mess3 = "                                        "
$messaggio = "VUOI AZZERARE LA PRODUZIONE ? : SI <1> - NO <0> "
$pressa = "vuota"
$report1 = " ***      P R O D U Z I O N E       ***  TEMPO CICLO SEC ..............  129.50  CICLI TOTALI .................       1  PRODUZIONE ORARIA ............      28"
$rx = ""
$spazio = " "
$stampa = "AZZERO PALLET SCARICO ? : SI <1> - NO <0> "
$temp = "1"
$tempo1 = "  129.50"
$tempo2 = "       1"
$tempo3 = "      28"
$tipo = "CASE 3400"
$tornio1 = "pieno"
$tornio2 = "pieno"
$tronca = " CONTINUO ???                           "
$tx = "M0"
$video = "        T  H  E  S  Y  S   s.r.l.                 Montichiari - BRESCIA                     Tel.  030-9961811               www.thesys.it  techdept@thesys.it   "
$video0 = "        T  H  E  S  Y  S   s.r.l.       "
$video1 = "          Montichiari - BRESCIA         "
$video2 = "            Tel.  030-9961811           "
$video3 = "    www.thesys.it  techdept@thesys.it   "
.END
