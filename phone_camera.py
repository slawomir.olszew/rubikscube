import numpy as np
import cv2

cap = cv2.VideoCapture('rtsp://admin:admin@100.68.26.128:8554/live')

while (True):
    # Capture frame-by-frame
    ret, frame = cap.read()

    frame = cv2.rotate(frame, cv2.cv2.ROTATE_90_CLOCKWISE)
    # Display the resulting frame
    cv2.imshow('frame', frame)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()
