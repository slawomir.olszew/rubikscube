import numpy as np
from robot import *

# kawasaki = Robot(27040, "192.168.0.220")
# kawasaki = Robot(27040)
mitsubishi = Robot(27041)

next_move = None

print("Remember to add ';angle_value' after every command, even if it's not a rotation command, e.g. 'T;0' or 'G;0'")
while next_move != 'q':
    next_robot = input("Choose robot(k - KAWASAKI, m: MITSIBISHI: ")
    next_move = input("Choose move: ")

    if next_move == 'q':
        #kawasaki.move("Q;0")
        mitsubishi.move("Q;0")
    if next_robot == 'k':
        pass
        #print(kawasaki.move(next_move))
    elif next_robot == 'm':
        pass
        print(mitsubishi.move(next_move))
    else:
        print("Wrong robot.")
