import cv2
import numpy as np

COLOR_NAMES = ["BIALY", "CZERWONY", "NIEBIESKI", "POMARANCZOWY", "ZIELONY", "ZOLTY"]
# COLORS = [(255, 255, 244.44),
#           (255.0, 62.44, 12.33),
#           (44.33, 86.77, 151.55),
#           (255.0, 224, 29.77),
#           (121, 255, 73.33),
#           (255, 255, 85.11)]
COLORS = [(244.44, 255, 255),
          (12.33, 62.44, 255.0),
          (151.55, 86.77, 43.33),
          (29.77, 224, 255.0),
          (73.33, 255, 121),
          (85.11, 255, 255)]


def draw_lines(img):
    color = (0, 0, 0)
    thick = 2
    img = cv2.line(img, (198, 117), (198, 370), color, thick)
    img = cv2.line(img, (198, 117), (448, 117), color, thick)
    img = cv2.line(img, (198, 370), (448, 370), color, thick)
    img = cv2.line(img, (448, 117), (448, 370), color, thick)
    img = cv2.line(img, (281, 117), (281, 370), color, thick)
    img = cv2.line(img, (364, 117), (364, 370), color, thick)
    img = cv2.line(img, (198, 201), (448, 201), color, thick)
    img = cv2.line(img, (198, 285), (448, 285), color, thick)
    return img


def histogram(img):
    color = []
    for channel in cv2.split(img):
        array = np.array(channel).flatten()
        unique, counts = np.unique(array, return_counts=True)
        array_h = np.zeros(256)
        array_h[unique] = counts
        color.append(np.argmax(array_h))
    return tuple(color)


# zmienic
def color_extraction(img):
    UL = img[125:195, 205:275]
    UL_histogram = histogram(UL)
    UM = img[125:195, 290:362]
    UM_histogram = histogram(UM)
    UR = img[125:195, 378:440]
    UR_histogram = histogram(UR)

    ML = img[208:278, 205:275]
    ML_histogram = histogram(ML)
    MM = img[208:278, 290:362]
    MM_histogram = histogram(MM)
    MR = img[208:278, 378:440]
    MR_histogram = histogram(MR)

    DL = img[293:362, 205:275]
    DL_histogram = histogram(DL)
    DM = img[293:362, 290:362]
    DM_histogram = histogram(DM)
    DR = img[293:362, 378:440]
    DR_histogram = histogram(DR)

    histogram_array = [UL_histogram, UM_histogram, UR_histogram,
                       ML_histogram, MM_histogram, MR_histogram,
                       DL_histogram, DM_histogram, DR_histogram]
    hist = mean_h(histogram_array)
    print(hist)
    # print(read_cube_colors(histogram_array))


def read_cube_colors(cubes_histogram):
    read_cube = []
    for cube_histogram in cubes_histogram:
        differences = [[color_difference(cube_histogram, target_value), i] for i, target_value in
                       enumerate(COLORS)]
        print(cube_histogram, differences)
        differences.sort()
        color_name = COLOR_NAMES[differences[0][1]]
        read_cube.append(color_name)
    return read_cube


def color_difference(color1, color2):
    return sum([abs(component1 - component2) for component1, component2 in zip(color1, color2)])


def mean_h(histogram_array):
    blue, green, red = 0, 0, 0
    number_of_h = len(histogram_array)
    for histogram in histogram_array:
        blue += histogram[0]
        green += histogram[1]
        red += histogram[2]
    return tuple([blue / number_of_h, green / number_of_h, red / number_of_h])


image = cv2.imread('kolory_mitusbishi/zolty.png')
image = draw_lines(image)

cv2.imshow('image', image)
color_extraction(image)
cv2.waitKey()