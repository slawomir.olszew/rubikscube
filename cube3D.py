from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
import matplotlib.pyplot as plt
from matplotlib.widgets import Button
import numpy as np
from matplotlib import animation
import matplotlib as mpl

mpl.rcParams['toolbar'] = 'None'


# odpalac przed .prepare_for_kociemba z argumentem colors_array=.SIDES
class Cube:
    def __init__(self, colors_array=None):
        plt.ion()
        self.finished = False
        self.num = 9
        self.colors_dict = {'BIALY': 'white', 'CZERWONY': 'red', 'NIEBIESKI': 'blue', 'POMARANCZOWY': 'orange',
                            'ZIELONY': 'green', 'ZOLTY': 'yellow'}
        self.fig = plt.figure()
        self.ax = self.fig.gca(projection='3d')
        if colors_array:
            self.colors = self.get_colors(colors_array)
        else:
            self.colors = ['orange'] * 9 + ['green'] * 9 + ['red'] * 9 + ['blue'] * 9 + ['yellow'] * 9 + ['white'] * 9
        self.color_gen = self.color_generator()

        self.up = []
        for x in np.linspace(-1.5, 0.5, 3):
            for y in np.linspace(-1.5, 0.5, 3):
                verts = [[(x, y, 1.5), (x + 1, y, 1.5), (x + 1, y + 1, 1.5), (x, y + 1, 1.5)]]
                cube = Poly3DCollection(verts, facecolors=next(self.color_gen), edgecolors='black')
                self.ax.add_collection3d(cube)
                self.up.append([cube, verts])
        self.down = []
        for x in np.linspace(-1.5, 0.5, 3):
            for y in np.linspace(-1.5, 0.5, 3):
                verts = [[(x, y, -1.5), (x + 1, y, -1.5), (x + 1, y + 1, -1.5), (x, y + 1, -1.5)]]
                cube = Poly3DCollection(verts, facecolors=next(self.color_gen), edgecolors='black')
                self.ax.add_collection3d(cube)
                self.down.append([cube, verts])
        self.front = []
        for x in np.linspace(-1.5, 0.5, 3):
            for z in np.linspace(-1.5, 0.5, 3):
                verts = [[(x, -1.5, z), (x + 1, -1.5, z), (x + 1, -1.5, z + 1), (x, -1.5, z + 1)]]
                cube = Poly3DCollection(verts, facecolors=next(self.color_gen), edgecolors='black')
                self.ax.add_collection3d(cube)
                self.front.append([cube, verts])
        self.back = []
        for x in np.linspace(-1.5, 0.5, 3):
            for z in np.linspace(-1.5, 0.5, 3):
                verts = [[(x, 1.5, z), (x + 1, 1.5, z), (x + 1, 1.5, z + 1), (x, 1.5, z + 1)]]
                cube = Poly3DCollection(verts, facecolors=next(self.color_gen), edgecolors='black')
                self.ax.add_collection3d(cube)
                self.back.append([cube, verts])
        self.right = []
        for y in np.linspace(-1.5, 0.5, 3):
            for z in np.linspace(-1.5, 0.5, 3):
                verts = [[(1.5, y, z), (1.5, y + 1, z), (1.5, y + 1, z + 1), (1.5, y, z + 1)]]
                cube = Poly3DCollection(verts, facecolors=next(self.color_gen), edgecolors='black')
                self.ax.add_collection3d(cube)
                self.right.append([cube, verts])
        self.left = []
        for y in np.linspace(-1.5, 0.5, 3):
            for z in np.linspace(-1.5, 0.5, 3):
                verts = [[(-1.5, y, z), (-1.5, y + 1, z), (-1.5, y + 1, z + 1), (-1.5, y, z + 1)]]
                cube = Poly3DCollection(verts, facecolors=next(self.color_gen), edgecolors='black')
                self.ax.add_collection3d(cube)
                self.left.append([cube, verts])
        self.refresh_sides()
        ax_viewReset = plt.axes([0.80, 0.05, 0.15, 0.075])
        self.b_viewReset = Button(ax_viewReset, 'Reset View')
        self.b_viewReset.on_clicked(self.reset_view)
        self.b_viewReset.set_active(False)

        self.ax.set_xlim(-1.5, 1.5)
        self.ax.set_ylim(-1.5, 1.5)
        self.ax.set_zlim(-1.5, 1.5)
        self.ax.set_xticklabels([])
        self.ax.set_yticklabels([])
        self.ax.set_zticklabels([])
        self.ax.grid(False)
        self.ax.axis('off')
        plt.show(block=False)

    def refresh_sides(self):
        self.up_side = self.up + self.front[2::3] + self.left[2::3] + self.back[2::3] + self.right[2::3]
        self.down_side = self.down + self.front[::3] + self.left[::3] + self.back[::3] + self.right[::3]
        self.front_side = self.front + self.up[::3] + self.right[0:3] + self.down[::3] + self.left[0:3]
        self.back_side = self.back + self.up[2::3] + self.right[6:9] + self.down[2::3] + self.left[6:9]
        self.right_side = self.right + self.front[6:9] + self.up[6:9] + self.back[6:9] + self.down[6:9]
        self.left_side = self.left + self.front[0:3] + self.up[0:3] + self.back[0:3] + self.down[0:3]

    def get_colors(self, c_a):
        colors = []
        sides_by_my_order = [c_a[0], c_a[2], c_a[4], c_a[1], c_a[3], c_a[5]]
        for i, side in enumerate(sides_by_my_order):
            if i == 5 or i == 3:
                side = [side[8], side[5], side[2], side[7], side[4], side[1], side[6], side[3], side[0]]
            elif i == 1:
                side = [side[0], side[3], side[6], side[1], side[4], side[7], side[2], side[5], side[8]]
            else:
                side = [side[6], side[3], side[0], side[7], side[4], side[1], side[8], side[5], side[2]]
            for color in side:
                colors.append(self.colors_dict[color])
        return colors

    def color_generator(self):
        for color in self.colors:
            yield color

    def rotate_Z(self, side_l, mov):
        if side_l == 'u':
            side = [[i, s] for i, s in enumerate(self.up_side)]
        elif side_l == 'd':
            side = [[i, s] for i, s in enumerate(self.down_side)]
        angle_max = mov * np.pi / 2
        for _ in range(self.num):
            for i, cube_verts in side:
                cube, verts = cube_verts
                angle = angle_max / self.num
                verts = verts[0]
                verts = np.array(verts).astype(np.float)
                R = np.array([[np.cos(angle), -np.sin(angle), 0],
                              [np.sin(angle), np.cos(angle), 0],
                              [0, 0, 1]])
                new_verts = []
                for vert in verts:
                    new_vert = np.dot(vert, R.T)
                    new_vert = tuple(new_vert)
                    new_verts.append(new_vert)
                new_verts = [new_verts]
                cube.set_verts(new_verts)
                side[i][1][1] = new_verts
            plt.draw()
            plt.pause(.001)
        if side_l == 'u':
            if mov == 1:
                tmp = self.front[8::-3]
                self.front[2::3] = self.left[8::-3]
                tmp2 = self.right[8::-3]
                self.right[8::-3] = tmp
                tmp = self.back[8::-3]
                self.back[2::3] = tmp2
                self.left[8::-3] = tmp
                self.up = [self.up[2], self.up[5], self.up[8], self.up[1], self.up[4], self.up[7],
                           self.up[0], self.up[3], self.up[6]]
            elif mov == -1:
                for _ in range(3):
                    tmp = self.front[8::-3]
                    self.front[2::3] = self.left[8::-3]
                    tmp2 = self.right[8::-3]
                    self.right[8::-3] = tmp
                    tmp = self.back[8::-3]
                    self.back[2::3] = tmp2
                    self.left[8::-3] = tmp
                self.up = [self.up[6], self.up[3], self.up[0], self.up[7], self.up[4], self.up[1],
                           self.up[8], self.up[5], self.up[2]]
        elif side_l == 'd':
            if mov == 1:
                tmp = self.front[6::-3]
                self.front[0::3] = self.left[6::-3]
                tmp2 = self.right[6::-3]
                self.right[6::-3] = tmp
                tmp = self.back[6::-3]
                self.back[0::3] = tmp2
                self.left[6::-3] = tmp
                self.down = [self.down[2], self.down[5], self.down[8], self.down[1], self.down[4], self.down[7],
                             self.down[0], self.down[3], self.down[6]]
            elif mov == -1:
                for _ in range(3):
                    tmp = self.front[6::-3]
                    self.front[0::3] = self.left[6::-3]
                    tmp2 = self.right[6::-3]
                    self.right[6::-3] = tmp
                    tmp = self.back[6::-3]
                    self.back[0::3] = tmp2
                    self.left[6::-3] = tmp
                self.down = [self.down[6], self.down[3], self.down[0], self.down[7], self.down[4], self.down[1],
                             self.down[8], self.down[5], self.down[2]]
        self.refresh_sides()

    def rotate_Y(self, side_l, mov):
        if side_l == 'f':
            side = [[i, s] for i, s in enumerate(self.front_side)]
        elif side_l == 'b':
            side = [[i, s] for i, s in enumerate(self.back_side)]
        angle_max = -mov * np.pi / 2
        for _ in range(self.num):
            for i, cube_verts in side:
                cube, verts = cube_verts
                angle = angle_max / self.num
                verts = verts[0]
                verts = np.array(verts).astype(np.float)
                R = np.array([[np.cos(angle), 0, np.sin(angle)],
                              [0, 1, 0, ],
                              [-np.sin(angle), 0, np.cos(angle)]])
                new_verts = []
                for vert in verts:
                    new_vert = np.dot(vert, R.T)
                    new_vert = tuple(new_vert)
                    new_verts.append(new_vert)
                new_verts = [new_verts]
                cube.set_verts(new_verts)
                side[i][1][1] = new_verts
            plt.draw()
            plt.pause(.001)
        if side_l == 'f':
            if mov == -1:
                tmp = self.right[0:3]
                self.right[2::-1] = self.up[::3]
                tmp2 = self.down[::3]
                self.down[::3] = tmp
                tmp = self.left[0:3]
                self.left[2::-1] = tmp2
                self.up[::3] = tmp

                self.front = [self.front[6], self.front[3], self.front[0], self.front[7], self.front[4], self.front[1],
                              self.front[8], self.front[5], self.front[2]]

            elif mov == 1:
                for _ in range(3):
                    tmp = self.right[0:3]
                    self.right[2::-1] = self.up[::3]
                    tmp2 = self.down[::3]
                    self.down[::3] = tmp
                    tmp = self.left[0:3]
                    self.left[2::-1] = tmp2
                    self.up[::3] = tmp

                self.front = [self.front[2], self.front[5], self.front[8], self.front[1], self.front[4], self.front[7],
                              self.front[0], self.front[3], self.front[6]]

        elif side_l == 'b':
            if mov == 1:
                tmp = self.up[2::3]
                self.up[8::-3] = self.right[6:9]
                tmp2 = self.left[6:9]
                self.left[6:9] = tmp
                tmp = self.down[2::3]
                self.down[8::-3] = tmp2
                self.right[6:9] = tmp

                self.back = [self.back[2], self.back[5], self.back[8], self.back[1], self.back[4], self.back[7],
                             self.back[0], self.back[3], self.back[6]]
            elif mov == -1:
                for _ in range(3):
                    tmp = self.up[2::3]
                    self.up[8::-3] = self.right[6:9]
                    tmp2 = self.left[6:9]
                    self.left[6:9] = tmp
                    tmp = self.down[2::3]
                    self.down[8::-3] = tmp2
                    self.right[6:9] = tmp
                self.back = [self.back[6], self.back[3], self.back[0], self.back[7], self.back[4], self.back[1],
                             self.back[8], self.back[5], self.back[2]]
        self.refresh_sides()

    def rotate_X(self, side_l, mov):
        if side_l == 'r':
            side = [[i, s] for i, s in enumerate(self.right_side)]
        elif side_l == 'l':
            side = [[i, s] for i, s in enumerate(self.left_side)]
        angle_max = -mov * np.pi / 2
        for _ in range(self.num):
            for i, cube_verts in side:
                cube, verts = cube_verts
                angle = angle_max / self.num
                verts = verts[0]
                verts = np.array(verts).astype(np.float)
                R = np.array([[1, 0, 0],
                              [0, np.cos(angle), -np.sin(angle)],
                              [0, np.sin(angle), np.cos(angle)]])
                new_verts = []
                for vert in verts:
                    new_vert = np.dot(vert, R.T)
                    new_vert = tuple(new_vert)
                    new_verts.append(new_vert)
                new_verts = [new_verts]
                cube.set_verts(new_verts)
                side[i][1][1] = new_verts
            plt.draw()
            plt.pause(.001)
        if side_l == 'r':
            if mov == -1:
                tmp = self.front[6:9]
                self.front[6:9] = self.up[6:9]
                tmp2 = self.down[6:9]
                self.down[6:9] = tmp[::-1]
                tmp = self.back[6:9]
                self.back[6:9] = tmp2
                self.up[6:9] = tmp[::-1]
                self.right = [self.right[2], self.right[5], self.right[8], self.right[1], self.right[4], self.right[7],
                              self.right[0], self.right[3], self.right[6]]

            elif mov == 1:
                for _ in range(3):
                    tmp = self.front[6:9]
                    self.front[6:9] = self.up[6:9]
                    tmp2 = self.down[6:9]
                    self.down[6:9] = tmp[::-1]
                    tmp = self.back[6:9]
                    self.back[6:9] = tmp2
                    self.up[6:9] = tmp[::-1]
                self.right = [self.right[6], self.right[3], self.right[0], self.right[7], self.right[4], self.right[1],
                              self.right[8], self.right[5], self.right[2]]

        elif side_l == 'l':
            if mov == 1:
                tmp = self.up[0:3]
                self.up[0:3] = self.front[0:3]
                tmp2 = self.back[2::-1]
                self.back[2::-1] = tmp
                tmp = self.down[0:3]
                self.down[2::-1] = tmp2
                self.front[2::-1] = tmp

                self.left = [self.left[6], self.left[3], self.left[0], self.left[7], self.left[4], self.left[1],
                             self.left[8], self.left[5], self.left[2]]
            elif mov == -1:
                for _ in range(3):
                    tmp = self.up[0:3]
                    self.up[0:3] = self.front[0:3]
                    tmp2 = self.back[2::-1]
                    self.back[2::-1] = tmp
                    tmp = self.down[0:3]
                    self.down[2::-1] = tmp2
                    self.front[2::-1] = tmp
                self.left = [self.left[2], self.left[5], self.left[8], self.left[1], self.left[4], self.left[7],
                             self.left[0], self.left[3], self.left[6]]
        self.refresh_sides()

    def reset_view(self, event):
        curr_azim = self.ax.azim
        curr_elev = self.ax.elev
        for azim in np.linspace(curr_azim, -60, self.num):
            self.ax.view_init(elev=curr_elev, azim=azim)
            plt.draw()
            plt.pause(0.001)
        for elev in np.linspace(curr_elev, 30, self.num):
            self.ax.view_init(elev=elev, azim=-60)
            plt.draw()
            plt.pause(0.001)

    @staticmethod
    def show(block):
        plt.show(block=block)

    @staticmethod
    def start():
        plt.draw()
        plt.pause(1)

    def move(self, move, angle):
        def single_move():
            if move in X:
                self.rotate_X(move.lower(), direction)
            if move in Y:
                self.rotate_Y(move.lower(), direction)
            if move in Z:
                self.rotate_Z(move.lower(), direction)
            self.show(False)

        X = ['L', 'R']
        Y = ['F', 'B']
        Z = ['U', 'D']
        left_dir_dict = {'L': 1, 'R': -1, 'F': 1, 'B': -1, 'U': 1, 'D': -1}
        direction = left_dir_dict[move]
        if angle == '90':
            direction = -1 * direction
        elif angle == '180':
            single_move()
        single_move()

    def rotate(self, N):
        if not self.finished:
            azim = self.ax.azim
            elev = self.ax.elev
            self.ax.view_init(elev=elev, azim=azim + N)


if __name__ == '__main__':
    import itertools


    def adjust(move):
        angle = '90'
        robot_move = move[0]
        if len(move) > 1:
            if move[1] == '2':
                angle = '180'
            elif move[1] == "'":
                angle = '-90'
        return robot_move, angle


    cube_array = [['CZERWONY', 'ZOLTY', 'ZOLTY', 'CZERWONY', 'NIEBIESKI', 'ZIELONY', 'BIALY', 'ZOLTY', 'POMARANCZOWY'],
                  ['ZIELONY', 'NIEBIESKI', 'BIALY', 'BIALY', 'ZOLTY', 'POMARANCZOWY', 'POMARANCZOWY', 'CZERWONY',
                   'NIEBIESKI'],
                  ['CZERWONY', 'POMARANCZOWY', 'ZIELONY', 'ZIELONY', 'ZIELONY', 'POMARANCZOWY', 'CZERWONY', 'NIEBIESKI',
                   'ZIELONY'],
                  ['ZOLTY', 'CZERWONY', 'CZERWONY', 'POMARANCZOWY', 'CZERWONY', 'CZERWONY', 'BIALY', 'NIEBIESKI',
                   'ZOLTY'],
                  ['POMARANCZOWY', 'ZIELONY', 'NIEBIESKI', 'NIEBIESKI', 'BIALY', 'ZIELONY', 'NIEBIESKI', 'BIALY',
                   'POMARANCZOWY'],
                  ['ZIELONY', 'ZOLTY', 'NIEBIESKI', 'ZOLTY', 'POMARANCZOWY', 'BIALY', 'ZOLTY', 'BIALY', 'BIALY']]
    c = Cube(cube_array)
    c.start()
    c.show(False)
    moves = "U B' U2 L' D2 R B L2 B2 U' F R F2 L2 D' L2 U' R2 L2 U' R2 B2"
    print(moves)
    moves = list(map(adjust, moves.split()))
    moves = list(zip(*(iter(list(itertools.chain(*moves))),) * 2))
    ani = animation.FuncAnimation(c.fig, c.rotate, 5, interval=30, blit=False)
    for move, angle in moves:
        c.move(move, angle)
    c.finished = True
    c.b_viewReset.set_active(True)
    c.show(True)  # zawsze na koniec, zblokuje reszte programu, dopoki nie zamknie sie okna to nie ruszy dalej
