import kociemba

'''
O - pomaranczowy
G - zielony
B - niebieski
W - bialy
R - czerwony
Y - zolty
'''

# gorna, prawa, frontowa, dolna, lewa, tylna
'''
gorna -> zielona
fronowa -> zolta
prawa -> czerwona
lewa -> pomaranczowa
tylna -> biala
dolna - niebieska
'''


gora = 'BGWGGBBGR'
prawa = 'YOORRYOYB'
front = 'YOGRYBBWY'
dol = 'WOGWBRGBW'
lewa = 'ORROOWRGO'
tyl = 'GYYBWYRWW'

kostka = gora + prawa + front + dol + lewa + tyl
kostka = kostka.replace('G', 'U')
kostka = kostka.replace('Y', 'F')
kostka = kostka.replace('O', 'L')
kostka = kostka.replace('B', 'D')
kostka = kostka.replace('W', 'B')

kostka = "UDLFUFLFBUUDRRRDUFURLLFBBBLDLBRDLULDBDFULDFDRFBRUBBRFR"


print(kociemba.solve(kostka))