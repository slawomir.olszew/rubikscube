import socket

class server():
    def __init__(self, ip_address, port):
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        print("Waiting for a client...")
        s.bind((ip_address, port))
        s.listen(10)
        self.conn, address = s.accept()
        self.last_data = b''

        print("Connected!")

    '''def __init__(self):
        self.client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.client_socket.connect(("192.168.0.24", 9999))
        #self.cap = cv2.VideoCapture(int(video), cv2.CAP_DSHOW)'''

    def send(self, message):
        message = message.encode()
        self.conn.sendall(message)

    def receive(self):
        self.last_data = self.conn.recv(4096).decode()
        return self.last_data


my_serv = server("192.168.0.73", 27041)
while(True):
    send_data = input("Server: ")
    my_serv.send(send_data)
    ack = my_serv.receive()
    print(ack)

