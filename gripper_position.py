import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
from scipy.optimize import fsolve
import math


def get_angles(aa, bb, cc, RR):

    pass



def center(x, y, z):
    X = np.sum(x) / len(x)
    Y = np.sum(y) / len(y)
    Z = np.sum(z) / len(z)
    diameter = np.sqrt(np.sum(np.square(x) + np.square(y) + np.square(z)))
    return X, Y, Z, diameter / 2


def plain(points):
    points = np.array(points)
    p1, p2, p3, p4 = points
    v1 = p2 - p1
    v2 = p4 - p3
    cp = np.cross(v1, v2)
    nn = cp / np.linalg.norm(cp)
    cp2 = np.cross(v1, nn)
    nn2 = cp2 / np.linalg.norm(cp2)
    cp3 = np.cross(nn2, nn)
    nn3 = cp3 / np.linalg.norm(cp3)
    rad_angles = np.abs(np.arcsin(nn))
    degree_angles = rad_angles * 360 / (2 * np.pi)
    alpha = np.arctan2(-nn[1], nn[2]) * 360 / (2 * np.pi)
    beta = np.arcsin(nn[0]) * 360 / (2 * np.pi)
    gamma = np.arctan2(-nn2[0], nn3[0]) * 360 / (2 * np.pi)
    return degree_angles, alpha, beta, gamma, nn


points = [(12.530, 1058.210, 935.8), (-0.36, 1057.56, 937.64), (4.63, 1057.94, 932.1), (8.390, 1058.05, 942.04)]
# points = [(2, 2, 2), (-2, -2, -2), (0, -2, 2 * np.sqrt(2)), (0, 2, -2 * np.sqrt(2))]
points = np.array(points)

x, y, z = [], [], []
for point in points:
    x_a, y_a, z_a = point
    x.append(x_a)
    y.append(y_a)
    z.append(z_a)


# R**2 = (x-a)**2 + (y-b)**2 + (z-c)**2
# 0 = (x-a)**2 + (y-b)**2 + (z-c)**2 - R**2
# 78.54398975258157 = (x - 10.01162666873382) * (x - 10.01162666873382) + (y - 979.7105913679053) * (y - 979.7105913679053) + (z - 936.6117674723424) * (z - 936.6117674723424)
# r^2 = (x - 10.01162666873382)^2 + (y - 979.7105913679053)^2 + (z - 936.6117674723424)^2
def equations(p):
    a_n, b_n, c_n, R_n = p
    return ((x[0] - a_n) ** 2 + (y[0] - b_n) ** 2 + (z[0] - c_n) ** 2 - R_n ** 2,
            (x[1] - a_n) ** 2 + (y[1] - b_n) ** 2 + (z[1] - c_n) ** 2 - R_n ** 2,
            (x[2] - a_n) ** 2 + (y[2] - b_n) ** 2 + (z[2] - c_n) ** 2 - R_n ** 2,
            (x[3] - a_n) ** 2 + (y[3] - b_n) ** 2 + (z[3] - c_n) ** 2 - R_n ** 2)

def plane(p_m):
    A_m, B_m, C_m, D_m = p_m
    return (A_m * x[0] + B_m * y[0] + C_m * z[0] + D_m,
            A_m * x[1] + B_m * y[1] + C_m * z[1] + D_m,
            A_m * x[2] + B_m * y[2] + C_m * z[2] + D_m,
            A_m * x[3] + B_m * y[3] + C_m * z[3] + D_m)


a, b, c, R = fsolve(equations, (6, 1050, 930, 10))
print(R)
v_A, v_B, v_C, offset = fsolve(plane, (10000, 1000, 1000, 10000))

print(v_A, v_B, v_C, offset)

print("Wspolrzedne srodka okregu: ", a, b, c)
print("Wspolrzedne srodka okregu (wzgledne): ", a - x[0], b - y[0], c - z[0])
print("Promien: ", np.sqrt(R))

cx, cy, cz, R = center(x, y, z)
angles, a, b, g, normal_v = plain(points)
print(f'Srodek: {cx, cy, cz}, promien: {R}')
print(f'Katy miedzy wektorem a dana osia: {angles}, wektor normalny do plaszczyzny punktow: {normal_v}')
print(f'obrot wektora normalnego wzgledem osi glownych: alpha betta gamma(rpy)-wokol (x, y, z): {a, b, g}')

fig = plt.figure(1)
ax = fig.add_subplot(111, projection='3d')
# punkty
ax.scatter(x, y, z)
# srodek
ax.scatter(cx, cy, cz, color='r')
# linie miedzy punktami
ax.plot(x[0:2], y[0:2], z[0:2], color='m')
ax.plot(x[2:4], y[2:4], z[2:4], color='m')
# plaszczyzna
d = -points[0].dot(normal_v)
xx, yy = np.meshgrid(range(np.int(np.min(x)), np.int(np.max(x)) + 1),
                     range(np.int(np.min(y)), np.int(np.max(y)) + 1))
zz = (-normal_v[0] * xx - normal_v[1] * yy - d) * 1. / normal_v[2]
ax.plot_surface(xx, yy, zz, alpha=0.2)
# wektora normalny
ax.plot([cx, normal_v[0] * 10], [cy, normal_v[1] * 10], [cz, normal_v[2] * 10], color='red')

ax.set_xlim3d(-1, 14)
ax.set_ylim3d(1050, 1065)
ax.set_zlim3d(930, 945)

fig_2, axs = plt.subplots(1, 3)

axs[0].plot(x[0:2], z[0:2], 'r', label='Płaszczyzna x i z -> widok od tyłu robota')
axs[1].plot(y[0:2], z[0:2], 'r', label='Płaszczyzna y i z -> widok od nasze strony robota')
axs[2].plot(x[0:2], y[0:2], 'r', label='Płaszczyzna x i y -> widok od góry robota')

axs[0].plot(x[2:4], z[2:4], 'r')
axs[1].plot(y[2:4], z[2:4], 'r')
axs[2].plot(x[2:4], y[2:4], 'r')

axs[0].set_xlim(-1, 14)
axs[0].set_ylim(930, 945)
axs[1].set_xlim(1050, 1065)
axs[1].set_ylim(930, 945)
axs[2].set_xlim(-1, 14)
axs[2].set_ylim(1050, 1065)

for ax in axs:
    ax.grid(True, which='both')
    ax.legend()

fig_2.set_size_inches(12, 4)
# glowne osie
# ax.plot([0, 1], [0, 0], [0, 0], color='black')
# ax.plot([0, 0], [0, 1], [0, 0], color='black')
# ax.plot([0, 0], [0, 0], [0, 1], color='black')
plt.show()
