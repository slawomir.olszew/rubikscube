import socket
import sys


class client():
    def __init__(self, ip_address, port):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        # Connect the socket to the port where the server is listening
        server_address = (ip_address, port)
        print('connecting to {} port {}'.format(*server_address))
        self.sock.connect(server_address)
        print('Connected')

    def send(self, message):
        message = message.encode()
        self.sock.sendall(message)

    def receive(self):
        data = self.sock.recv(255).decode()
        if len(data) != 0:
            print("Server: ", data)


my_client = client('192.168.0.73', 10010)

while(True):
    send_data = input("Client: ")
    my_client.send(send_data)
