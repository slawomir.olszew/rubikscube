from robot import *
from side_reading import SideReading
import itertools
from cube3D import Cube
from matplotlib import animation

# test_moves = "F' R D F' R B2 U' R' F2 R D' F2 R2 F2 D2 B2 U B2 R2 D2"
test_moves = "F F' F F F2"


def adjust(move):
    angle = '90'
    robot_move = move[0]
    if len(move) > 1:
        if move[1] == '2':
            angle = '180'
        elif move[1] == "'":
            angle = '-90'
    return robot_move, angle


def change_holder():
    if kawasaki.holds_cube_one_field:
        print("---------")
        print("Changing from holding one field to two fields on cube.")
        print("---------")
        kawasaki.move("K;0")
        mitsubishi.move("T;0")
        kawasaki.move("G;0")
        kawasaki.move("J;0")
        mitsubishi.move("G;0")
    else:
        print("---------")
        print("Changing from holding two fields to one field on cube.")
        print("---------")
        kawasaki.move("T;0")
        mitsubishi.move("T;0")
        kawasaki.move("G;0")
        kawasaki.move("P;0")
        mitsubishi.move("G;0")

    kawasaki.holds_cube_one_field = not kawasaki.holds_cube_one_field
    kawasaki.move('N;0')


def print_current_move(move, angle):
    print("---------")
    print("Kawasaki: ", move)
    print("Mitsubishi: ", move, angle)
    print("---------")


def solve(moves):
    reset_orient_dict = {'L': 'L',
                         'R': 'R',
                         'F': 'F',
                         'D': 'D',
                         'U': 'U',
                         'B': 'B'}
    global cube3D
    moves = list(map(adjust, moves.split()))
    moves = list(zip(*(iter(list(itertools.chain(*moves))),) * 2))

    mitsubishi.move("P;0")
    if moves[0][0] == 'Q':
        kawasaki.holds_cube_one_field = False
        kawasaki.move("J;0")
    elif moves[0][0] in Robot.FLR_moves:
        kawasaki.move("P;0")
        kawasaki.holds_cube_one_field = True
    elif moves[0][0] in Robot.BUD_moves:
        kawasaki.holds_cube_one_field = False
        kawasaki.move("J;0")
    mitsubishi.move("G;0")

    for i, move_angle in enumerate(moves):
        move, angle = move_angle
        cube3D.move(move, angle)
        print(f'ZOSTALO RUCHOW: {len(moves) - i}')
        if move == 'Q':
            break
        move = reset_orient_dict[move]
        if move in Robot.FLR_moves:
            if not kawasaki.holds_cube_one_field:
                change_holder()

            print_current_move(move, angle)
            kawasaki.move(move + ';0')
            mitsubishi.move(move + ';' + angle)

            if move == 'F':
                if angle == '90':
                    tmp = reset_orient_dict['U']
                    reset_orient_dict['U'] = reset_orient_dict['L']
                    reset_orient_dict['L'] = reset_orient_dict['D']
                    reset_orient_dict['D'] = reset_orient_dict['R']
                    reset_orient_dict['R'] = tmp
                elif angle == '-90':
                    tmp = reset_orient_dict['U']
                    reset_orient_dict['U'] = reset_orient_dict['R']
                    reset_orient_dict['R'] = reset_orient_dict['D']
                    reset_orient_dict['D'] = reset_orient_dict['L']
                    reset_orient_dict['L'] = tmp
                elif angle == '180':
                    tmp = reset_orient_dict['U']
                    reset_orient_dict['U'] = reset_orient_dict['D']
                    temp2 = reset_orient_dict['L']
                    reset_orient_dict['L'] = reset_orient_dict['R']
                    reset_orient_dict['D'] = tmp
                    reset_orient_dict['R'] = temp2
        elif move in Robot.BUD_moves:
            if kawasaki.holds_cube_one_field:
                change_holder()

            print_current_move(move, angle)
            kawasaki.move(move + ';0')
            mitsubishi.move(move + ';' + angle)
            kawasaki.move('N;0')

    send_quit()


def send_quit():
    mitsubishi.move("Q;0")
    kawasaki.move("Q;1")


def scan_cube():
    global cube3D
    detector = SideReading()

    kawasaki.send_receive_ack("S;0")
    detector.get_cube_colors('k')  # scan
    kawasaki.send_receive_ack("S;0")
    detector.get_cube_colors('k')  # scan
    kawasaki.send_receive_ack("S;0")
    detector.get_cube_colors('k')  # scan
    kawasaki.send_receive_ack("S;0")

    mitsubishi.send_receive_ack("T;0")
    kawasaki.send_receive_ack("G;0")
    kawasaki.move('N;0')
    mitsubishi.send_receive_ack("S;0")
    detector.get_cube_colors('m')  # scan
    mitsubishi.send_receive_ack("S;0")
    detector.get_cube_colors('m')  # scan
    mitsubishi.send_receive_ack("S;0")
    kawasaki.move('N;0')
    detector.get_cube_colors('m')  # scan
    mitsubishi.send_receive_ack("S;0")

    cube3D = Cube(detector.SIDES)
    detector.prepare_for_kociemba()

    cube3D.start()
    cube3D.show(False)

    moves = detector.solve()
    print(moves)
    return moves


if __name__ == '__main__':
    global cube3D
    # kawasaki = Robot(27040, "192.168.0.220")
    kawasaki = Robot(27040, "192.168.0.220")
    # kawasaki = Robot(27040)
    mitsubishi = Robot(27041)
    kawasaki.send_receive_ack("W;0")
    # kawasaki.send_receive_ack("H;0")
    solve_moves = scan_cube()
    # ani = animation.FuncAnimation(cube3D.fig, cube3D.rotate, 5, interval=30, blit=False)
    # kawasaki.holds_cube = True
    # mitsubishi.holds_cube = True
    # run kociemba

    # print(test_moves)
    # send_quit()
    solve(solve_moves)
    cube3D.finished = True
    cube3D.b_viewReset.set_active(True)
    cube3D.show(True)
