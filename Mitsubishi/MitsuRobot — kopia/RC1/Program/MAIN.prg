1 'Main programm for RubiksCube project
2 'Set the following points before start:
3 'JHome - home position
4 'JEndCube - end cube position
5 'JWaitCube - position where robot waits for next move
6 'JHoldCube - position where robot holds cube while second one is operating
7 'JFrontCube - Rubik cube's front face position (Mitsubushi view)
8 'JLeftCube - Rubik cube's left face position (Mitsubushi view)
9 'JRightCube - Rubik cube's right face position (Mitsubushi view)
10 '(Optional): JTopCube, JBottomCube
11 '
12 '
13 'Settings
14 Base(+0.00,+0.00,+0.00,-0.50,-4.00,+0.00)
15 Tool(-47.54,+0.00,+315.18,+0.00,+0.00,+0.00)
16 Spd 100
17 JOvrd 100
18 Ovrd 100
19 Accel 70,50
20 Cnt 0
21 Servo On
22 Wait M_Svo(1)=1
23 Cmp Off
24 '
25 'TCP settings
26 Def Char ReceivedMessage
27 Def Char RotateFace
28 Def Inte RotateAngle
29 '
30 '####main programm####
31 GoSub *BeginConnection
32 CallP "GrippZero"
33 CallP "GrippError"
34 'Mov JHome
35 *loop
36 '
37 GoSub *Receive
38 GoSub *EncodeMessage
39 '
40 If RotateFace$ = "F" Then
41     Mov PFrontCube
42     Dly 0.1
43     GoSub *SendAck
44 EndIf
45 '
46 If RotateFace$ = "L" Then
47     Mov PLeftCube
48     Dly 0.1
49     GoSub *SendAck
50 EndIf
51 '
52 '
53 If RotateFace$ = "R" Then
54     Mov PRightCube
55     Dly 0.1
56     GoSub *SendAck
57 EndIf
58 '
59 '
60 If RotateFace$ = "N" Then
61     Mov PRotateCube, -100
62     Dly 0.1
63     Mvs PRotateCube
64     CallP "GrippClose"
65     PAngle = (+0.00,+0.00,+0.00,+0.00,+0.00,+0.00)
66     PAngle.C = PAngle.C + RotateAngle%
67     Prec On
68     Mov PRotateCube*PAngle
69     Prec Off
70     CallP "GrippOpen"
71     Mvs P_Curr, -100
72     Dly 0.1
73     PAngle.C = PAngle.C - RotateAngle%
74     Mov P_Curr*PAngle
75     Mov PWaitCube
76     GoSub *SendAck
77 EndIf
78 '
79 '
80 If RotateFace$ = "T" Then
81     CallP "GrippOpen"
82     Mov PTakeCube, -100
83     Mvs PTakeCube
84     CallP "GrippClose"
85     GoSub *SendAck
86 EndIf
87 '
88 '
89 If RotateFace$ = "G" Then
90     CallP "GrippOpen"
91     Dly 0.1
92     Mvs P_Curr, -100
93     Dly 0.1
94     Mov PWaitCube
95     Dly 0.1
96     GoSub *SendAck
97 EndIf
98 '
99 '
100 If RotateFace$ = "S" Then
101 GoSub *ScanCube
102 EndIf
103 '
104 '
105 If RotateFace$ = "Q" Then
106 GoTo *end_prog
107 EndIf
108 '
109 GoTo *loop
110 '
111 *BeginConnection
112 'C_Com(2)="ETH:192.168.0.40,27041"
113 C_Com(2)="ETH:192.168.0.164,27041"
114 *Connect
115 Open "COM2:" As #1
116 If M_Open(1)<>1 Then GoTo *Connect
117 'Com(1) On
118 Return
119 '
120 *SendAck
121 Print #1, "mitsu_end"
122 Return
123 '
124 *Receive
125 Input #1, ReceivedMessage$
126 Return
127 '
128 *EncodeMessage
129 MLength = Len(ReceivedMessage$) - 2
130 RotateFace$ = Left$(ReceivedMessage$,1)
131 CTemp$ = Mid$(ReceivedMessage$,3,MLength)
132 RotateAngle% = Val(CTemp$)
133 Return
134 '
135 *ScanCube
136 Mov JScanCube1
137 GoSub *SendAck
138 GoSub *Receive
139 Mov JScanCube2
140 GoSub *SendAck
141 GoSub *Receive
142 Mov JScanCube3
143 GoSub *SendAck
144 GoSub *Receive
145 Mov JHoldCube
146 GoSub *SendAck
147 Return
148 '
149 'samples
150 'CallP "PutCubeBack", JEndCube
151 'CallP "RotateTop", JTopCube, JWaitCube, Rad(90)
152 'CallP "RotateFront", JFrontCube, JWaitCube, Rad(90)
153 'CallP "RotateBottom", JBottomCube, JWaitCube, Rad(90)
154 'CallP "RotateLeft", JLeftCube, JWaitCube, Rad(90)
155 'CallP "RotateRight", JRightCube, JWaitCube, Rad(90)
156 'CallP "TakeCube", JHoldCube, JWaitCube
157 'CallP "GiveCube", JWaitCube
158 '
159 *end_prog
160 Close #1
161 If RotateAngle% = 1 Then
162 CallP "PutCubeBack", JEndCube
163 EndIf
164 Mov JHome
165 Dly 10000
166 End
PFrontCube=(+916.74,+0.00,+1073.58,-180.00,+81.43,+180.00)(7,0)
PLeftCube=(+916.74,+0.00,+1073.58,+85.88,+81.43,-180.00)(7,0)
PRightCube=(+916.74,+0.00,+1073.58,-96.00,+81.43,-180.00)(7,0)
PRotateCube=(+1342.93,+25.81,+999.12,-3.00,+82.97,-2.03)(6,0)
PAngle=(+0.00,+0.00,+0.00,+0.00,+0.00,+0.00,+0.00,+0.00)(,)
PWaitCube=(+1207.76,+23.48,+982.48,-3.00,+82.97,-2.03)(6,0)
PTakeCube=(+1395.41,+26.72,+1005.58,-3.00,+82.97,-2.03)(6,0)
JScanCube1=(+89.94,+0.76,+50.96,-4.18,+30.87,-170.06)
JScanCube2=(+88.02,+18.76,+68.37,-3.84,-88.89,-172.68)
JScanCube3=(+90.02,-2.73,+60.22,-3.84,+29.37,-352.86)
JHoldCube=(+87.58,-18.90,+115.67,+8.36,-6.55,-95.51)
JEndCube=(+40.56,+0.95,+130.78,-3.84,+49.71,-308.51)
JHome=(+89.69,-24.57,+105.60,+0.45,+94.63,-184.47)
JBottomCube=(+1.21,+71.13,+42.27,+1.44,-109.97,+19.29)
JFrontCube=(+87.87,-16.84,+120.26,-21.84,-13.34,+23.91)
JLeftCube=(+19.69,+57.99,+64.87,+97.53,-107.34,-58.19)
JRightCube=(-15.47,+54.38,+67.29,+78.33,+101.17,-58.26)
JTopCube=(+0.00,+0.00,+0.00,+0.00,+0.00,+0.00)
JWaitCube=(+86.18,-47.41,+137.71,-0.31,-14.06,-83.38)
