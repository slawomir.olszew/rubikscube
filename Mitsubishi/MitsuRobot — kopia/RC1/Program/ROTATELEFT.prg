1 FPrm JLeftPosition,JWaitPosition, MAngle
2 JRotateJoint = JLeftPosition
3 JRotateJoint.J6 = JRotateJoint.J6 + MAngle
4 PLeftPosition = JtoP(JLeftPosition) 'convert joint to point
5 PLeftSafePoint = PLeftPosition
6 PLeftSafePoint.X = PLeftSafePoint.X - 100
7 PLeftSafePoint.Y = PLeftSafePoint.Y + 100
8 CallP "GrippOpen"
9 Mov PLeftSafePoint
10 Mov PLeftPosition, -100
11 Mvs PLeftPosition
12 CallP "GrippClose"
13 Mov JRotateJoint
14 CallP "GrippOpen"
15 'Dly 1
16 PLeftPosition = JtoP(JRotateJoint) 'convert joint to point
17 Mvs PLeftPosition, -100
18 Mov PLeftSafePoint
19 Mov JWaitPosition
PLeftPosition=(+1160.95,+317.96,+456.00,-86.75,-1.67,-178.99,+0.00,+0.00)(6,0)
PLeftSafePoint=(+1060.95,+417.96,+456.00,-27.17,+86.35,-116.11,+0.00,+0.00)(6,0)
JLeftPosition=(+19.69,+57.99,+64.87,+97.53,-107.34,-58.19)
JWaitPosition=(+3.59,+1.39,+109.46,+0.00,-17.12,+3.59)
JRotateJoint=(+19.69,+57.99,+64.87,+97.53,-107.34,-148.19)
