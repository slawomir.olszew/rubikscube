1 FPrm JRightPosition,JWaitPosition, MAngle
2 JRotateJoint = JRightPosition
3 JRotateJoint.J6 = JRotateJoint.J6 + MAngle
4 PRightPosition = JtoP(JRightPosition) 'convert joint to point
5 PRightSafePoint = PRightPosition
6 PRightSafePoint.X = PRightSafePoint.X - 100
7 PRightSafePoint.Y = PRightSafePoint.Y - 100
8 CallP "GrippOpen"
9 Mov PRightSafePoint
10 Mov PRightPosition, -100
11 Mvs PRightPosition
12 CallP "GrippClose"
13 Mov JRotateJoint
14 CallP "GrippOpen"
15 'Dly 1
16 PRightPosition = JtoP(JRotateJoint) 'convert joint to point
17 Mvs PRightPosition, -100
18 Mov PRightSafePoint
19 Mov JWaitPosition
PRightPosition=(+1173.05,-227.96,+485.95,-94.25,-24.86,+1.96,+0.00,+0.00)(7,0)
PRightSafePoint=(+1073.05,-327.96,+485.95,+94.25,+24.86,-178.04,+0.00,+0.00)(7,0)
JRightPosition=(-15.47,+54.38,+67.29,+78.33,+101.17,-58.26)
JWaitPosition=(+3.59,+1.39,+109.46,+0.00,-17.12,+3.59)
JRotateJoint=(-15.47,+54.38,+67.29,+78.33,+101.17,+121.74)
