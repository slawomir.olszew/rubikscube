1 'Main programm for RubiksCube project
2 'Set the following points before start:
3 'JHome - home position
4 'JEndCube - end cube position
5 'JWaitCube - position where robot waits for next move
6 'JHoldCube - position where robot holds cube while second one is operating
7 'JFrontCube - Rubik cube's front face position (Mitsubushi view)
8 'JLeftCube - Rubik cube's left face position (Mitsubushi view)
9 'JRightCube - Rubik cube's right face position (Mitsubushi view)
10 '(Optional): JTopCube, JBottomCube
11 '
12 '
13 'Settings
14 Base(+0.00,+0.00,+0.00,-0.50,-4.00,+0.00)
15 Tool(-47.54,+0.00,+315.18,+0.00,+0.00,+0.00)
16 Spd 500
17 JOvrd 40
18 Ovrd 100
19 Accel 100,100
20 Cnt 0
21 Servo On
22 Wait M_Svo(1)=1
23 Cmp Off
24 ColChk Off
25 '
26 'TCP settings
27 Def Char ReceivedMessage
28 Def Char RotateFace
29 Def Inte RotateAngle
30 '
31 '####main programm####
32 GoSub *BeginConnection
33 CallP "GrippZero"
34 CallP "GrippError"
35 Mov PWaitCube
36 *loop
37 '
38 GoSub *Receive
39 GoSub *EncodeMessage
40 '
41 If RotateFace$ = "F" Then
42     Mov PRotateF, -100
43     Dly 0.1
44     Mvs PRotateF
45     If RotateAngle% = 180 Then
46         RotateAngle% = 90
47         GoSub *RotateDepart
48         Mvs PRotateF
49         Dly 0.1
50         GoSub *RotateDepart
51     Else
52         GoSub *RotateDepart
53     EndIf
54     Mov PWaitCube
55     Dly 0.1
56     GoSub *SendAck
57 EndIf
58 '
59 If RotateFace$ = "L" Then
60     If RotateAngle% = 90 Then
61         Mov PRotateLR, -100
62         Dly 0.1
63         Mvs PRotateLR
64         GoSub *RotateDepart
65     EndIf
66     If RotateAngle% = 180 Then
67         RotateAngle% = 90
68         Mov PRotateLR, -100
69         Dly 0.1
70         Mvs PRotateLR
71         GoSub *RotateDepart
72         Mvs PRotateLR
73         Dly 0.1
74         GoSub *RotateDepart
75     EndIf
76     If RotateAngle% = -90 Then
77         Mov PRotateLL, -100
78         Dly 0.1
79         Mvs PRotateLL
80         GoSub *RotateDepart
81     EndIf
82     Mov PWaitCube
83     Dly 0.1
84     GoSub *SendAck
85 EndIf
86 '
87 If RotateFace$ = "R" Then
88     Mov PRotateR, -100
89     Dly 0.1
90     Mvs PRotateR
91     If RotateAngle% = 180 Then
92         RotateAngle% = 90
93         GoSub *RotateDepart
94         Mvs PRotateR
95         Dly 0.1
96         GoSub *RotateDepart
97     Else
98         GoSub *RotateDepart
99     EndIf
100     Mov PWaitCube
101     Dly 0.1
102     GoSub *SendAck
103 EndIf
104 '
105 If RotateFace$ = "B" Then
106     Mov PRotateB, -100
107     Dly 0.1
108     Mvs PRotateB
109     If RotateAngle% = 180 Then
110         RotateAngle% = 90
111         GoSub *RotateDepart
112         Mvs PRotateB
113         Dly 0.1
114         GoSub *RotateDepart
115     Else
116         GoSub *RotateDepart
117     EndIf
118     Mov PWaitCube
119     Dly 0.1
120     GoSub *SendAck
121 EndIf
122 '
123 If RotateFace$ = "U" Then
124     Mov PRotateU, -100
125     Dly 0.1
126     Mvs PRotateU
127     If RotateAngle% = 180 Then
128         RotateAngle% = 90
129         GoSub *RotateDepart
130         Mvs PRotateU
131         Dly 0.1
132         GoSub *RotateDepart
133     Else
134         GoSub *RotateDepart
135     EndIf
136     Mov PWaitCube
137     Dly 0.1
138     GoSub *SendAck
139 EndIf
140 '
141 If RotateFace$ = "D" Then
142     If RotateAngle% = 90 Then
143         Mov PRotateDR, -230
144         Dly 0.1
145         Mvs PRotateDR
146         GoSub *RotateDepartD
147     EndIf
148     If RotateAngle% = 180 Then
149         RotateAngle% = 90
150         Mov PRotateDR, -230
151         Dly 0.1
152         Mvs PRotateDR
153         GoSub *RotateDepartD
154         Mvs PRotateDR
155         GoSub *RotateDepartD
156     EndIf
157     If RotateAngle% = -90 Then
158         Mov PRotateDL, -230
159         Dly 0.1
160         Mvs PRotateDL
161         GoSub *RotateDepartD
162     EndIf
163     Mov PWaitCube
164     Dly 0.1
165     GoSub *SendAck
166 EndIf
167 '
168 If RotateFace$ = "T" Then
169     CallP "GrippOpen"
170     Mov PTakeCube, -100
171     Dly 0.1
172     Mvs PTakeCube
173     Dly 0.1
174     CallP "GrippClose"
175     GoSub *SendAck
176 EndIf
177 '
178 If RotateFace$ = "P" Then
179     Mov PTakeCube
180     Dly 0.1
181     GoSub *SendAck
182 EndIf
183 '
184 If RotateFace$ = "G" Then
185     CallP "GrippOpen"
186     Mvs P_Curr, -100
187     Dly 0.1
188     Mov PWaitCube
189     Dly 0.1
190     GoSub *SendAck
191 EndIf
192 '
193 If RotateFace$ = "S" Then
194 GoSub *ScanCube
195 EndIf
196 '
197 '
198 If RotateFace$ = "Q" Then
199 GoTo *end_prog
200 EndIf
201 '
202 GoTo *loop
203 '
204 *BeginConnection
205 C_Com(2)="ETH:192.168.0.2,27041"
206 'C_Com(2)="ETH:192.168.0.164,27041"
207 *Connect
208 Open "COM2:" As #1
209 If M_Open(1)<>1 Then GoTo *Connect
210 'Com(1) On
211 Return
212 '
213 *RotateDepart
214     Accel 50,50
215     JOvrd 20
216     CallP "GrippClose"
217     Dly 0.1
218     PAngle = (+0.00,+0.00,+0.00,+0.00,+0.00,+0.00)
219     PAngle.C = PAngle.C + RotateAngle%
220     Prec On
221     Mvs P_Curr*PAngle
222     Prec Off
223     CallP "GrippOpen"
224     Mvs P_Curr, -100
225     Dly 0.1
226     PAngle = (+0.00,+0.00,+0.00,+0.00,+0.00,+0.00)
227     PAngle.C = PAngle.C - RotateAngle%
228     Mvs P_Curr*PAngle
229     Dly 0.1
230     Accel 100,100
231     JOvrd 40
232 Return
233 '
234 *RotateDepartD
235     Accel 50,50
236     JOvrd 20
237     CallP "GrippClose"
238     Dly 0.1
239     PAngle = (+0.00,+0.00,+0.00,+0.00,+0.00,+0.00)
240     PAngle.C = PAngle.C + RotateAngle%
241     Prec On
242     Mvs P_Curr*PAngle
243     Prec Off
244     CallP "GrippOpen"
245     Mvs P_Curr, -230
246     Dly 0.1
247     PAngle = (+0.00,+0.00,+0.00,+0.00,+0.00,+0.00)
248     PAngle.C = PAngle.C - RotateAngle%
249     Mvs P_Curr*PAngle
250     Dly 0.1
251     Accel 100,100
252     JOvrd 40
253 Return
254 '
255 *SendAck
256 Print #1, "mitsu_end"
257 Return
258 '
259 *Receive
260 Input #1, ReceivedMessage$
261 Return
262 '
263 *EncodeMessage
264 MLength = Len(ReceivedMessage$) - 2
265 RotateFace$ = Left$(ReceivedMessage$,1)
266 CTemp$ = Mid$(ReceivedMessage$,3,MLength)
267 RotateAngle% = Val(CTemp$)
268 Return
269 '
270 *ScanCube
271 Mov JScanCube1
272 GoSub *SendAck
273 GoSub *Receive
274 Mov JScanCube2
275 GoSub *SendAck
276 GoSub *Receive
277 Mov JScanCube3
278 GoSub *SendAck
279 GoSub *Receive
280 Mov PWaitCube
281 GoSub *SendAck
282 Return
283 '
284 'samples
285 'CallP "PutCubeBack", JEndCube
286 'CallP "RotateTop", JTopCube, JWaitCube, Rad(90)
287 'CallP "RotateFront", JFrontCube, JWaitCube, Rad(90)
288 'CallP "RotateBottom", JBottomCube, JWaitCube, Rad(90)
289 'CallP "RotateLeft", JLeftCube, JWaitCube, Rad(90)
290 'CallP "RotateRight", JRightCube, JWaitCube, Rad(90)
291 'CallP "TakeCube", JHoldCube, JWaitCube
292 'CallP "GiveCube", JWaitCube
293 '
294 *end_prog
295 Mov PWaitCube
296 Dly 5
297 GoSub *SendAck
298 Dly 0.1
299 Close #1
300 Mov JHome
301 Dly 10000
302 End
PWaitCube=(-100.82,+729.97,+1146.35,+86.90,+1.03,-178.93)(7,0)
PRotateF=(-100.82,+1011.61,+1146.35,+86.90,+1.03,-178.93)(7,0)
PRotateLR=(-86.23,+939.69,+914.88,+90.00,+0.00,-180.00)(7,1048576)
PRotateLL=(-88.69,+939.83,+914.88,-90.00,-90.00,+0.00)(7,1048576)
PRotateR=(-178.89,+897.70,+1113.33,-90.00,+87.72,+0.00)(7,0)
PRotateB=(-100.82,+1012.30,+1146.35,+86.90,+1.03,-178.93)(7,0)
PRotateU=(-158.10,+1310.66,+1234.03,-78.48,-90.00,-10.36)(7,1048576)
PRotateDR=(-136.71,+921.42,+1242.50,+90.00,+90.00,+180.00)(7,0)
PRotateDL=(-137.61,+923.02,+1238.40,+90.00,+0.00,-180.00)(7,0)
PTakeCube=(-100.82,+1180.63,+1146.35,+86.90,+1.03,-178.93)(7,0)
PAngle=(+0.00,+0.00,+0.00,+0.00,+0.00,-90.00,+0.00,+0.00)(,)
JScanCube1=(+88.77,+9.41,+33.96,+1.17,+46.98,-174.46)
JScanCube2=(+88.15,+18.94,+66.53,-3.91,-86.79,-175.84)
JScanCube3=(+89.57,-2.68,+60.21,-1.46,+29.24,-354.87)
JHome=(+89.69,-24.57,+105.60,+0.45,+94.63,-184.47)
JEndCube=(+40.56,+0.95,+130.78,-3.84,+49.71,-308.51)
