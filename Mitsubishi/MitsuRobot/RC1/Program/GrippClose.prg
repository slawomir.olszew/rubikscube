1 '===== Zamykanie chwytaka
2 *grCloseLoop
3 'Ewentualne wyzerowanie errora chwytaka
4 'IF M_In(4)=0 CallP "GrippError"
5 Dly 0.25 'Czekanie 0,25s
6 M_Out(26) = 1
7 Dly 0.25 'Czekanie 0,25s
8 'Zerowanie sygnałów wejściowych chwytaka
9 M_Out(26) = 0 'IN0
10 M_Out(6) = 1 'IN1
11 M_Out(18) = 1 'IN2
12 M_Out(19) = 0 'IN3
13 Dly 0.25 'Czekanie 0,25s
14 M_Out(26) = 1
15 Dly 0.5 'Czekanie 0,5s
16 'Czekanie na pojawienia sie informacji o zakonczeniu chwytania
17 M1 = M_Timer(1)
18 *waitIn16
19 M2 = M_Timer(1)
20 If (M2-M1<1500) And (M_In(16)=1) GoTo *waitIn16
21 Dly 0.25 'Czekanie 0,25s
22 M_Out(26) = 0
23 'IF M_In(4)=0 GOTO *grCloseLoop
24 End
