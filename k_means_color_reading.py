import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D


class KMeanReading:
    COLORS = ['w', 'r', 'g', 'b', '#FFA500', 'y', 'k']
    COLOR_NAMES = ["w", "r", "g", "b", "o", "y"]

    def __init__(self):
        # self.test_data = [(42, 50, 197), (255, 255, 255), (18, 20, 252), (38, 153, 72), (252, 241, 227), (190, 98, 2),
        #                   (21, 210, 238), (21, 212, 238), (189, 92, 2)]

        # white, red, green, blue, orange, yellow
        self.centroids = [(255, 255, 244.44), (255.0, 62.44, 12.33), (121, 255, 73.33), (44.33, 86.77, 151.55),
                          (255.0, 240, 29.77), (255, 255, 85.11)]

        self.point_length = 3

    def set_centroids(self, centroids):
        self.centroids = centroids

    def calc_best_centroid(self, point, centroids):
        distance = []
        for centroid in centroids:
            add = 0
            for i in range(self.point_length):
                add += (point[i] - centroid[i]) ** 2
            distance.append(np.sqrt(add))

        return min(range(len(distance)), key=distance.__getitem__)

    def assign_to_centroids(self, points, centroids):
        centroid_assignment = [[] for x in range(len(centroids))]
        points_assignment = []
        for point in points:
            points_assignment.append(self.calc_best_centroid(point, centroids))
            centroid_assignment[points_assignment[-1]].append(point)

        return centroid_assignment, points_assignment

    def update_centroids(self, centr_assignment):
        centroids = []
        i = 0
        for centers in centr_assignment:
            i += 1
            if len(centers) == 0:
                centroids.append(self.centroids[i])
                continue
            mean_arr = []
            for i in range(self.point_length):
                mean_arr.append(np.mean([x[i] for x in centers]))

            centroids.append(mean_arr)

        return centroids

    def print_cube_face(self, points_assignment):
        print("|-{}-|-{}-|-{}-|".format(self.COLOR_NAMES[points_assignment[0]], self.COLOR_NAMES[points_assignment[1]],
                                        self.COLOR_NAMES[points_assignment[2]]))
        print("|-{}-|-{}-|-{}-|".format(self.COLOR_NAMES[points_assignment[3]], self.COLOR_NAMES[points_assignment[4]],
                                        self.COLOR_NAMES[points_assignment[5]]))
        print("|-{}-|-{}-|-{}-|".format(self.COLOR_NAMES[points_assignment[6]], self.COLOR_NAMES[points_assignment[7]],
                                        self.COLOR_NAMES[points_assignment[8]]))

    def get_colors(self, data, plot=False):
        centroids_arr = self.centroids.copy()
        self.point_length = len(data[0])
        # Calculation loop
        centroid_assignment_arr = []
        points_assignment_arr = []
        for i in range(2000):
            centroid_assignment_arr, points_assignment_arr = self.assign_to_centroids(data, centroids_arr)
            centroids_arr = self.update_centroids(centroid_assignment_arr)

        self.print_cube_face(points_assignment_arr)

        if plot:
            self.plot_data(centroids_arr, centroid_assignment_arr)

        for i in range(len(points_assignment_arr)):
            points_assignment_arr[i] = self.COLOR_NAMES[points_assignment_arr[i]]

        return points_assignment_arr

    def plot_data(self, centroids_arr, centr_assignment_arr):
        # Plotting results
        fig, axs = plt.subplots(1, 1)
        gs = axs.get_gridspec()

        axs.remove()

        ax1 = fig.add_subplot(gs[:, 0], projection='3d')
        fig.tight_layout()

        for j in range(len(centroids_arr)):
            label = 'Claster ' + self.COLOR_NAMES[j]
            ax1.plot3D([i[0] for i in centr_assignment_arr[j]], [i[1] for i in centr_assignment_arr[j]],
                       [i[2] for i in centr_assignment_arr[j]], 'o', color=self.COLORS[j], label=label)

        ax1.plot3D([i[0] for i in centroids_arr], [i[1] for i in centroids_arr],
                   [i[2] for i in centroids_arr], '+', color=self.COLORS[-1], label='Center of the claster')

        fig.set_size_inches(9, 6)
        ax1.legend()

        plt.show()
