from pymba import Vimba, Frame, VimbaException
from typing import Optional
import cv2

FEATURE_NAME = 'ExposureAuto'
PIXEL_FORMATS_CONVERSIONS = {
    'BayerRG8': cv2.COLOR_BAYER_RG2RGB,
}


def display_frame(frame: Frame, delay: Optional[int] = 1) -> None:
    image = frame.buffer_data_numpy()
    image = draw_lines(image)
    try:
        image = cv2.cvtColor(image, PIXEL_FORMATS_CONVERSIONS[frame.pixel_format])
    except KeyError:
        pass
    cv2.imshow('Image', image)
    # cv2.imwrite('kolory_nowe/test.png', image)
    cv2.waitKey(delay)


def draw_lines(img):
    color = (0, 0, 0)
    thick = 2
    img = cv2.line(img, (198, 117), (198, 370), color, thick)
    img = cv2.line(img, (198, 117), (448, 117), color, thick)
    img = cv2.line(img, (198, 370), (448, 370), color, thick)
    img = cv2.line(img, (448, 117), (448, 370), color, thick)
    img = cv2.line(img, (281, 117), (281, 370), color, thick)
    img = cv2.line(img, (364, 117), (364, 370), color, thick)
    img = cv2.line(img, (198, 201), (448, 201), color, thick)
    img = cv2.line(img, (198, 285), (448, 285), color, thick)
    return img


with Vimba() as vimba:
    camera = vimba.camera(0)
    camera.open()
    # feature = camera.feature(FEATURE_NAME)
    # feature.value = 'Continuous'

    feature = camera.feature(FEATURE_NAME)
    feature.value = 'Off'
    print('"{}" was set to "{}"'.format(feature.name, feature.value))

    feature = camera.feature('ExposureAutoAdjustTol')
    feature.value = 2
    print('"{}" was set to "{}"'.format(feature.name, feature.value))

    feature = camera.feature('ExposureAutoMin')
    feature.value = 4838
    print('"{}" was set to "{}"'.format(feature.name, feature.value))

    feature = camera.feature('ExposureTimeAbs')
    feature.value = 4498
    print('"{}" was set to "{}"'.format(feature.name, feature.value))
    #
    # for feature_name in camera.feature_names():
    #     feature = camera.feature(feature_name)
    #
    #     try:
    #         value = feature.value
    #         range_ = feature.range
    #
    #     except VimbaException as e:
    #         value = e
    #         range_ = None
    #
    #     print('\n\t'.join(
    #         str(x) for x in (
    #             feature_name,
    #             'value: {}'.format(value),
    #             'range: {}'.format(range_))
    #         if x is not None))

    # arm the camera and provide a function to be called upon frame ready
    cv2.namedWindow('Image')
    camera.arm('Continuous', display_frame)
    camera.start_frame_acquisition()

    # stream images for a while...
    while cv2.waitKey(1) != ord('q'):
        pass

    # stop frame acquisition
    # start_frame_acquisition can simply be called again if the camera is still armed
    camera.stop_frame_acquisition()
    camera.disarm()

    camera.close()
