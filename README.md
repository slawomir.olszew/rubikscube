# RubiksCube
Jak na razie dodana jest kalibracja kamery do kolorów na kostce oraz wykrycie ułożenia kolorów na jednej ścianie.

Uruchamiamy plik color_reading.py, początkowo należy zczytać kolor wskazywany w lewym górnym rogu
(gdy nakierujemy na dany kolor wskaźnik to zatwierdzamy poprzez wciśnięcie S), a następnie należy zczytać 
całą kostkę(zatwierdzamy klaiwszem S). Wyświetli się nam wtedy cała ściana, wraz z nazwą koloru danego kwadratu.

Aby zapewnić poprawne działanie wymagane jest bardzo dobre, stałe oświetlenie.

# Checkpointy
- [x] Zapozanie się ze stanowiskiem - kamera, stół, możliwości zamocowania kostki, jaki robot do symulacji, rozeznanie w chwytakach, opracowanie odpowiedniego oświetlenia
- [x] Zakup kilku sztuk odpowiedniej kostki, dopasowanej do możliwości stanowiska
- [ ] Interaktywna symulacja 3D kostki połączona z rzeczywistym położeniem kostki obustronnie
- [x] Kalibracja kamery z kolorami na kostce
- [x] Czytanie kolorów kwadratów na ścianie kostki - ustawienie ich równiez na symulacji kostki
- [x] Opracowanie algorytmu zwracającego konrketne ruchy robota na podstawie początkowego ułożenia kostki
- [x] Opracowanie ruchów robota - obrót górnej ściany o 90&deg; w lewo i prawo - zakładam użycie jednego robota
- [x] Opracowanie ruchów robota - obrót kostki, aby wybrana ściana znajdowała się u góry
- [ ] Przyspieszyć chwytak
- [ ] Ogarnąć kamerę i czujnik laserowy, żeby robot zgarniał kostkę z obojętnie jakiego miejsca
- [x] Komunikacja aplikacji z robotem - podczas rzeczywistego obracania kostki robotem

# Lista na następne spotkanie
### Na pewno do zrobienia:
1. Zamontować nowe palce wraz z gumą i sprawdzić jak chwytak łapie kostkę
1. Sprawdzić jak robot obraca kostkę, kiedy trzyma ją inny (w trybie teach)
1. Skonfigurować RT ToolBoxa, żeby mógł się łączyć z serwerem
1. Zebrać wszystkie punkty dla obu robotów
1. Puścić program na 100%
### Jeśli starczy czasu:
1. Sprawdzić dokładnie chwytak, czy uda się chociaż z nim połączyć