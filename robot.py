import socket


def is_number(string):
    try:
        float(string)
        return True
    except ValueError:
        return False


class Robot:
    """Posiible robot moves:
        KAWASAKI:
        - Go to THoldFront:                 F;0
        - Go to THoldRight:                 R;0
        - Go to THoldLeft:                  L;0
        - Go to THoldBack:                  B;0
        - Go to THoldUp:                    U;0
        - Go to THoldDown:                  D;0
        - Begin scan procedure:             S;0
        - Begin LiftCube procedure:         W;0
        - Go to TTakeBUD:                   T;0
        - Go to TTakeBUD with an approach:  J;0
        - Go to TTakeRLF with an approach:  P;0
        - Go to TTakeRLF:                   K;0
        - Open gripper and depart
          from current position:            G;0
        - Put cube back, go home
          and finish connection:            Q;1
        - Do nothing:                       N;0

        MITSUBISHI:
        - Go to PRotateF and rotate <angle>
          degrees around Z axis                         F;angle
        - Go to PRotateRR (if <angle> == 90)
          or PRotateRL    (if <angle> == -90)
          and rotate <angle> degrees around Z axis:     R;angle
        - Go to PRotateL and rotate <angle>
          degrees around Z axis                         L;angle
        - Go to PRotateB and rotate <angle>
          degrees around Z axis                         B;angle
        - Go to PRotateU and rotate <angle>
          degrees around Z axis                         U;angle
        - Go to PRotateDR (if <angle> == 90)
          or PRotateDL    (if <angle> == -90)
          and rotate <angle> degrees around Z axis:     D;angle
        - Begin scan procedure:                         S;0
        - Go to PTakeCube:                              T;0
        - Open gripper and depart
          from current position:                        G;0
        - Go home and finish connection:                Q;0
          """
    FLR_moves = {'F', 'L', 'R'}
    BUD_moves = {'B', 'U', 'D'}

    rotate_move = 'N'

    def __init__(self, port, ip_address=None):
        # Variables
        if ip_address is not None:
            self.is_server = True
        else:
            self.is_server = False

        self.last_data = b''
        self.holds_cube_one_field = False
        self.last_message = ""
        self.moves = {'F', 'B', 'U', 'D', 'L', 'R', 'S', 'T', 'G', 'Q', 'W', 'H', 'P', 'N', 'K', 'J'}
        # Connect
        if not self.is_server:
            self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            ip_address = socket.gethostbyname(socket.gethostname())
            self.s.bind((ip_address, port))
            self.s.listen(10)
            print("Server ip address: ", ip_address)
            print("Waiting for a client...")
            self.conn, address = self.s.accept()
            print("Connected!")
        else:
            self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            server_address = (ip_address, port)

            print('connecting to {} port {}'.format(*server_address))
            self.s.connect(server_address)
            print("Connected!")

    def move(self, move_type):
        self.send(move_type)
        self.receive()

    def send(self, message):
        # Check message format
        message = message.upper()
        check_array = message.split(';')
        if not check_array[0] in self.moves or not is_number(check_array[1]):
            message = "Q;0"

        message = message.encode()
        if not self.is_server:
            self.conn.sendall(message)
        else:
            self.s.sendall(message)

    def send_receive_ack(self, message):
        self.send(message)
        self.receive()

    def receive(self):
        if not self.is_server:
            self.last_message = self.conn.recv(256).decode()
        else:
            self.last_message = self.s.recv(256).decode()
        # print(self.last_message)
